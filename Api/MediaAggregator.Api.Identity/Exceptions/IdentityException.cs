﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace MediaAggregator.Api.Identity.Exceptions
{
    public class IdentityException : AggregateException
    {
        public IdentityException(IList<string> errors)
            : base(errors.FirstOrDefault(), errors.Select(x => new Exception(x)))
        {
        }
    }
}
