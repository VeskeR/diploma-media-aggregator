﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.Security.Claims;
using System.Text;
using Microsoft.AspNetCore.Http;
using MediaAggregator.Api.Identity.Extensions;
using MediaAggregator.Core.DataInterfaces;
using MediaAggregator.Core.Enumerations;

namespace MediaAggregator.Api.Identity
{
    public static class ProfileClaims
    {
        public static IEnumerable<Claim> ToClaims(this IProfile profile)
        {
            yield return new Claim(CustomClaimTypes.ProfileId, profile.Id.ToString());
            yield return new Claim(CustomClaimTypes.ProfileType, profile.Type.ToString());
        }

        public static Guid? GetProfileId(this ClaimsPrincipal user)
        {
            return user?.FindFirstGuid(CustomClaimTypes.ProfileId);
        }

        public static ProfileType? GetProfileType(this ClaimsPrincipal user)
        {
            var profileTypeClaim = user?.FindFirstValue(CustomClaimTypes.ProfileType);
            if (profileTypeClaim != null && Enum.TryParse(profileTypeClaim, true, out ProfileType t))
            {
                return t;
            }
            return null;
        }

        public static Guid? GetProfileId(this IHttpContextAccessor contextAccessor)
        {
            return contextAccessor?.HttpContext?.User.GetProfileId();
        }
    }
}
