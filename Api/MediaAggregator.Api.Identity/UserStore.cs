﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Security.Claims;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using MediaAggregator.Api.Identity.CommandHandlers.Authorization;
using MediaAggregator.Api.Identity.Data;
using MediaAggregator.Api.Identity.Data.Entities;
using MediaAggregator.Core.Data;

namespace MediaAggregator.Api.Identity
{
    internal class UserStore : UserStore<User, Role, IdentityContext>
    {
        public UserStore(IdentityContext context, IdentityErrorDescriber describer = null) : base(context, describer)
        {
        }

        public override async Task<IList<Claim>> GetClaimsAsync(User user, CancellationToken cancellationToken = new CancellationToken())
        {
            Debug.WriteLine("GetClaimsAsync");
            var claims = await base.GetClaimsAsync(user, cancellationToken);
            return claims;
        }
    }
}
