﻿using System.Text;
using Microsoft.AspNetCore.Identity;
using MediaAggregator.Api.Identity.Data;
using MediaAggregator.Api.Identity.Data.Entities;

namespace MediaAggregator.Api.Identity
{
    public class UserStoreFactory
    {
        private readonly IdentityErrorDescriber _describer;

        public UserStoreFactory(IdentityErrorDescriber describer)
        {
            _describer = describer;
        }

        public IUserStore<User> Create(IdentityContext context)
        {
            return new UserStore(context, _describer);
        }
    }
}
