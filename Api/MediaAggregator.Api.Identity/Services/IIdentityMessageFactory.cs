﻿using System;
using System.Collections.Generic;
using System.Text;
using MediaAggregator.Api.Identity.Data.Entities;

namespace MediaAggregator.Api.Identity.Services
{
    public interface IIdentityMessageFactory<out TMessage>
    {
        TMessage CreateAccountConfirmationMessage(User user, string code);
        TMessage CreateResetPasswordMessage(User user, string code);
    }
}
