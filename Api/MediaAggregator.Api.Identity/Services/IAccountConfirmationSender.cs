﻿using System.Threading.Tasks;
using MediaAggregator.Api.Identity.Data.Entities;

namespace MediaAggregator.Api.Identity.Services
{
    public interface IAccountConfirmationSender
    {
        Task SendEmailAsync(User user, string code);
        Task SendSmsAsync(User user, string code);
    }
}