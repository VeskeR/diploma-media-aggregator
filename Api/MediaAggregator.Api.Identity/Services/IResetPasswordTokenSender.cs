﻿using System.Threading.Tasks;
using MediaAggregator.Api.Identity.Data.Entities;

namespace MediaAggregator.Api.Identity.Services
{
    public interface IResetPasswordTokenSender
    {
        Task SendEmailAsync(User user, string token);
        Task SendSmsAsync(User user, string token);
    }
}