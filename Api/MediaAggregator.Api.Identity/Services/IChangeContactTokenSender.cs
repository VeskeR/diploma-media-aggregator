﻿using System.Threading.Tasks;

namespace MediaAggregator.Api.Identity.Services
{
    public interface IChangeContactTokenSender
    {
        Task SendEmailAsync(string email, string code);
        Task SendSmsAsync(string phone, string token);
    }
}