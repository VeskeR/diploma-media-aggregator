﻿using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using MediaAggregator.Api.Identity.Data.Entities;
using MediaAggregator.Core.Data;

namespace MediaAggregator.Api.Identity.Data
{
    public class IdentityContext : DataContext
    {
        public IdentityContext()
        {
        }

        public IdentityContext(DbContextOptions options, IEnumerable<IDataObserver> observers)
            : base(options, observers)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<User>()
                .HasMany(x => x.Claims)
                .WithOne()
                .HasForeignKey(x => x.UserId)
                .OnDelete(DeleteBehavior.Cascade);

            modelBuilder.Entity<User>()
                .HasMany<IdentityUserLogin<string>>()
                .WithOne()
                .HasForeignKey(x => x.UserId)
                .OnDelete(DeleteBehavior.Cascade);

            modelBuilder.Entity<User>()
                .HasMany<IdentityUserToken<string>>()
                .WithOne()
                .HasForeignKey(x => x.UserId)
                .OnDelete(DeleteBehavior.Cascade);

            modelBuilder.Entity<User>()
                .HasMany<IdentityUserRole<string>>()
                .WithOne()
                .HasForeignKey(x => x.UserId)
                .OnDelete(DeleteBehavior.Cascade);

            modelBuilder.Entity<Role>();

            modelBuilder.Entity<Role>()
                .HasMany<IdentityUserRole<string>>()
                .WithOne()
                .HasForeignKey(x => x.RoleId)
                .OnDelete(DeleteBehavior.Cascade);

            modelBuilder.Entity<IdentityUserRole<string>>()
                .HasKey(x => new {x.UserId, x.RoleId});

            modelBuilder.Entity<IdentityUserLogin<string>>()
                .ToTable("UserLogins")
                .HasKey(x => new { x.UserId, x.LoginProvider });

            modelBuilder.Entity<IdentityUserToken<string>>()
                .ToTable("UserTokens")
                .HasKey(x => new { x.UserId, x.LoginProvider });

            modelBuilder.Entity<IdentityUserClaim<string>>()
                .ToTable("UserClaims");
        }

        public DbSet<User> Users { get; set; }
        public DbSet<Role> Roles { get; set; }
        public DbSet<IdentityUserRole<string>> UserRoles { get; set; }
        public DbSet<IdentityUserClaim<string>> UserClaims { get; set; }
        public DbSet<IdentityUserLogin<string>> UserLogins { get; set; }
        public DbSet<IdentityUserToken<string>> UserTokens { get; set; }
    }
}
