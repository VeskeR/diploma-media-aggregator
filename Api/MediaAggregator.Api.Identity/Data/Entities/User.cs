using System.Collections.Generic;
using Microsoft.AspNetCore.Identity;

namespace MediaAggregator.Api.Identity.Data.Entities
{
    public class User : IdentityUser
    {
        public List<IdentityUserClaim<string>> Claims { get; set; } = new List<IdentityUserClaim<string>>();
    }
}