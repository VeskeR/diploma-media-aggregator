﻿using Microsoft.AspNetCore.Identity;

namespace MediaAggregator.Api.Identity.Data.Entities
{
    public class Role : IdentityRole
    {
        public Role()
        {
            
        }

        public Role(string name) : base(name)
        {
        }

        public const string Admin = "Admin";
    }
}