﻿using System;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using MediaAggregator.Api.Identity.CommandHandlers.Authorization;
using MediaAggregator.Api.Identity.Data;
using MediaAggregator.Api.Identity.Data.Entities;
using MediaAggregator.Api.Identity.Extensions;
using MediaAggregator.Core.CommandHandlers;
using MediaAggregator.Core.Commands;

namespace MediaAggregator.Api.Identity
{
    public class AuthorizedCommandHandlerProvider : ICommandHandlerProvider
    {
        public IUserClaimsPrincipalFactory<User> ClaimsPrincipalFactory { get; }
        public ICommandHandlerProvider Provider { get; }
        public IHttpContextAccessor ContextAccessor { get; }

        public AuthorizedCommandHandlerProvider(ICommandHandlerProvider provider, 
            IHttpContextAccessor contextAccessor, 
            IUserClaimsPrincipalFactory<User> claimsPrincipalFactory)
        {
            ClaimsPrincipalFactory = claimsPrincipalFactory ?? throw new ArgumentNullException(nameof(claimsPrincipalFactory));
            Provider = provider ?? throw new ArgumentNullException(nameof(provider));
            ContextAccessor = contextAccessor ?? throw new ArgumentNullException(nameof(contextAccessor));
        }

        public ICommandHandler<TCommand> GetHandler<TCommand>() 
            where TCommand : ICommand
        {
            return new AuthorizeCommandHandler<TCommand>(
                Provider.GetHandler<TCommand>(),
                ContextAccessor.HttpContext.User,
                ClaimsPrincipalFactory);
        }

        public ICommandHandler<TCommand, TResult> GetHandler<TCommand, TResult>() 
            where TCommand : ICommand
        {
            return new AuthorizeCommandHandler<TCommand, TResult>(
                Provider.GetHandler<TCommand, TResult>(), 
                ContextAccessor.HttpContext.User,
                ClaimsPrincipalFactory);
        }
    }
}
