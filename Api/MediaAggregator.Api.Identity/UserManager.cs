﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using MediaAggregator.Api.Identity.Data.Entities;
using MediaAggregator.Api.Identity.Extensions;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using MediaAggregator.Api.Identity.Services;
using MediaAggregator.Core.DataInterfaces;

namespace MediaAggregator.Api.Identity
{
    internal class UserManager : UserManager<User>
    {
        public RoleManager<Role> RoleManager { get; }
        public IAccountConfirmationSender AccountConfirmationSender { get; }

        public UserManager(IUserStore<User> store, IOptions<IdentityOptions> optionsAccessor, IPasswordHasher<User> passwordHasher, IEnumerable<IUserValidator<User>> userValidators, IEnumerable<IPasswordValidator<User>> passwordValidators, ILookupNormalizer keyNormalizer, IdentityErrorDescriber errors,
            IServiceProvider services, ILogger<UserManager<User>> logger, RoleManager<Role> roleManager,
            IAccountConfirmationSender accountConfirmationSender)
            : base(store, optionsAccessor, passwordHasher, userValidators, passwordValidators, keyNormalizer, errors, services, logger)
        {
            RoleManager = roleManager ?? throw new ArgumentNullException(nameof(roleManager));
            AccountConfirmationSender = accountConfirmationSender ?? throw new ArgumentNullException(nameof(accountConfirmationSender));
        }

        public override async Task<IList<Claim>> GetClaimsAsync(User user)
        {
            var claims = await base.GetClaimsAsync(user);
            var roles = await base.GetRolesAsync(user);
            foreach (var role in roles)
            {
                claims.Add(new Claim(ClaimTypes.Role, role));
            }
            return claims;
        }

        public override async Task<IdentityResult> AddToRoleAsync(User user, string role)
        {
            var roleExists = await RoleManager.RoleExistsAsync(role);
            if (!roleExists)
            {
                var result = await RoleManager.CreateAsync(new Role(role));
                if (!result.Succeeded)
                {
                    return result;
                }
            }

            return await base.AddToRoleAsync(user, role);
        }

        public override Task<User> FindByNameAsync(string userName)
        {
            return FindByIdentificator(userName);
        }

        public Task<User> FindByIdentificator(string identificator)
        {
            Debug.Assert(!base.Options.User.AllowedUserNameCharacters.Contains('@') &&
                         !base.Options.User.AllowedUserNameCharacters.Contains('+'),
                "User name should not allow @ and + symbols as it may interfer with email address or phone number");

            return Users.SingleOrDefaultAsync(x =>
                x.UserName == identificator ||
                x.Email == identificator ||
                x.PhoneNumber == identificator);
        }

#if DEBUG
        private static readonly string[] TestUserNames =
        {
            "__TestOrg__",
            "__TestCoach__",
            "__TestPlayer__",
            "__TestParent__"
        };

        internal bool IsTestUser(User user)
        {
            if (user == null) return false;
            return TestUserNames.Contains(user.UserName);
        }
#endif

        public Task<bool> ExistsAsync(string userName)
        {
            return Users.AnyAsync(x => x.UserName == userName);
        }

        public async Task<User> CreateAsync(IProfile profile, bool isProfileMoreThen13)
        {
            var userName = profile.Id.ToString();
            while (await ExistsAsync(userName))
            {
                userName = Guid.NewGuid().ToString();
            }
            return await CreateAsync(profile, userName, null, isProfileMoreThen13);
        }

        public async Task<User> CreateAsync(IProfile profile, string userName, string password, bool isProfileMoreThen13)
        {
            var user = new User { UserName = userName, Email = profile.Contact.Email };

            if (string.IsNullOrEmpty(password))
            {
                await CreateAsync(user).ThrowIfFailed();
            }
            else
            {
                await CreateAsync(user, password).ThrowIfFailed();
            }

            await SetUpUser(user, profile, isProfileMoreThen13);

            await AddClaimsAsync(user, profile.ToClaims()).ThrowIfFailed();

            return user;
        }

        private async Task SetUpUser(User user, IProfile profile, bool isProfileMoreThen13)
        {
            await SetUserEmail(user, profile.Contact.Email, isProfileMoreThen13);
            await SetUserPhoneNumber(user, profile.Contact.Phone, isProfileMoreThen13);
        }

        private async Task SetUserEmail(User user, string email, bool isProfileMoreThen13)
        {
            if (!isProfileMoreThen13)
            {
                var confCode = await GenerateEmailConfirmationTokenAsync(user);
                await ConfirmEmailAsync(user, confCode).ThrowIfFailed();
                return;
            }

            if (string.IsNullOrEmpty(email)) return;

            await SetEmailAsync(user, email).ThrowIfFailed();

            var code = await GenerateEmailConfirmationTokenAsync(user);
            await AccountConfirmationSender.SendEmailAsync(user, code);

#if DEBUG
            if (IsTestUser(user))
            {
                await ConfirmEmailAsync(user, code).ThrowIfFailed();
            }
#endif
        }

        private async Task SetUserPhoneNumber(User user, string phone, bool isProfileMoreThen13)
        {
            if (string.IsNullOrEmpty(phone)) return;

            await SetPhoneNumberAsync(user, phone).ThrowIfFailed();

            var code = await GenerateChangePhoneNumberTokenAsync(user, phone);
            await AccountConfirmationSender.SendSmsAsync(user, code);
#if DEBUG
            if (IsTestUser(user))
            {
                await ChangePhoneNumberAsync(user, user.PhoneNumber, code).ThrowIfFailed();
            }
#endif
        }

    }
}
