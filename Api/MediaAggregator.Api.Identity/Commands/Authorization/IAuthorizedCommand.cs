﻿using System.Security.Claims;
using MediaAggregator.Core.Commands;

namespace MediaAggregator.Api.Identity.Commands.Authorization
{
    public interface IAuthorizedCommand : ICommand
    {
        ClaimsPrincipal User { get; set; }
    }
}