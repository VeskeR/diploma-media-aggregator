﻿using System.Security.Claims;
using Newtonsoft.Json;
using MediaAggregator.Api.Identity.Commands.Authorization;
using MediaAggregator.Core.Commands.Profile;

namespace MediaAggregator.Api.Identity.Commands.Account
{
    public class DeleteAccountCommand : DeleteProfileCommand, IAuthorizedCommand
    {
        [JsonIgnore]
        public ClaimsPrincipal User { get; set; }
    }
}
