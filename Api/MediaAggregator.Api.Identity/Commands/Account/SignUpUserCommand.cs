﻿namespace MediaAggregator.Api.Identity.Commands.Account
{
    public class SignUpUserCommand
    {
        public string UserName { get; set; }
        public string Password { get; set; }
    }
}
