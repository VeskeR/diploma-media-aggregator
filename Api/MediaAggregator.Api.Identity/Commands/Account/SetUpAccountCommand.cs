﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MediaAggregator.Core.Commands;
using MediaAggregator.Core.Commands.Profile;

namespace MediaAggregator.Api.Identity.Commands.Account
{
    public class SetUpAccountCommand : CreateProfileCommand
    {
        public SignUpUserCommand User { get; set; }
    }
}
