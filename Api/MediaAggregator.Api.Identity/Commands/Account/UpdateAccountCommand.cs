﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Security.Claims;
using System.Text;
using Newtonsoft.Json;
using MediaAggregator.Api.Identity.Commands.Authorization;
using MediaAggregator.Core.Commands.Profile;

namespace MediaAggregator.Api.Identity.Commands.Account
{
    public class UpdateAccountCommand : UpdateProfileCommand, IAuthorizedCommand
    {
        [EmailAddress]
        public string Email { get; set; }

        public string CurrentPassword { get; set; }
        public string NewPassword { get; set; }
        public string ConfirmNewPassword { get; set; }

        [JsonIgnore]
        public ClaimsPrincipal User { get; set; }
    }
}
