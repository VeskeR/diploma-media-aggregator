﻿using System.ComponentModel.DataAnnotations;
using MediaAggregator.Core.Commands;

namespace MediaAggregator.Api.Identity.Commands.Account.Confirmation
{
    public class ConfirmPhoneCommand : ICommand
    {
        [Required, Phone]
        public string Phone { get; set; }
        [Required]
        public string Code { get; set; }
    }
}