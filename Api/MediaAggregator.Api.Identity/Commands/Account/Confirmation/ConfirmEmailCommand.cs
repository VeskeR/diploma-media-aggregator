﻿using System.ComponentModel.DataAnnotations;
using MediaAggregator.Core.Commands;

namespace MediaAggregator.Api.Identity.Commands.Account.Confirmation
{
    public class ConfirmEmailCommand : ICommand
    {
        [Required, EmailAddress]
        public string Email { get; set; }
        [Required]
        public string Code { get; set; }
    }
}
