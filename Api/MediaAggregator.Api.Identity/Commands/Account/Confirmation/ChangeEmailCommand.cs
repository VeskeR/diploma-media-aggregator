﻿using System.ComponentModel.DataAnnotations;
using System.Security.Claims;
using Newtonsoft.Json;
using MediaAggregator.Api.Identity.Commands.Authorization;
using MediaAggregator.Core.Commands;

namespace MediaAggregator.Api.Identity.Commands.Account.Confirmation
{
    public class ChangeEmailCommand : Core.Commands.Profile.ChangeEmailCommand, IAuthorizedCommand
    {
        [Required]
        public string Code { get; set; }

        [JsonIgnore]
        public ClaimsPrincipal User { get; set; }
    }
}
