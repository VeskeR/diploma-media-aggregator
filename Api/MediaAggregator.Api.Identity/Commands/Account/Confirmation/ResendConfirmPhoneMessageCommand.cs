﻿using System.ComponentModel.DataAnnotations;
using MediaAggregator.Core.Commands;

namespace MediaAggregator.Api.Identity.Commands.Account.Confirmation
{
    public class ResendConfirmPhoneMessageCommand : ICommand
    {
        [Required, Phone]
        public string Phone { get; set; }
    }
}