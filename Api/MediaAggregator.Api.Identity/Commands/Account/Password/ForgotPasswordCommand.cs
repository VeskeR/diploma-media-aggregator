﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;
using MediaAggregator.Core.Commands;

namespace MediaAggregator.Api.Identity.Commands.Account.Password
{
    public class ForgotPasswordCommand : ICommand
    {
        [Required]
        public string Identificator { get; set; }
    }
}
