﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;
using MediaAggregator.Core.Commands;

namespace MediaAggregator.Api.Identity.Commands.Account.Password
{
    public class ResetPasswordCommand : ICommand
    {
        [Required]
        public string Identificator { get; set; }
        [Required]
        public string Token { get; set; }
        [Required]
        public string Password { get; set; }
    }
}
