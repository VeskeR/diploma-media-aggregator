﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Security.Claims;
using System.Text;
using MediaAggregator.Api.Identity.Commands.Authorization;
using MediaAggregator.Core.Commands;

namespace MediaAggregator.Api.Identity.Commands.Account.Password
{
    public class ChangePasswordCommand : IAuthorizedCommand
    {
        [Required]
        public string OldPassword { get; set; }
        [Required]
        public string NewPassword { get; set; }

        public ClaimsPrincipal User { get; set; }
    }
}
