﻿using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using MediaAggregator.Api.Identity.Data;
using MediaAggregator.Api.Identity.Data.Entities;
using MediaAggregator.Api.Identity.Services;

namespace MediaAggregator.Api.Identity
{
    public class UserManagerFactory
    {
        private readonly UserStoreFactory _userStoreFactory;
        private readonly RoleManagerFactory _roleManagerFactory;
        private readonly IOptions<IdentityOptions> _identityOptions;
        private readonly IPasswordHasher<User> _passwordHasher;
        private readonly IEnumerable<IUserValidator<User>> _userValidators;
        private readonly IEnumerable<IPasswordValidator<User>> _passwordValidators;
        private readonly ILookupNormalizer _keyNormalizer;
        private readonly IdentityErrorDescriber _describer;
        private readonly IServiceProvider _serviceProvider;
        private readonly ILogger<UserManager<User>> _logger;
        private readonly IAccountConfirmationSender _accountConfirmationSender;

        public UserManagerFactory(UserStoreFactory userStoreFactory, 
            RoleManagerFactory roleManagerFactory,
            IOptions<IdentityOptions> identityOptions,
            IPasswordHasher<User> passwordHasher,
            IEnumerable<IUserValidator<User>> userValidators,
            IEnumerable<IPasswordValidator<User>> passwordValidators,
            ILookupNormalizer keyNormalizer, 
            IdentityErrorDescriber describer,
            IServiceProvider serviceProvider,
            ILogger<UserManager<User>> logger,
            IAccountConfirmationSender accountConfirmationSender)
        {
            _userStoreFactory = userStoreFactory;
            _roleManagerFactory = roleManagerFactory;
            _identityOptions = identityOptions;
            _passwordHasher = passwordHasher;
            _userValidators = userValidators;
            _passwordValidators = passwordValidators;
            _keyNormalizer = keyNormalizer;
            _describer = describer;
            _serviceProvider = serviceProvider;
            _logger = logger;
            _accountConfirmationSender = accountConfirmationSender;
        }

        public UserManager<User> Create(IdentityContext context)
        {
            var userStore = _userStoreFactory.Create(context);
            var roleManager = _roleManagerFactory.Create(context);
            return new UserManager(userStore, _identityOptions, 
                _passwordHasher, _userValidators, _passwordValidators,
                _keyNormalizer, _describer, _serviceProvider, _logger, 
                roleManager, _accountConfirmationSender);
        }
    }
}