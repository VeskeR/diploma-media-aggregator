﻿using System;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using MediaAggregator.Api.Identity.CommandHandlers;
using MediaAggregator.Api.Identity.Commands.Authorization;
using MediaAggregator.Api.Identity.Data;
using MediaAggregator.Api.Identity.Data.Entities;
using MediaAggregator.Api.Identity.Extensions;
using MediaAggregator.Core.CommandHandlers;
using MediaAggregator.Core.Commands;
using MediaAggregator.Core.Data;
using MediaAggregator.Core.Enumerations;
using MediaAggregator.Core.Utils;

namespace MediaAggregator.Api.Identity
{
    internal class CommandAuthorizator<TCommand> where TCommand : ICommand
    {
        private readonly ClaimsPrincipal _user;
        private readonly IUserClaimsPrincipalFactory<User> _claimsFactory;

        public CommandAuthorizator(ClaimsPrincipal user, IUserClaimsPrincipalFactory<User> claimsFactory)
        {
            _user = user;
            _claimsFactory = claimsFactory;
        }

        public async Task AuthorizeAsync(TCommand command, ICommandContext context)
        {
            var authorizedCommand = command as IAuthorizedCommand;
            if (authorizedCommand != null)
            {
                authorizedCommand.User = _user;
            }

            if (_user != null && _user.IsInRole(Role.Admin)) return;

            if (command is IProfileCommand profileCommand)
            {
                if (_user == null)
                {
                    profileCommand.ProfileId = null;
                    profileCommand.ProfileType = null;
                }
                else
                {
                    profileCommand.ProfileType = _user.GetProfileType();
                    if (!IsParentCommand(profileCommand))
                    {
                        profileCommand.ProfileId = _user.GetProfileId();
                    }
                    else
                    {
                        var parentId = _user.GetProfileId();
                        var child = await context.Db.Profiles.FindByIdAsync(profileCommand.ProfileId);
                        if (child != null && child.ParentId == parentId)
                        {
                            profileCommand.ProfileType = ProfileType.Player;
                            if (authorizedCommand != null)
                            {
                                var childUser = await context.IdentityDb().Users.SingleOrDefaultAsync(
                                    u => u.Claims.Any(
                                        x => x.ClaimType == CustomClaimTypes.ProfileId &&
                                             x.ClaimValue == profileCommand.ProfileId.ToString()));
                                authorizedCommand.User = await _claimsFactory.CreateAsync(childUser);
                            }
                        }
                        else
                        {
                            profileCommand.ProfileId = parentId;
                        }
                    }
                }
            }
        }

        private bool IsParentCommand(IProfileCommand profileCommand)
        {
            return !profileCommand.ProfileId.IsNullOrEmpty() && profileCommand.ProfileType == ProfileType.Parent;
        }
    }
}
