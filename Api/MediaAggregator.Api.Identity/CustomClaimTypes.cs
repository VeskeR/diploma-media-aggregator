﻿namespace MediaAggregator.Api.Identity
{
    public static class CustomClaimTypes
    {
        public const string ProfileId = "profile-id";
        public const string ProfileType = "profile-type";
    }
}
