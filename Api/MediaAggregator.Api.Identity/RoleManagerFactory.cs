using System.Collections.Generic;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Logging;
using MediaAggregator.Api.Identity.Data;
using MediaAggregator.Api.Identity.Data.Entities;

namespace MediaAggregator.Api.Identity
{
    public class RoleManagerFactory
    {
        private readonly RoleStoreFactory _roleStoreFactory;
        private readonly IEnumerable<IRoleValidator<Role>> _roleValidators;
        private readonly ILookupNormalizer _keyNormalizer;
        private readonly IdentityErrorDescriber _describer;
        private readonly ILogger<RoleManager<Role>> _logger;

        public RoleManagerFactory(RoleStoreFactory roleStoreFactory, 
            IEnumerable<IRoleValidator<Role>> roleValidators,
            ILookupNormalizer keyNormalizer,
            IdentityErrorDescriber describer,
            ILogger<RoleManager<Role>> logger)
        {
            _roleStoreFactory = roleStoreFactory;
            _roleValidators = roleValidators;
            _keyNormalizer = keyNormalizer;
            _describer = describer;
            _logger = logger;
        }

        public RoleManager<Role> Create(IdentityContext context)
        {
            var store = _roleStoreFactory.Create(context);
            return new RoleManager<Role>(store, _roleValidators, _keyNormalizer, _describer, _logger);
        }
    }
}