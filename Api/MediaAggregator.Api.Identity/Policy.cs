﻿namespace MediaAggregator.Api.Identity
{
    public static class Policy
    {
        public const string Organization = "Organization";
        public const string Player = "Player";
        public const string Parent = "Parent";
        public const string Coach = "Coach";
        public const string Fan = "Fan";
        public const string PlayerOrParentOrFan = "PlayerOrParentOrFan";
        public const string CoachOrOrganization = "CoachOrOrganization";
    }
}
