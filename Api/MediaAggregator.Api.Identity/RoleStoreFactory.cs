﻿using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using MediaAggregator.Api.Identity.Data;
using MediaAggregator.Api.Identity.Data.Entities;

namespace MediaAggregator.Api.Identity
{
    public class RoleStoreFactory
    {
        private readonly IdentityErrorDescriber _describer;

        public RoleStoreFactory(IdentityErrorDescriber describer)
        {
            _describer = describer;
        }

        public IRoleStore<Role> Create(IdentityContext context)
        {
            return new RoleStore<Role>(context, _describer);
        }
    }
}