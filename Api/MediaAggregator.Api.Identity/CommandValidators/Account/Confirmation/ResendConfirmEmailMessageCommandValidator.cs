using System;
using MediaAggregator.Api.Identity.Commands.Account;
using MediaAggregator.Api.Identity.Commands.Account.Confirmation;
using MediaAggregator.Core.CommandValidators;

namespace MediaAggregator.Api.Identity.CommandValidators.Account.Confirmation
{
    public class ResendConfirmEmailMessageCommandValidator : CommandValidator<ResendConfirmEmailMessageCommand>
    {
        public override void Validate(ResendConfirmEmailMessageCommand command)
        {
            base.Validate(command);
            if (string.IsNullOrEmpty(command.Email)) throw new ArgumentNullException(nameof(command.Email));
        }
    }
}