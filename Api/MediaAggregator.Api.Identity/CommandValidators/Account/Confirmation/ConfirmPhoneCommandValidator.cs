﻿using System;
using MediaAggregator.Api.Identity.Commands.Account.Confirmation;
using MediaAggregator.Core.CommandValidators;

namespace MediaAggregator.Api.Identity.CommandValidators.Account.Confirmation
{
    public class ConfirmPhoneCommandValidator : CommandValidator<ConfirmPhoneCommand>
    {
        public override void Validate(ConfirmPhoneCommand command)
        {
            base.Validate(command);
            if (string.IsNullOrEmpty(command.Phone)) throw new ArgumentNullException(nameof(command.Phone));
            if (string.IsNullOrEmpty(command.Code)) throw new ArgumentNullException(nameof(command.Code));
        }
    }
}
