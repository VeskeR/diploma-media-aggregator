﻿using System;
using System.Collections.Generic;
using System.Text;
using MediaAggregator.Api.Identity.Commands.Account;
using MediaAggregator.Api.Identity.Commands.Account.Confirmation;
using MediaAggregator.Core.CommandValidators;

namespace MediaAggregator.Api.Identity.CommandValidators.Account.Confirmation
{
    public class ChangeEmailCommandValidator : IdentityCommandValidator<ChangeEmailCommand>
    {
        private readonly ICommandValidator<Core.Commands.Profile.ChangeEmailCommand> _baseValidator;

        public ChangeEmailCommandValidator(ICommandValidator<Core.Commands.Profile.ChangeEmailCommand> baseValidator)
        {
            _baseValidator = baseValidator;
        }

        public override void Validate(ChangeEmailCommand command)
        {
            base.Validate(command);
            if (string.IsNullOrEmpty(command.Email)) throw new ArgumentNullException(nameof(command.Email));
            if (string.IsNullOrEmpty(command.Code)) throw new ArgumentNullException(nameof(command.Code));
            _baseValidator.Validate(command);
        }
    }
}
