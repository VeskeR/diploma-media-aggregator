using System;
using MediaAggregator.Api.Identity.Commands.Account.Confirmation;
using MediaAggregator.Core.CommandValidators;

namespace MediaAggregator.Api.Identity.CommandValidators.Account.Confirmation
{
    public class ResendConfirmPhoneMessageCommandValidator : CommandValidator<ResendConfirmPhoneMessageCommand>
    {
        public override void Validate(ResendConfirmPhoneMessageCommand command)
        {
            base.Validate(command);
            if (string.IsNullOrEmpty(command.Phone)) throw new ArgumentNullException(nameof(command.Phone));
        }
    }
}