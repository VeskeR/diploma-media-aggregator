﻿using System;
using System.Collections.Generic;
using System.Text;
using MediaAggregator.Api.Identity.Commands.Account;
using MediaAggregator.Api.Identity.Commands.Account.Confirmation;
using MediaAggregator.Core.CommandValidators;

namespace MediaAggregator.Api.Identity.CommandValidators.Account.Confirmation
{
    public class ConfirmEmailCommandValidator : CommandValidator<ConfirmEmailCommand>
    {
        public override void Validate(ConfirmEmailCommand command)
        {
            base.Validate(command);
            if (string.IsNullOrEmpty(command.Email)) throw new ArgumentNullException(nameof(command.Email));
            if (string.IsNullOrEmpty(command.Code)) throw new ArgumentNullException(nameof(command.Code));
        }
    }
}
