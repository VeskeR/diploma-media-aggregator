﻿using System;
using System.Collections.Generic;
using System.Text;
using MediaAggregator.Api.Identity.Commands.Account;
using MediaAggregator.Core.Commands.Profile;
using MediaAggregator.Core.CommandValidators;

namespace MediaAggregator.Api.Identity.CommandValidators.Account
{
    public class SetUpAccountCommandValidator : CommandValidator<SetUpAccountCommand>
    {
        private readonly ICommandValidator<CreateProfileCommand> _baseValidator;

        public SetUpAccountCommandValidator(ICommandValidator<CreateProfileCommand> baseValidator)
        {
            _baseValidator = baseValidator ?? throw new ArgumentNullException(nameof(baseValidator));
        }

        public override void Validate(SetUpAccountCommand command)
        {
            base.Validate(command);
            if (command.User == null) throw new ArgumentNullException(nameof(command.User));
            _baseValidator.Validate(command);
        }
    }
}
