﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;
using MediaAggregator.Api.Identity.Commands.Account;
using MediaAggregator.Core.Commands.Profile;
using MediaAggregator.Core.CommandValidators;

namespace MediaAggregator.Api.Identity.CommandValidators.Account
{
    public class UpdateAccountCommandValidator : IdentityCommandValidator<UpdateAccountCommand>
    {
        private readonly ICommandValidator<UpdateProfileCommand> _baseValidator;

        public UpdateAccountCommandValidator(ICommandValidator<UpdateProfileCommand> baseValidator)
        {
            _baseValidator = baseValidator ?? throw new ArgumentNullException(nameof(baseValidator));
        }

        public override void Validate(UpdateAccountCommand command)
        {
            base.Validate(command);

            if (string.IsNullOrEmpty(command.CurrentPassword) &&
                (!string.IsNullOrEmpty(command.NewPassword) ||
                 !string.IsNullOrEmpty(command.ConfirmNewPassword)))
            {
                throw new ArgumentException("Current password is not provided", nameof(command.CurrentPassword));
            }

            if (!string.Equals(command.NewPassword, command.ConfirmNewPassword))
                throw new ArgumentException("Passwords don't match");

            _baseValidator.Validate(command);
        }
    }
}
