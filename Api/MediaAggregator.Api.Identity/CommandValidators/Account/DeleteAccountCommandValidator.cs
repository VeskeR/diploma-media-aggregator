﻿using System;
using System.Collections.Generic;
using System.Text;
using MediaAggregator.Api.Identity.Commands.Account;
using MediaAggregator.Api.Identity.Data.Entities;
using MediaAggregator.Core.Commands.Profile;
using MediaAggregator.Core.CommandValidators;

namespace MediaAggregator.Api.Identity.CommandValidators.Account
{
    public class DeleteAccountCommandValidator : IdentityCommandValidator<DeleteAccountCommand>
    {
        private readonly ICommandValidator<DeleteProfileCommand> _baseValidator;

        public DeleteAccountCommandValidator(ICommandValidator<DeleteProfileCommand> baseValidator)
        {
            _baseValidator = baseValidator ?? throw new ArgumentNullException(nameof(baseValidator));
        }

        public override void Validate(DeleteAccountCommand command)
        {
            base.Validate(command);
            if (command.User.IsInRole(Role.Admin)) throw new ArgumentException("Admin cannot delete himself", nameof(command));
            _baseValidator.Validate(command);
        }
    }
}
