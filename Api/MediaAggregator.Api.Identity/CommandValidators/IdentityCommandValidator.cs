﻿using System;
using System.Collections.Generic;
using System.Text;
using MediaAggregator.Api.Identity.Commands.Authorization;
using MediaAggregator.Core.CommandValidators;

namespace MediaAggregator.Api.Identity.CommandValidators
{
    public abstract class IdentityCommandValidator<TCommand> : CommandValidator<TCommand>
        where TCommand: IAuthorizedCommand
    {
        public override void Validate(TCommand command)
        {
            base.Validate(command);
            if (command.User == null) throw new UnauthorizedAccessException();
        }
    }
}
