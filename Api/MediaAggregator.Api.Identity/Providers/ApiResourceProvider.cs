﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using IdentityModel;
using IdentityServer4.Models;
using MediaAggregator.Api.Identity;

namespace MediaAggregator.Api.Identity.Providers
{
    internal class ApiResourceProvider
    {
        public static IEnumerable<ApiResource> GetAllResources() => new[]
        {
            new ApiResource("api", "Media Aggregator API", new []
            {
                JwtClaimTypes.Subject,
                JwtClaimTypes.Name,
                ClaimTypes.Role,
                CustomClaimTypes.ProfileId,
                CustomClaimTypes.ProfileType
            }), 
        };
    }
}
