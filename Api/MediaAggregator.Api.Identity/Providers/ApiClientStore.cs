﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using IdentityModel;
using IdentityServer4;
using IdentityServer4.EntityFramework.Entities;
using IdentityServer4.Models;
using IdentityServer4.Stores;
using Client = IdentityServer4.Models.Client;

namespace MediaAggregator.Api.Identity.Providers
{
    public class ApiClientStore : IClientStore
    {
        public static IEnumerable<Client> AllClients { get; } = new[]
        {
            new Client
            {
                ClientId = "web",
                ClientName = "Web Client",
                AccessTokenType = AccessTokenType.Jwt,
                AccessTokenLifetime = 60 * 60 * 24,
                AllowedGrantTypes = { GrantType.ResourceOwnerPassword },
                RequireClientSecret = false,
                AllowedScopes =
                {
                    IdentityServerConstants.StandardScopes.OpenId,
                    IdentityServerConstants.StandardScopes.Profile,
                    "api"
                },
                AllowOfflineAccess = true
            },
            new Client
            {

                ClientId = "swaggerui",
                ClientName = "Swagger UI",
                AllowedGrantTypes = GrantTypes.Implicit,
                AllowAccessTokensViaBrowser = true,

              RedirectUris = { "http://localhost:5000/swagger/o2c.html" },
                PostLogoutRedirectUris = { "http://localhost:5000/swagger/" },

                AllowedScopes =
                {
                    IdentityServerConstants.StandardScopes.OpenId,
                    IdentityServerConstants.StandardScopes.Profile,
                    "api"
                }
            }
            // here mobile client should go
        };

        public Task<Client> FindClientByIdAsync(string clientId)
        {
            return Task.FromResult(AllClients.FirstOrDefault(c => c.ClientId == clientId));
        }
    }
}
