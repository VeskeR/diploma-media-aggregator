﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using IdentityServer4.Models;
using IdentityServer4.Validation;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Identity;
using MediaAggregator.Api.Identity;
using MediaAggregator.Api.Identity.Data.Entities;

namespace MediaAggregator.Api.Identity.Providers
{
    internal class ApiResourceOwnerGrantValidator : IResourceOwnerPasswordValidator
    {
        private const string DefaultErrorMessage = "invalid_username_or_password";

        private static readonly Dictionary<SignInResult, string> Errors = new Dictionary<SignInResult, string>()
        {
            { SignInResult.Failed, "invalid_username_or_password" },
            { SignInResult.LockedOut, "locked_out" },
            { SignInResult.NotAllowed, "not_allowed" },
            { SignInResult.TwoFactorRequired, "two_factor_required" },
        };

        public SignInManager<User> SignInManager { get; }
        public IAuthenticationService AuthenticationService { get; }

        public ApiResourceOwnerGrantValidator(SignInManager<User> signInManager, IAuthenticationService authenticationService)
        {
            SignInManager = signInManager;
            AuthenticationService = authenticationService;
        }

        public async Task ValidateAsync(ResourceOwnerPasswordValidationContext context)
        {
            var result = await SignInManager.PasswordSignInAsync(context.UserName, context.Password, lockoutOnFailure: false, isPersistent: true);
            if (result.Succeeded)
            {
                context.Result = new GrantValidationResult(SignInManager.Context.User);
            }
            else
            {
                string message;
                if (!Errors.TryGetValue(result, out message))
                {
                    message = DefaultErrorMessage;
                }
                context.Result = new GrantValidationResult(TokenRequestErrors.InvalidGrant, message);
            }
        }
    }
}
