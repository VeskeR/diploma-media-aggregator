﻿using System;
using System.Security.Claims;

namespace MediaAggregator.Api.Identity.Extensions
{
    public static class ClaimsPrincipleExtensions
    {
        public static Guid FindFirstGuid(this ClaimsPrincipal user, string claimType)
        {
            var claim = user?.FindFirstValue(claimType);
            return Guid.TryParse(claim, out Guid guid) ? guid : Guid.Empty;
        }
    }
}
