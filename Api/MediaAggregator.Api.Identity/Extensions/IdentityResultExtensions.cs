﻿using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using MediaAggregator.Api.Identity.Exceptions;

namespace MediaAggregator.Api.Identity.Extensions
{
    public static class IdentityResultExtensions
    {
        public static void ThrowIfFailed(this IdentityResult result)
        {
            if (!result.Succeeded)
            {
                throw new IdentityException(result.Errors.Select(x => x.Description).ToArray());
            }
        }

        public static async Task ThrowIfFailed(this Task<IdentityResult> resultTask)
        {
            ThrowIfFailed(await resultTask);
        }
    }
}
