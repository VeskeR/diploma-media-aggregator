﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using IdentityModel;
using IdentityServer4.Validation;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Authorization.Infrastructure;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using MediaAggregator.Api.Identity.CommandHandlers;
using MediaAggregator.Api.Identity.CommandHandlers.Account;
using MediaAggregator.Api.Identity.Data;
using MediaAggregator.Api.Identity.Data.Entities;
using MediaAggregator.Api.Identity.Extensions;
using MediaAggregator.Api.Identity.Providers;
using MediaAggregator.Core.Enumerations;

namespace MediaAggregator.Api.Identity
{
    public static class ServiceCollectionExtensions
    {
        public static IServiceCollection AddIdentity(this IServiceCollection services, string serverUrl)
        {
            services.AddIdentityCore<User, Role>()
                .AddRoleStore<RoleStore<Role, IdentityContext>>()
                .AddRoleManager<RoleManager<Role>>()
                .AddEntityFrameworkStores<IdentityContext>()
                .AddDefaultTokenProviders()
                .AddUserManager<UserManager>()
                .AddUserStore<UserStore>()
                .AddSignInManager<SignInManager>();

            services.AddAuthentication(options =>
            {
                options.DefaultScheme = JwtBearerDefaults.AuthenticationScheme;
                options.DefaultAuthenticateScheme = options.DefaultScheme;
                options.DefaultChallengeScheme = options.DefaultScheme;
                options.DefaultSignInScheme = options.DefaultScheme;
            }).AddJwtBearer(o => {
                o.Audience = "api";
                o.Authority = serverUrl;
                o.RequireHttpsMetadata = false;
                o.Events = new JwtBearerEvents()
                {
                    OnMessageReceived = context =>
                    {
                        context.Token = context.Request.Query["access_token"];
                        return Task.CompletedTask;
                    }
                };
            });

            services
                .AddIdentityServer(options =>
                {
                    options.Events.RaiseErrorEvents = true;
                    options.Events.RaiseFailureEvents = true;
                    options.Events.RaiseInformationEvents = true;
                    options.IssuerUri = serverUrl;
                })
                .AddDeveloperSigningCredential()
                .AddJwtBearerClientAuthentication()
                .AddInMemoryApiResources(ApiResourceProvider.GetAllResources())
                .AddClientStore<ApiClientStore>()
                .AddAspNetIdentity<User>();

            services.AddTransient<IdentityCommandHandlerServiceProvider>()
                .AddTransient<UserStoreFactory>()
                .AddTransient<RoleStoreFactory>()
                .AddTransient<UserManagerFactory>()
                .AddTransient<RoleManagerFactory>();

            services.Configure<IdentityOptions>(options =>
            {
                options.SignIn.RequireConfirmedEmail = true;
                options.User.RequireUniqueEmail = false;
                options.User.AllowedUserNameCharacters = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789-._";
                options.Password.RequireUppercase = false;
                options.Password.RequireNonAlphanumeric = false;
                options.ClaimsIdentity.RoleClaimType = ClaimTypes.Role;
                options.ClaimsIdentity.UserIdClaimType = ClaimTypes.NameIdentifier;
                options.ClaimsIdentity.UserNameClaimType = JwtClaimTypes.Name;
            });

            services.Configure<AuthorizationOptions>(options =>
            {
                options.AddPolicy(Policy.Organization, policy => policy
                    .AddAuthenticationSchemes(JwtBearerDefaults.AuthenticationScheme)
                    .RequireAuthenticatedUser()
                    .RequireClaim(CustomClaimTypes.ProfileType, ProfileType.Organization.ToString()));

                options.AddPolicy(Policy.Coach, policy => policy
                    .AddAuthenticationSchemes(JwtBearerDefaults.AuthenticationScheme)
                    .RequireAuthenticatedUser()
                    .RequireClaim(CustomClaimTypes.ProfileType, ProfileType.Coach.ToString()));

                options.AddPolicy(Policy.Player, policy => policy
                    .AddAuthenticationSchemes(JwtBearerDefaults.AuthenticationScheme)
                    .RequireAuthenticatedUser()
                    .RequireClaim(CustomClaimTypes.ProfileType, ProfileType.Player.ToString()));

                options.AddPolicy(Policy.Parent, policy => policy
                    .AddAuthenticationSchemes(JwtBearerDefaults.AuthenticationScheme)
                    .RequireAuthenticatedUser()
                    .RequireClaim(CustomClaimTypes.ProfileType, ProfileType.Parent.ToString()));

                options.AddPolicy(Policy.Fan, policy => policy
                    .AddAuthenticationSchemes(JwtBearerDefaults.AuthenticationScheme)
                    .RequireAuthenticatedUser()
                    .RequireClaim(CustomClaimTypes.ProfileType, ProfileType.Fan.ToString()));

                options.AddPolicy(Policy.PlayerOrParentOrFan, policy => policy
                    .AddAuthenticationSchemes(JwtBearerDefaults.AuthenticationScheme)
                    .RequireAuthenticatedUser()
                    .RequireClaim(CustomClaimTypes.ProfileType, ProfileType.Player.ToString(), ProfileType.Parent.ToString(), ProfileType.Fan.ToString()));

                options.AddPolicy(Policy.CoachOrOrganization, policy => policy
                    .AddAuthenticationSchemes(JwtBearerDefaults.AuthenticationScheme)
                    .RequireAuthenticatedUser()
                    .RequireClaim(CustomClaimTypes.ProfileType, ProfileType.Coach.ToString(), ProfileType.Organization.ToString()));
            });

            return services;
        }
    }
}
