﻿using System;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using MediaAggregator.Api.Identity.Data;
using MediaAggregator.Api.Identity.Data.Entities;
using MediaAggregator.Core.CommandHandlers;
using MediaAggregator.Core.Commands;

namespace MediaAggregator.Api.Identity.CommandHandlers.Authorization
{
    internal class AuthorizeCommandHandler<TCommand> : CommandHandlerDecorator<TCommand> where TCommand : ICommand
    {
        private readonly CommandAuthorizator<TCommand> _authorizator;

        public AuthorizeCommandHandler(ICommandHandler<TCommand> commandHandler, ClaimsPrincipal user, IUserClaimsPrincipalFactory<User> claimsPrincipalFactory)
            : base(commandHandler)
        {
            _authorizator = new CommandAuthorizator<TCommand>(user, claimsPrincipalFactory);
        }

        public override async Task ExecuteAsync(TCommand command, ICommandContext context)
        {
            await _authorizator.AuthorizeAsync(command, context);
            await base.ExecuteAsync(command, context);
        }
    }

    internal class AuthorizeCommandHandler<TCommand, TResult> : CommandHandlerDecorator<TCommand, TResult> where TCommand : ICommand
    {
        private readonly CommandAuthorizator<TCommand> _authorizator;

        public AuthorizeCommandHandler(ICommandHandler<TCommand, TResult> commandHandler, ClaimsPrincipal user, IUserClaimsPrincipalFactory<User> claimsPrincipalFactory)
            : base(commandHandler)
        {
            _authorizator = new CommandAuthorizator<TCommand>(user, claimsPrincipalFactory);
        }

        public override async Task<TResult> ExecuteAsync(TCommand command, ICommandContext context)
        {
            await _authorizator.AuthorizeAsync(command, context);
            return await base.ExecuteAsync(command, context);
        }
    }
}