﻿using System.Collections.Generic;
using System.Text;
using Microsoft.Extensions.Logging;
using MediaAggregator.Core.CommandHandlers;

namespace MediaAggregator.Api.Identity.CommandHandlers
{
    public class IdentityCommandHandlerServiceProvider
    {
        public UserManagerFactory UserManagerFactory { get; }
        public ILoggerFactory LoggerFactory { get; }

        public ILogger AccountLogger { get; }

        public IdentityCommandHandlerServiceProvider(UserManagerFactory userManagerFactory, ILoggerFactory loggerFactory)
        {
            UserManagerFactory = userManagerFactory;
            LoggerFactory = loggerFactory;

            AccountLogger = loggerFactory.CreateLogger("Account");
        }
    }
}
