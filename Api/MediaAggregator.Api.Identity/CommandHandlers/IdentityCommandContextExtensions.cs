﻿using System;
using System.Collections.Generic;
using System.Text;
using MediaAggregator.Api.Identity.Data;
using MediaAggregator.Core.CommandHandlers;

namespace MediaAggregator.Api.Identity.CommandHandlers
{
    internal static class IdentityCommandContextExtensions
    {
        public static IdentityContext IdentityDb(this ICommandContext context)
        {
            return (IdentityContext) context.Db;
        }
    }
}
