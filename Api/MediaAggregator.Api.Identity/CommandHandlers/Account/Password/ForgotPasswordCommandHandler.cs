﻿using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using MediaAggregator.Api.Identity.Commands.Account;
using MediaAggregator.Api.Identity.Commands.Account.Password;
using MediaAggregator.Api.Identity.Data.Entities;
using MediaAggregator.Api.Identity.Extensions;
using MediaAggregator.Api.Identity.Services;
using MediaAggregator.Core.CommandHandlers;

namespace MediaAggregator.Api.Identity.CommandHandlers.Account.Password
{
    public class ForgotPasswordCommandHandler : IdentityCommandHandlerBase, ICommandHandler<ForgotPasswordCommand>
    {
        public ILogger Logger { get; }
        public IResetPasswordTokenSender Sender { get; }

        public ForgotPasswordCommandHandler(IdentityCommandHandlerServiceProvider services, IResetPasswordTokenSender sender)
            : base(services)
        {
            Sender = sender ?? throw new ArgumentNullException(nameof(sender));
            Logger = Services.AccountLogger;
        }

        public async Task ExecuteAsync(ForgotPasswordCommand command, ICommandContext context)
        {
            var userManager = CreateUserManager(context);
            var user = await userManager.FindByNameAsync(command.Identificator);
            if (user == null)
            {
                Logger.LogWarning($"Forgot password command failed. User not found by identificator: {command.Identificator}.");
                return;
            }
            var token = await userManager.GeneratePasswordResetTokenAsync(user);
            if (user.EmailConfirmed && string.Equals(user.Email, command.Identificator,
                    StringComparison.OrdinalIgnoreCase))
            {
                await Sender.SendEmailAsync(user, token);
                Logger.LogInformation($"Reset password token sent to {user.Email}");
            }
            else if (user.PhoneNumberConfirmed && string.Equals(user.PhoneNumber, command.Identificator,
                         StringComparison.OrdinalIgnoreCase))
            {
                await Sender.SendSmsAsync(user, token);
                Logger.LogInformation($"Reset password token sent to {user.PhoneNumber}");
            }
            else
            {
                Logger.LogWarning($"Forgot password command failed. User '{command.Identificator}' has no confirmed contacts.");
            }
        }
    }
}
