﻿using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using MediaAggregator.Api.Identity.Commands.Account;
using MediaAggregator.Api.Identity.Commands.Account.Password;
using MediaAggregator.Api.Identity.Data.Entities;
using MediaAggregator.Api.Identity.Extensions;
using MediaAggregator.Api.Identity.Services;
using MediaAggregator.Core.CommandHandlers;

namespace MediaAggregator.Api.Identity.CommandHandlers.Account.Password
{
    public class ResetPasswordCommandHandler : IdentityCommandHandlerBase, ICommandHandler<ResetPasswordCommand>
    {
        public ILogger Logger { get; }

        public ResetPasswordCommandHandler(IdentityCommandHandlerServiceProvider services)
            : base(services)
        {
            Logger = Services.AccountLogger;
        }

        public async Task ExecuteAsync(ResetPasswordCommand command, ICommandContext context)
        {
            var userManager = CreateUserManager(context);
            var user = await userManager.FindByNameAsync(command.Identificator);
            if (user == null)
            {
                Logger.LogWarning($"Reset password command failed. User not found by identificator: {command.Identificator}.");
                return;
            }
            await userManager.ResetPasswordAsync(user, command.Token, command.Password).ThrowIfFailed();
            Logger.LogInformation($"Password has been reset for {user.UserName}");
        }
    }
}
