﻿using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using MediaAggregator.Api.Identity.Commands.Account;
using MediaAggregator.Api.Identity.Commands.Account.Password;
using MediaAggregator.Api.Identity.Data.Entities;
using MediaAggregator.Api.Identity.Extensions;
using MediaAggregator.Api.Identity.Services;
using MediaAggregator.Core.CommandHandlers;

namespace MediaAggregator.Api.Identity.CommandHandlers.Account.Password
{
    public class ChangePasswordCommandHandler : IdentityCommandHandlerBase, ICommandHandler<ChangePasswordCommand>
    {
        public ILogger Logger { get; }

        public ChangePasswordCommandHandler(IdentityCommandHandlerServiceProvider services)
            : base(services)
        {
            Logger = Services.AccountLogger;
        }

        public async Task ExecuteAsync(ChangePasswordCommand command, ICommandContext context)
        {
            var userManager = CreateUserManager(context);
            var user = await userManager.GetUserAsync(command.User);
            if (user == null) throw new ArgumentException("Failed to find user", nameof(command.User));

            await userManager.ChangePasswordAsync(user, command.OldPassword, command.NewPassword).ThrowIfFailed();
            Logger.LogInformation($"Password has been changed for {user.UserName}");
        }
    }
}
