﻿using System;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using MediaAggregator.Api.Identity.Commands.Account;
using MediaAggregator.Core.CommandHandlers;
using MediaAggregator.Core.Commands.Profile;
using MediaAggregator.Core.Data.Entities;
using MediaAggregator.Core.Models;

namespace MediaAggregator.Api.Identity.CommandHandlers.Account
{
    public class SetUpAccountCommandHandler : IdentityCommandHandlerBase, ICommandHandler<SetUpAccountCommand, ProfileDto>
    {
        public ICommandHandler<CreateProfileCommand, ProfileDto> CommandHandler { get; }

        public SetUpAccountCommandHandler(IdentityCommandHandlerServiceProvider services,
            ICommandHandler<CreateProfileCommand, ProfileDto> commandHandler)
            : base(services)
        {
            CommandHandler = commandHandler ?? throw new ArgumentNullException(nameof(commandHandler));
        }

        public async Task<ProfileDto> ExecuteAsync(SetUpAccountCommand command, ICommandContext context)
        {
            using (var transaction = await context.Db.BeginTransactionAsync())
            {
                var profile = await CommandHandler.ExecuteAsync(command, context);
                var isUserMoreThan13 = Profile.IsUserMoreThan13(profile.DateOfBirth);
                var userManager = CreateUserManager(context);

                if (!isUserMoreThan13)
                {
                    command.User.UserName = GenereateChildLogin(profile);
                    command.User.Password = GenerateChildPassword();

                    context.Db.ChildsUnder13.Add(new ChildUnder13()
                    {
                        ProfileId = profile.Id,
                        ParentId = command.ParentId.Value,
                        Password = command.User.Password,
                        UserName = command.User.UserName
                    });
                    profile.ChildPassword = command.User.Password;
                    profile.ChildUserName = command.User.UserName;
                }

                await userManager.CreateAsync(profile, command.User.UserName, command.User.Password, isUserMoreThan13);
                foreach (var child in profile.Children)
                {
                    var childMore13 = Profile.IsUserMoreThan13(profile.DateOfBirth);
                    if (!childMore13)
                    {
                        var userName = GenereateChildLogin(child); ;
                        var pw = GenerateChildPassword();


                        context.Db.ChildsUnder13.Add(new ChildUnder13()
                        {
                            ProfileId = child.Id,
                            ParentId = child.ParentId.Value,
                            Password = pw,
                            UserName = userName
                        });
                        await userManager.CreateAsync(child, userName, pw, false);
                    }
                    await userManager.CreateAsync(child, childMore13);
                }

                transaction.Commit();

                return profile;
            }
        }

        private string GenereateChildLogin(ProfileDto profile)
        {
            var uidLogin = Regex.Replace(Convert.ToBase64String(Guid.NewGuid().ToByteArray()), "[/+=]", "");
            return $"{profile.FirstName}_{profile.ParentId}_{uidLogin}_{profile.LastName}";
        }
        private string GenerateChildPassword()
        {
            return Regex.Replace(Convert.ToBase64String(Guid.NewGuid().ToByteArray()), "[/+=]", "");
        }
    }
}
