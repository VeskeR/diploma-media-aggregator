﻿using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using MediaAggregator.Api.Identity.Commands.Account.Confirmation;
using MediaAggregator.Api.Identity.Data.Entities;
using MediaAggregator.Api.Identity.Services;
using MediaAggregator.Core.CommandHandlers;

namespace MediaAggregator.Api.Identity.CommandHandlers.Account.Confirmation
{
    public class ResendConfirmPhoneMessageCommandHandler : IdentityCommandHandlerBase, ICommandHandler<ResendConfirmPhoneMessageCommand>
    {
        public ILogger Logger { get; }
        public IAccountConfirmationSender Sender { get; }

        public ResendConfirmPhoneMessageCommandHandler(IdentityCommandHandlerServiceProvider services, IAccountConfirmationSender sender)
            : base(services)
        {
            Sender = sender ?? throw new ArgumentNullException(nameof(sender));
            Logger = Services.AccountLogger;
        }

        public async Task ExecuteAsync(ResendConfirmPhoneMessageCommand command, ICommandContext context)
        {
            var userManager = CreateUserManager(context);
            var user = await userManager.Users.SingleOrDefaultAsync(x => x.PhoneNumber == command.Phone);
            if (user == null)
            {
                Logger.LogWarning($"Resend confirmation phone message failed. User not found by phone number: {command.Phone}.");
                return;
            }
            var token = await userManager.GenerateChangePhoneNumberTokenAsync(user, command.Phone);
            await Sender.SendSmsAsync(user, token);
            Logger.LogInformation($"Confirmation SMS message sent to {command.Phone}");
        }
    }
}
