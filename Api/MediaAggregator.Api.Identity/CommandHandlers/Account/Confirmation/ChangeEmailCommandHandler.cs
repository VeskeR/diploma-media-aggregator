﻿using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Logging;
using MediaAggregator.Api.Identity.Commands.Account.Confirmation;
using MediaAggregator.Api.Identity.Data.Entities;
using MediaAggregator.Api.Identity.Extensions;
using MediaAggregator.Core.CommandHandlers;

namespace MediaAggregator.Api.Identity.CommandHandlers.Account.Confirmation
{
    public class ChangeEmailCommandHandler : ICommandHandler<ChangeEmailCommand>
    {
        public UserManagerFactory UserManagerFactory { get; }
        public ICommandHandler<Core.Commands.Profile.ChangeEmailCommand> BaseHandler { get; }
        public ILogger Logger { get; }

        public ChangeEmailCommandHandler(UserManagerFactory userManagerFactory, ILoggerFactory loggerFactory,
            ICommandHandler<Core.Commands.Profile.ChangeEmailCommand> baseHandler)
        {
            UserManagerFactory = userManagerFactory;
            BaseHandler = baseHandler;
            Logger = loggerFactory.CreateLogger("Account");
        }

        public async Task ExecuteAsync(ChangeEmailCommand command, ICommandContext context)
        {
            var userManager = UserManagerFactory.Create(context.IdentityDb());
            var user = await userManager.GetUserAsync(command.User);
            if (user == null) throw new ArgumentException("User not found");
            var oldEmail = user.Email;
            using (var transaction = await context.Db.BeginTransactionAsync())
            {
                await userManager.ChangeEmailAsync(user, command.Email, command.Code).ThrowIfFailed();
                await BaseHandler.ExecuteAsync(command, context);
                transaction.Commit();

                Logger.LogInformation($"Email address '{oldEmail}' changed to '{command.Email}' for user '{user.UserName}'");
            }
        }
    }
}
