﻿using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Logging;
using MediaAggregator.Api.Identity.Commands.Account.Confirmation;
using MediaAggregator.Api.Identity.Data.Entities;
using MediaAggregator.Api.Identity.Extensions;
using MediaAggregator.Core.CommandHandlers;

namespace MediaAggregator.Api.Identity.CommandHandlers.Account.Confirmation
{
    public class ConfirmEmailCommandHandler : ICommandHandler<ConfirmEmailCommand>
    {
        public UserManagerFactory UserManagerFactory { get; }
        public ILogger Logger { get; }

        public ConfirmEmailCommandHandler(UserManagerFactory userManagerFactory, ILoggerFactory loggerFactory)
        {
            UserManagerFactory = userManagerFactory;
            Logger = loggerFactory.CreateLogger("Account");
        }

        public async Task ExecuteAsync(ConfirmEmailCommand command, ICommandContext context)
        {
            var userManager = (UserManager) UserManagerFactory.Create(context.IdentityDb());
            var user = await userManager.FindByEmailAsync(command.Email);
            if (user == null)
            {
                Logger.LogWarning($"Email confirmation failed. User not found by email address: {command.Email}.");
                return;
            }
            await userManager.ConfirmEmailAsync(user, command.Code).ThrowIfFailed();
            Logger.LogInformation($"Email address {command.Email} confirmed");
        }
    }
}
