﻿using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using MediaAggregator.Api.Identity.Commands.Account.Confirmation;
using MediaAggregator.Api.Identity.Data.Entities;
using MediaAggregator.Api.Identity.Extensions;
using MediaAggregator.Core.CommandHandlers;

namespace MediaAggregator.Api.Identity.CommandHandlers.Account.Confirmation
{
    public class ConfirmPhoneCommandHandler : IdentityCommandHandlerBase, ICommandHandler<ConfirmPhoneCommand>
    {
        public ILogger Logger { get; }

        public ConfirmPhoneCommandHandler(IdentityCommandHandlerServiceProvider services)
            : base(services)
        {
            Logger = Services.AccountLogger;
        }

        public async Task ExecuteAsync(ConfirmPhoneCommand command, ICommandContext context)
        {
            var userManager = CreateUserManager(context);
            var user = await userManager.Users.SingleOrDefaultAsync(x => x.PhoneNumber == command.Phone);
            if (user == null)
            {
                Logger.LogWarning($"Phone confirmation failed. User not found by phone number: {command.Phone}.");
                return;
            }
            await userManager.ChangePhoneNumberAsync(user, command.Phone, command.Code).ThrowIfFailed();
            Logger.LogInformation($"Phone number {command.Phone} confirmed");
        }
    }
}
