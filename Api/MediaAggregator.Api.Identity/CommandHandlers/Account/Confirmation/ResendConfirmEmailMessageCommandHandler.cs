﻿using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Logging;
using MediaAggregator.Api.Identity.Commands.Account.Confirmation;
using MediaAggregator.Api.Identity.Data.Entities;
using MediaAggregator.Api.Identity.Services;
using MediaAggregator.Core.CommandHandlers;

namespace MediaAggregator.Api.Identity.CommandHandlers.Account.Confirmation
{
    public class ResendConfirmEmailCommandMessageHandler : ICommandHandler<ResendConfirmEmailMessageCommand>
    {
        public UserManagerFactory UserManagerFactory { get; }
        public ILogger Logger { get; }
        public IAccountConfirmationSender Sender { get; }

        public ResendConfirmEmailCommandMessageHandler(UserManagerFactory userManagerFactory, ILoggerFactory loggerFactory, IAccountConfirmationSender sender)
        {
            UserManagerFactory = userManagerFactory ?? throw new ArgumentNullException(nameof(userManagerFactory));
            Sender = sender ?? throw new ArgumentNullException(nameof(sender));
            Logger = loggerFactory?.CreateLogger("Account") ?? throw new ArgumentNullException(nameof(loggerFactory));
        }

        public async Task ExecuteAsync(ResendConfirmEmailMessageCommand command, ICommandContext context)
        {
            var userManager = UserManagerFactory.Create(context.IdentityDb());
            var user = await userManager.FindByEmailAsync(command.Email);
            if (user == null)
            {
                Logger.LogWarning($"Resend confirmation email message failed. User not found by email address: {command.Email}.");
                return;
            }
            var token = await userManager.GenerateEmailConfirmationTokenAsync(user);
            await Sender.SendEmailAsync(user, token);
            Logger.LogInformation($"Confirmation email message sent to {command.Email}");
        }
    }
}
