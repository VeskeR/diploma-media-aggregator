﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using MediaAggregator.Api.Identity.Data.Entities;
using MediaAggregator.Core.CommandHandlers;
using MediaAggregator.Core.CommandHandlers.Global;
using MediaAggregator.Core.Commands.Global;

namespace MediaAggregator.Api.Identity.CommandHandlers.Account
{
    public class CheckExistingEntityCommandHandlerDecorator : CommandHandlerDecorator<CheckExistingEntityCommand, bool>
    {
        internal UserManager UserManager { get; }

        public CheckExistingEntityCommandHandlerDecorator(UserManager<User> userManager) 
            : base(new CheckExistingEntityCommandHandler())
        {
            UserManager = (UserManager) userManager ?? throw new ArgumentNullException(nameof(userManager));
        }

        public override Task<bool> ExecuteAsync(CheckExistingEntityCommand command, ICommandContext context)
        {
            if (command.Category?.ToLower() == "user")
            {
                return UserManager.ExistsAsync(command.Key);
            }
            return base.ExecuteAsync(command, context);
        }
    }
}
