﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Logging;
using MediaAggregator.Api.Identity.Commands.Account;
using MediaAggregator.Api.Identity.Data.Entities;
using MediaAggregator.Api.Identity.Extensions;
using MediaAggregator.Api.Identity.Services;
using MediaAggregator.Core.CommandHandlers;
using MediaAggregator.Core.Commands.Profile;
using MediaAggregator.Core.Models;

namespace MediaAggregator.Api.Identity.CommandHandlers.Account
{
    public class UpdateAccountCommandHandler : IdentityCommandHandlerBase, ICommandHandler<UpdateAccountCommand, ProfileDto>
    {
        private IChangeContactTokenSender ChangeContactTokenSender { get; }
        private ICommandHandler<UpdateProfileCommand, ProfileDto> BaseHandler { get; }
        private ILogger Logger { get; }

        public UpdateAccountCommandHandler(IdentityCommandHandlerServiceProvider services, 
            ICommandHandler<UpdateProfileCommand, ProfileDto> baseHandler,
            IChangeContactTokenSender changeContactTokenSender)
            : base(services)
        {
            ChangeContactTokenSender = changeContactTokenSender ?? throw new ArgumentNullException(nameof(changeContactTokenSender));
            BaseHandler = baseHandler ?? throw new ArgumentNullException(nameof(baseHandler));
            Logger = Services.AccountLogger;
        }

        public async Task<ProfileDto> ExecuteAsync(UpdateAccountCommand command, ICommandContext context)
        {
            var userManager = CreateUserManager(context);
            var user = await userManager.GetUserAsync(command.User);
            if (user == null) throw new ArgumentException("User not found");
            if (user.Email != null && string.IsNullOrWhiteSpace(command.Email))
                throw new ArgumentException("Email Address is empty", nameof(command.Email));

            string token = null;
            bool emailChanged = !string.Equals(user.Email, command.Email, StringComparison.OrdinalIgnoreCase);
            if (emailChanged)
            {
                var existingUser = await userManager.FindByEmailAsync(command.Email);
                if (existingUser != null)
                {
                    throw new ArgumentException($"Email '{command.Email}' is already taken");
                }
            }

            ProfileDto profile;

            bool passwordChanged = !string.IsNullOrWhiteSpace(command.NewPassword);
            using (var transaction = await context.Db.BeginTransactionAsync())
            {
                try
                {
                    if (emailChanged)
                    {
                        token = await userManager.GenerateChangeEmailTokenAsync(user, command.Email);
                    }
                    if (passwordChanged)
                    {
                        await userManager.ChangePasswordAsync(user, command.CurrentPassword, command.NewPassword).ThrowIfFailed();
                    }

                    profile = await BaseHandler.ExecuteAsync(command, context);

                    transaction.Commit();

                    if (passwordChanged)
                    {
                        Logger.LogInformation($"Password has been changed for user '{user.UserName}'");
                    }
                }
                catch (Exception e)
                {
                    throw new ArgumentException("Operation failed", e);
                }
            }

            if (emailChanged)
            {
                await ChangeContactTokenSender.SendEmailAsync(command.Email, token);
                Logger.LogInformation($"Change email address token has been sent to '{command.Email}' for user '{user.UserName}'");
            }

            return profile;
        }
    }
}
