﻿using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using MediaAggregator.Api.Identity.Commands.Account;
using MediaAggregator.Api.Identity.Data.Entities;
using MediaAggregator.Api.Identity.Extensions;
using MediaAggregator.Core.CommandHandlers;
using MediaAggregator.Core.Commands;
using MediaAggregator.Core.Commands.Profile;

namespace MediaAggregator.Api.Identity.CommandHandlers.Account
{
    public class DeleteAccountCommandHandler : IdentityCommandHandlerBase, ICommandHandler<DeleteAccountCommand> 
    {
        public ICommandHandler<DeleteProfileCommand> CommandHandler { get; }

        public DeleteAccountCommandHandler(IdentityCommandHandlerServiceProvider services, ICommandHandler<DeleteProfileCommand> commandHandler)
            : base(services)
        {
            CommandHandler = commandHandler ?? throw new ArgumentNullException(nameof(commandHandler));
        }

        public async Task ExecuteAsync(DeleteAccountCommand command, ICommandContext context)
        {
            var userManager = CreateUserManager(context);
            var user = await userManager.GetUserAsync(command.User);
            if (user == null) throw new ArgumentException("User not found", nameof(command));

            using (var transaction = await context.Db.BeginTransactionAsync())
            {
                await userManager.DeleteAsync(user).ThrowIfFailed();

                await CommandHandler.ExecuteAsync(command, context);

                transaction.Commit();
            }
        }
    }
}
