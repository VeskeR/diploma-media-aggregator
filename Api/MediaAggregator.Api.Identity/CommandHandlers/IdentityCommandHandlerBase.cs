﻿using System;
using MediaAggregator.Core.CommandHandlers;

namespace MediaAggregator.Api.Identity.CommandHandlers
{
    public class IdentityCommandHandlerBase
    {
        public IdentityCommandHandlerServiceProvider Services { get; }

        public IdentityCommandHandlerBase(IdentityCommandHandlerServiceProvider services)
        {
            Services = services ?? throw new ArgumentNullException(nameof(services));
        }

        internal UserManager CreateUserManager(ICommandContext context)
        {
            return (UserManager)Services.UserManagerFactory.Create(context.IdentityDb());
        }
    }
}