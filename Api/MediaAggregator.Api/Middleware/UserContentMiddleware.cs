﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.StaticFiles;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Options;
using MediaAggregator.Api.Options;
using MediaAggregator.Core.Services;

namespace MediaAggregator.Api.Middleware
{
    public class UserContentMiddleware
    {
        private readonly IOptions<UserContentOptions> _options;
        private readonly IFileStorage _fileStorage;
        private readonly RequestDelegate _next;

        public UserContentMiddleware(RequestDelegate next, IFileStorage fileStorage, IOptions<UserContentOptions> options)
        {
            _options = options ?? throw new ArgumentNullException(nameof(options));
            _fileStorage = fileStorage ?? throw new ArgumentNullException(nameof(fileStorage));
            _next = next ?? throw new ArgumentNullException(nameof(next));
        }

        private static readonly string[] SupportedHeaders = {HttpMethods.Get, HttpMethods.Head};

        public async Task Invoke(HttpContext context)
        {
            if (context.Request.Path.StartsWithSegments(new PathString($"/{_options.Value.Prefix}"),
                StringComparison.OrdinalIgnoreCase, out PathString remaining))
            {
                var filePath = remaining.ToString().TrimStart('/');

                var queryIndex = filePath.IndexOf("?", StringComparison.Ordinal);
                if (queryIndex > -1)
                {
                    filePath = filePath.Remove(queryIndex);
                }

                if (!SupportedHeaders.Contains(context.Request.Method, StringComparer.OrdinalIgnoreCase))
                {
                    context.Response.StatusCode = StatusCodes.Status404NotFound;
                    return;
                }

                var rangeHeader = context.Request.GetTypedHeaders().Range;
                var isRangeRequest = rangeHeader != null &&
                                     rangeHeader.Ranges.Count == 1 &&
                                     string.Equals(rangeHeader.Unit.ToString(), "bytes",
                                         StringComparison.OrdinalIgnoreCase);

                var metadata = await _fileStorage.GetMetadataAsync(filePath);
                if (metadata.Exists)
                {
                    var provider = new FileExtensionContentTypeProvider();

                    context.Response.Headers.Add("Accept-Ranges", "bytes");
                    context.Response.ContentLength = metadata.Size;
                    if (!provider.TryGetContentType(filePath, out string contentType))
                    {
                        contentType = "application/octet-stream";
                    }
                    context.Response.ContentType = contentType;

                    context.Response.StatusCode = isRangeRequest
                        ? StatusCodes.Status206PartialContent
                        : StatusCodes.Status200OK;

                    if (context.Request.Method == HttpMethods.Get)
                    {
                        ByteRange byteRange = null;
                        if (isRangeRequest)
                        {
                            var ranges = rangeHeader.Ranges.Single();
                            byteRange = new ByteRange(ranges.From, ranges.To);
                            context.Response.Headers.Add("Content-Range", 
                                $"bytes {ranges.From ?? 0}-{ranges.To ?? metadata.Size - 1}/{metadata.Size}");
                            context.Response.ContentLength = byteRange.GetLength(metadata.Size);
                        }
                        using (var stream = await _fileStorage.GetAsync(filePath, byteRange))
                        {
                            await stream.CopyToAsync(context.Response.Body);
                        }
                    }
                }
                else
                {
                    context.Response.StatusCode = StatusCodes.Status404NotFound;
                }
            }
            else
            {
                // Call the next delegate/middleware in the pipeline
                await this._next(context);
            }
        }
    }
}
