﻿using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using MediaAggregator.Api.Filters;
using MediaAggregator.Core.Commands.Post;
using MediaAggregator.Core.Models;

namespace MediaAggregator.Api.Controllers
{
    [Produces("application/json")]
    [Route("api/post")]
    public class PostController : MediaAggregatorController
    {
        [HttpGet]
        public Task<DataListDto<PostDto>> GetPosts([FromQuery] GetPostsCommand command) =>
            ExecuteAsync<GetPostsCommand, DataListDto<PostDto>>(command);

        [HttpPost]
        public Task<PostDto> Publish([FromBody] PublishPostCommand command) =>
            ExecuteAsync<PublishPostCommand, PostDto>(command);

        [HttpPost("comment")]
        public Task<PostCommentDto> Comment([FromBody] AddPostCommentCommand command) =>
            ExecuteAsync<AddPostCommentCommand, PostCommentDto>(command);

        [HttpPost("edit")]
        public Task<PostDto> Edit([FromBody] EditPostCommand command) => ExecuteAsync<EditPostCommand, PostDto>(command);

        [HttpPost("{id:guid}/remove")]
        [NoContent]
        public Task RemoveAsync(Guid id) => ExecuteAsync(new DeletePostCommand(id));
    }
}