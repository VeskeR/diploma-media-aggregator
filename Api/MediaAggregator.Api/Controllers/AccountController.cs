﻿using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using MediaAggregator.Api.Filters;
using MediaAggregator.Api.Identity.Commands.Account;
using MediaAggregator.Api.Identity.Commands.Account.Confirmation;
using MediaAggregator.Api.Identity.Commands.Account.Password;
using MediaAggregator.Core.CommandHandlers;
using MediaAggregator.Core.Commands.Global;
using MediaAggregator.Core.Models;

namespace MediaAggregator.Api.Controllers
{
    [Produces("application/json")]
    [Route("api/account")]
    public class AccountController : MediaAggregatorController
    {

        /// <summary>
        /// Creation of user
        /// </summary>
        /// <param name="command"></param>
        /// <returns></returns>
        [AllowAnonymous]
        [HttpPost]
        [Created]
        public Task<ProfileDto> SetUp([FromBody] SetUpAccountCommand command) => ExecuteAsync<SetUpAccountCommand, ProfileDto>(command);

        /// <summary>
        /// Cheking of existed user by specific parameters 
        /// </summary>
        /// <param name="command"></param>
        /// <returns></returns>
        [AllowAnonymous]
        [HttpGet("check-existing")]
        public Task<JsonResult> CheckExistingAsync([FromQuery] CheckExistingEntityCommand command)
        {
            return ExecuteAsync<CheckExistingEntityCommand, bool>(command)
                .ContinueWith(x => new JsonResult(
                    new { exists = x.Result },
                    new JsonSerializerSettings() { DefaultValueHandling = DefaultValueHandling.Include }));
        }

        /// <summary>
        /// Confirms email by link from email
        /// </summary>
        /// <param name="command"></param>
        /// <returns></returns>
        [AllowAnonymous]
        [HttpPost("confirm-email")]
        [NoContent]
        public Task ConfirmEmailAsync([FromBody] ConfirmEmailCommand command) => ExecuteAsync(command);

        [AllowAnonymous]
        [HttpPost("confirm-email/resend")]
        [NoContent]
        public Task ResendConfirmEmailMessageAsync([FromBody] ResendConfirmEmailMessageCommand command) => ExecuteAsync(command);

        [AllowAnonymous]
        [HttpPost("confirm-phone")]
        [NoContent]
        public Task ConfirmPhoneAsync([FromBody] ConfirmPhoneCommand command) => ExecuteAsync(command);

        [AllowAnonymous]
        [HttpPost("confirm-phone/resend")]
        [NoContent]
        public Task ResendConfirmPhoneMessageAsync([FromBody] ResendConfirmPhoneMessageCommand command) => ExecuteAsync(command);

        [AllowAnonymous]
        [HttpPost("forgot-password")]
        [NoContent]
        public Task ForgotPasswordAsync([FromBody] ForgotPasswordCommand command) => ExecuteAsync(command);

        [AllowAnonymous]
        [HttpPost("reset-password")]
        [NoContent]
        public Task ResetPasswordAsync([FromBody] ResetPasswordCommand command) => ExecuteAsync(command);

        [Authorize]
        [HttpPost("change-email")]
        [NoContent]
        public Task ChangeEmailAsync([FromBody] ChangeEmailCommand command) => ExecuteAsync(command);

        [Authorize]
        [HttpPost("change-password")]
        [NoContent]
        public Task ChangePasswordAsync([FromBody] ChangePasswordCommand command) => ExecuteAsync(command);

#if DEBUG

        [HttpGet("")]
        public JsonResult Get()
        {
            return Json(new
            {
                isAuthenticated = User.Identity.IsAuthenticated,
                name = User.Identity.Name,
                claims = User.Claims.Select(x => string.Join(" : ", x.Type, x.Value)).ToArray()
            });
        }


        [HttpDelete]
        [NoContent]
        public Task DeleteAccountAsync()
        {
            return ExecuteAsync<DeleteAccountCommand>();
        }

#endif
    }
}