﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using MediaAggregator.Api.Identity.Commands.Account;
using MediaAggregator.Core.Commands.Profile;
using MediaAggregator.Core.Models;

namespace MediaAggregator.Api.Controllers
{
    [Produces("application/json")]
    [Route("api/profile")]
    public class ProfileController : MediaAggregatorController
    {
        [HttpGet]
        public Task<ProfileDto> GetMyProfile() => ExecuteAsync<GetMyProfileCommand, ProfileDto>();

        [HttpGet("{id}")]
        public Task<ProfileDto> GetProfile(string id)
            => ExecuteAsync<GetProfileCommand, ProfileDto>(new GetProfileCommand(id));

        [HttpPost]
        public Task<ProfileDto> Update([FromBody] UpdateAccountCommand command)
            => ExecuteAsync<UpdateAccountCommand, ProfileDto>(command);

        [HttpPost("avatar")]
        public Task<AvatarDto> UpdateAvatar([FromBody] UpdateAvatarCommand command) =>
            ExecuteAsync<UpdateAvatarCommand, AvatarDto>(command);

        [HttpPost("background-image")]
        public Task<MediaDto> UpdateBackgroundImage([FromBody] UpdateBackgroundImageCommand command) =>
            ExecuteAsync<UpdateBackgroundImageCommand, MediaDto>(command);

        [HttpPost("background-image/remove")]
        public Task RemoveBackgroundImage() => ExecuteAsync<RemoveBackgroundImageCommand>();
    }
}