﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.DependencyInjection;
using MediaAggregator.Api.Data;
using MediaAggregator.Api.Services;
using MediaAggregator.Core.CommandHandlers;
using MediaAggregator.Core.Commands;

namespace MediaAggregator.Api.Controllers
{
    public abstract class MediaAggregatorController : Controller
    {
        private readonly Lazy<CommandExecutor> _executor;
        public CommandExecutor Executor => _executor.Value;

        protected MediaAggregatorController()
        {
            _executor = new Lazy<CommandExecutor>(CreateExecutor);
        }

        private CommandExecutor CreateExecutor()
        {
            return new CommandExecutor(HttpContext.RequestServices.GetService<ICommandHandlerProvider>());
        }

        public ApplicationDataContext DataContext => HttpContext.RequestServices.GetService<ApplicationDataContext>();

        protected Task<TResult> ExecuteAsync<TCommand, TResult>(TCommand command) 
            where TCommand : ICommand
        {
            return Executor.ExecuteAsync<TCommand, TResult>(command, DataContext);
        }

        protected Task ExecuteAsync<TCommand>(TCommand command) 
            where TCommand : ICommand
        {
            return Executor.ExecuteAsync(command, DataContext);
        }

        protected Task<TResult> ExecuteAsync<TCommand, TResult>()
            where TCommand : ICommand, new()
        {
            return Executor.ExecuteAsync<TCommand, TResult>(DataContext);
        }

        protected Task ExecuteAsync<TCommand>()
            where TCommand : ICommand, new()
        {
            return Executor.ExecuteAsync<TCommand>(DataContext);
        }
    }
}
