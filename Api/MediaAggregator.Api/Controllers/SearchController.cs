﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using MediaAggregator.Core.CommandHandlers;
using MediaAggregator.Core.Commands.Profile;
using MediaAggregator.Core.Commands.Profile.Search;
using MediaAggregator.Core.Models;

namespace MediaAggregator.Api.Controllers
{
    [Produces("application/json")]
    [Route("api/search")]
    public class SearchController : MediaAggregatorController
    {
        [Authorize]
        [HttpGet("friends")]
        public Task<DataListDto<SearchResultDto>> SearchFromFriends([FromQuery] SearchFriendsProfileCommand command)
            => ExecuteAsync<SearchFriendsProfileCommand, DataListDto<SearchResultDto>>(command);

        [HttpGet]
        [Authorize]
        public Task<DataListDto<SearchResultDto>> SearchPerson([FromQuery] SearchProfileCommand command)
        => ExecuteAsync<SearchProfileCommand, DataListDto<SearchResultDto>>(command);
    }
}