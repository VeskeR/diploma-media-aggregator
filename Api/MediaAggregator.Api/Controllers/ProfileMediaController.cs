﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using MediaAggregator.Api.Filters;
using MediaAggregator.Core.Commands.Profile.Gallery;
using MediaAggregator.Core.Models;

namespace MediaAggregator.Api.Controllers
{
    [Produces("application/json")]
    [Route("api/profile/media")]
    public class ProfileMediaController : MediaAggregatorController
    {
        [HttpGet]
        public Task<DataListDto<ProfileMediaDto>> GetAll(Guid? profileId = null)
        {
            return ExecuteAsync<GetGalleryMediaCommand, DataListDto<ProfileMediaDto>>(
                new GetGalleryMediaCommand(profileId));
        }

        // 5 gb limits
        [HttpPost]
        [RequestSizeLimit(5000000000)]
        public Task<ProfileMediaDto> Upload([FromBody] UploadGalleryMediaCommand command) =>
            ExecuteAsync<UploadGalleryMediaCommand, ProfileMediaDto>(command);

        [HttpPost("remove")]
        [NoContent]
        public Task Remove([FromBody] RemoveGalleryMediaCommand command) => ExecuteAsync(command);
    }
}