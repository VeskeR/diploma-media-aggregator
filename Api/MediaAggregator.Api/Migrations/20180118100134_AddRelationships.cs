﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace MediaAggregator.Api.Migrations
{
    public partial class AddRelationships : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Relationships",
                columns: table => new
                {
                    Profile1Id = table.Column<Guid>(nullable: false),
                    Profile2Id = table.Column<Guid>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Relationships", x => new { x.Profile1Id, x.Profile2Id });
                    table.ForeignKey(
                        name: "FK_Relationships_Profiles_Profile1Id",
                        column: x => x.Profile1Id,
                        principalTable: "Profiles",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Relationships_Profiles_Profile2Id",
                        column: x => x.Profile2Id,
                        principalTable: "Profiles",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Relationships_Profile1Id",
                table: "Relationships",
                column: "Profile1Id");

            migrationBuilder.CreateIndex(
                name: "IX_Relationships_Profile2Id",
                table: "Relationships",
                column: "Profile2Id");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Relationships");
        }
    }
}
