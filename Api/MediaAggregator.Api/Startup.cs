﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Threading.Tasks;
using Autofac;
using Autofac.Core;
using Autofac.Extensions.DependencyInjection;
using AutoMapper;
using Microsoft.AspNet.SignalR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Http.Extensions;
using Microsoft.AspNetCore.Mvc.Authorization;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.FileProviders;
using Microsoft.Extensions.FileProviders.Physical;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Microsoft.Extensions.PlatformAbstractions;
using MediaAggregator.Amazon;
using MediaAggregator.Api.Data;
using MediaAggregator.Api.Filters;
using MediaAggregator.Api.Identity;
using MediaAggregator.Api.Identity.CommandHandlers;
using MediaAggregator.Api.Identity.CommandHandlers.Account.Confirmation;
using MediaAggregator.Api.Identity.CommandValidators.Account.Confirmation;
using MediaAggregator.Api.Identity.Data;
using MediaAggregator.Api.Identity.Services;
using MediaAggregator.Api.Middleware;
using MediaAggregator.Api.Options;
using MediaAggregator.Api.Services;
using MediaAggregator.Api.SignalIRWebsockets;
using MediaAggregator.Core.CommandHandlers;
using MediaAggregator.Core.CommandValidators;
using MediaAggregator.Core.Data;
using MediaAggregator.Core.Mail;
using MediaAggregator.Core.Mapping;
using MediaAggregator.Core.Services;
using MediaAggregator.Core.Sms;
using MediaAggregator.SendGrid;
using MediaAggregator.ThumbnailGeneration;
using SendGrid;
using Swashbuckle.AspNetCore.Swagger;

namespace MediaAggregator.Api
{
    public class Startup
    {
        public Startup(IConfiguration configuration, IHostingEnvironment environment)
        {
            Configuration = configuration;
            Environment = environment;
        }

        public IConfiguration Configuration { get; }
        public IHostingEnvironment Environment { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public IServiceProvider ConfigureServices(IServiceCollection services)
        {
            services.AddCors();

            services.AddDbContext<ApplicationDataContext>(options => options
                .UseSqlServer(Configuration.GetConnectionString("DefaultConnection")));

            services.AddSignalR();

            services
                .AddMvc(config =>
                {
                    config.Filters.Add(new ValidateModelFilter());
                    var policy = new AuthorizationPolicyBuilder()
                        .RequireAuthenticatedUser()
                        .Build();
                    config.Filters.Add(new AuthorizeFilter(policy));
                    config.Filters.Add(new ArgumentExceptionFilter());
                    config.Filters.Add(new InvalidOperationExceptionFilter());
                    config.Filters.Add(new IdentityExceptionFilter());
                })
                .AddJsonOptions(options =>
                {
                    options.SerializerSettings.Converters.Add(new Newtonsoft.Json.Converters.StringEnumConverter());
                });

            services.AddIdentity(GetServerUrl());

            services.Configure<SendGridClientOptions>(Configuration.GetSection("SendGrid"));
            services.Configure<ClientOptions>(Configuration.GetSection("Client"));
            services.Configure<UserContentOptions>(Configuration.GetSection("UserContent"));
            services.Configure<AwsS3FileStorageOptions>(Configuration.GetSection("Aws:S3"));

            //services.AddTransient<IProfileService, ProfileService>();


            services.AddSwaggerGen(options =>
            {
                options.SwaggerDoc("v1", new Info() { Title = "Media Aggregator", Description = "API documentation", TermsOfService = "None" });

                options.IncludeXmlComments(GetXmlCommentsPath(PlatformServices.Default.Application));
                options.DescribeAllEnumsAsStrings();
            });

            var container = BuildContainer(services);

            return new AutofacServiceProvider(container);
        }
        private string GetXmlCommentsPath(ApplicationEnvironment appEnvironment)
        {
            return Path.Combine(appEnvironment.ApplicationBasePath, "MediaAggregator.Api.xml");
        }
        private string GetServerUrl()
        {
            var section = Configuration.GetSection("Server");
            if (section == null) throw new Exception("No Server section configured");
            var url = section["Url"];
            if (string.IsNullOrEmpty(url)) throw new Exception("No Server.Url configured");
            return url.TrimEnd('/');
        }

        private IContainer BuildContainer(IServiceCollection services)
        {
            var containerBuilder = new ContainerBuilder();
            containerBuilder.Populate(services);

            containerBuilder.RegisterType<ApplicationDataContext>()
                .As<IdentityContext>()
                .As<DataContext>()
                .InstancePerLifetimeScope();

            containerBuilder.RegisterAssemblyTypes(typeof(ProfileMappingProfile).Assembly)
                .Where(t => t.IsPublic && typeof(Profile).IsAssignableFrom(t))
                .As<Profile>()
                .SingleInstance();

            containerBuilder.Register(c =>
                {
                    Mapper.Initialize(mapper =>
                    {
                        var profiles = c.Resolve<IEnumerable<Profile>>().ToArray();
                        Array.ForEach(profiles, mapper.AddProfile);
                    });
                    return Mapper.Instance;
                })
                .As<IMapper>()
                .SingleInstance();

            containerBuilder.RegisterType<CommandHandlerProvider>()
                .Named<ICommandHandlerProvider>(nameof(CommandHandlerProvider))
                .SingleInstance();

            containerBuilder.RegisterType<AuthorizedCommandHandlerProvider>()
                .WithParameter(new ResolvedParameter(
                    (pi, context) => pi.ParameterType == typeof(ICommandHandlerProvider),
                    (pi, context) => context.ResolveNamed<ICommandHandlerProvider>(nameof(CommandHandlerProvider))))
                .As<ICommandHandlerProvider>()
                .SingleInstance();

            containerBuilder.RegisterType<CommandExecutor>().SingleInstance();
            containerBuilder.RegisterType<TransientDataContextFactory>().As<IDataContextFactory>().SingleInstance();
            containerBuilder.RegisterType<CommandHandlerServiceProvider>().SingleInstance();
            containerBuilder.RegisterType<IdentityCommandHandlerServiceProvider>().SingleInstance();

            containerBuilder.RegisterType<ThumbnailGenerator>().As<IThumbnailGenerator>().SingleInstance();

            containerBuilder.RegisterType<FriendRequestSender>().As<IFriendRequestSender>().SingleInstance();
            containerBuilder.RegisterType<GroupRequestSender>().As<IGroupRequestSender>().SingleInstance();
            containerBuilder.RegisterType<TeamMembershipRequestSender>().As<ITeamMembershipRequestSender>().SingleInstance();
            containerBuilder.RegisterType<TeamMembershipUpdateSender>().As<ITeamMembershipUpdateSender>().SingleInstance();
            containerBuilder.RegisterType<InviteSender>().As<IInviteSender>().SingleInstance();
            containerBuilder.RegisterType<AccountConfirmationSender>().As<IAccountConfirmationSender>().SingleInstance();
            containerBuilder.RegisterType<ResetPasswordTokenSender>().As<IResetPasswordTokenSender>().SingleInstance();
            containerBuilder.RegisterType<ChangeContactTokenSender>().As<IChangeContactTokenSender>().SingleInstance();

            containerBuilder.RegisterType<EmailSender>().As<IEmailSender>().SingleInstance();
            containerBuilder.RegisterType<NullSmsSender>().As<ISmsSender>().SingleInstance();
            containerBuilder.RegisterType<MessageFactory>().AsImplementedInterfaces().SingleInstance();
            containerBuilder.RegisterType<MessageSystemFacade>().SingleInstance();

            containerBuilder.RegisterType<AwsS3FileStorage>().As<IFileStorage>();

            containerBuilder.RegisterAssemblyTypes(typeof(ICommandHandler).Assembly, typeof(ConfirmEmailCommandHandler).Assembly)
                .Where(t => t.IsPublic && typeof(ICommandHandler).IsAssignableFrom(t))
                .As<ICommandHandler>()
                .AsImplementedInterfaces()
                .AsSelf()
                .SingleInstance();

            containerBuilder.RegisterAssemblyTypes(typeof(ICommandValidator).Assembly, typeof(ConfirmEmailCommandValidator).Assembly)
                .Where(t => t.IsPublic && typeof(ICommandValidator).IsAssignableFrom(t))
                .As<ICommandValidator>()
                .AsImplementedInterfaces()
                .AsSelf()
                .SingleInstance();

            containerBuilder.RegisterAssemblyTypes(typeof(IDataObserver).Assembly)
                .Where(t => t.IsPublic && typeof(IDataObserver).IsAssignableFrom(t))
                .As<IDataObserver>()
                .SingleInstance();

            return containerBuilder.Build();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory,
            IOptionsSnapshot<ClientOptions> clientOptions, IOptionsSnapshot<UserContentOptions> userContentOptions)
        {
            loggerFactory.AddConsole(Configuration.GetSection("Logging"));
            loggerFactory.AddDebug(LogLevel.Trace);

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseDatabaseErrorPage();
                // app.UseBrowserLink();
            }
            // Route all unknown requests to app root
            app.Use(async (context, next) =>
            {
                await next();

                // If there's no available file and the request doesn't contain an extension, we're probably trying to access a page.
                // Rewrite request to use app root
                if (context.Response.StatusCode == 404 && !Path.HasExtension(context.Request.Path.Value))
                {
                    context.Request.Path = "/index.html"; // Put your Angular root page here 
                    context.Response.StatusCode = 200; // Make sure we update the status code, otherwise it returns 404
                    await next();
                }
            });


#if !DEBUG
            if (!MigrateDatabase(app))
            {
                app.Use(req => (context =>
                {
                    context.Response.StatusCode = StatusCodes.Status503ServiceUnavailable;
                    return req(context);
                }));
                return;
            }
#endif

            if (!string.Equals(clientOptions.Value.Url.TrimEnd('/'), GetServerUrl()))
            {
                app.UseCors(builder => builder
                    .WithOrigins(clientOptions.Value.Url)
                    .AllowAnyHeader()
                    .AllowCredentials()
                    .AllowAnyMethod());
                loggerFactory.CreateLogger<Startup>().LogInformation($"CORS policy added for {clientOptions.Value.Url}");
            }


            app.UseStaticFiles();
            app.UseDefaultFiles();
            app.UseFileServer();
            app.UseMiddleware<UserContentMiddleware>();

            app.UseIdentityServer();

            GlobalHost.HubPipeline.RequireAuthentication();
            app.UseSignalR(routes =>
            {
                routes.MapHub<ImHub>("/imhub");
                routes.MapHub<GroupImHub>("/groupimhub");
            });

            app.UseMvc();

            app.UseSwagger(options => options.PreSerializeFilters.Add((document, request)
                => document.Host = request.Host.Value));

            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "My API v1");
            });
        }
       
        private bool MigrateDatabase(IApplicationBuilder app)
        {
            Debug.Assert(app != null);
            var logger = app.ApplicationServices.GetService<ILoggerFactory>().CreateLogger<Startup>();
            logger.LogDebug("Checking DB migrations...");
            try
            {
                using (var context = app.ApplicationServices.GetService<ApplicationDataContext>())
                {
                    var pendingMigrations = context.Database.GetPendingMigrations().ToArray();
                    if (pendingMigrations.Length > 0)
                    {
                        logger.LogInformation($"Applying migrations {string.Join(",", pendingMigrations)}...");
                        try
                        {
                            context.Database.Migrate();
                            logger.LogInformation("Migrated successfully");
                        }
                        catch (Exception e)
                        {
                            logger.LogCritical(e, "DB migration failed");
                            return false;
                        }
                    }
                }
                logger.LogDebug("DB migrations check completed...");
            }
            catch (Exception e)
            {
                logger.LogError(e, "DB migrations check failed");
                return false;
            }
            return true;
        }
    }
}
