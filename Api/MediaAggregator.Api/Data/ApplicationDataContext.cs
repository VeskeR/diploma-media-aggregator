﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using MediaAggregator.Api.Identity;
using MediaAggregator.Api.Identity.Data;
using MediaAggregator.Core.Data;

namespace MediaAggregator.Api.Data
{
    public class ApplicationDataContext : IdentityContext
    {
        public ApplicationDataContext()
        {
        }

        public ApplicationDataContext(DbContextOptions<ApplicationDataContext> options, IEnumerable<IDataObserver> observers)
            : base(options, observers)
        {
        }
    }
}
