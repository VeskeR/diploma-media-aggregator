﻿namespace MediaAggregator.Api.Options
{
    public class ClientOptions
    {
        public string Url { get; set; }
        public ClientEndpointsOptions Endpoints { get; set; }
    }

    public class ClientEndpointsOptions
    {
        public string PublicProfile { get; set; }
        public string ConfirmEmail { get; set; }
        public string Register { get; set; }
        public string ResetPassword { get; set; }
        public string ChangeEmail { get; set; }
        public string AcceptFriendRequest { get; set; }
        public string ConfirmMember { get; set; }
    }
}
