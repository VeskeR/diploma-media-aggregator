﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MediaAggregator.Api.Options
{
    public class UserContentOptions
    {
        public string Prefix { get; set; } = "user-content";
    }
}
