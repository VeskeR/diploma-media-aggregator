﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;
using MediaAggregator.Core.Exceptions;
using MediaAggregator.Core.Services;

namespace MediaAggregator.Api.Services
{
    public class LocalFileStorage : IFileStorage
    {
        public IHostingEnvironment Environment { get; }

        public const string Folder = "uploads";

        public LocalFileStorage(IHostingEnvironment environment)
        {
            Environment = environment;
        }

        public async Task SaveAsync(string path, Stream data, bool overwrite = false)
        {
            var localPath = CreateLocalPath(path);

            if (!overwrite && File.Exists(localPath))
            {
                throw new FileAlreadyExistsException();
            }

            using (var fs = File.Create(localPath))
            {
                await data.CopyToAsync(fs);
            }
        }

        public Task<FileMetadata> GetMetadataAsync(string path)
        {
            var fileInfo = new FileInfo(GetLocalPath(path));
            if (!fileInfo.Exists)
            {
                return Task.FromResult(FileMetadata.NotExist);
            }
            return Task.FromResult(new FileMetadata(fileInfo.Length));
        }

        class PartialFileStream : Stream, IDisposable
        {
            public Stream Inner { get; }
            public ByteRange ByteRange { get; }
            public long Start { get; }
            public long End { get; }

            public PartialFileStream(Stream inner, ByteRange byteRange)
            {
                if (inner == null) throw new ArgumentNullException(nameof(inner));
                if (byteRange == null) throw new ArgumentNullException(nameof(byteRange));
                Inner = inner;
                ByteRange = byteRange;

                Start = ByteRange.Start ?? 0;
                Inner.Position = Start;

                End = ByteRange.End ?? inner.Length - 1;
            }

            public override void Flush()
            {
                Inner.Flush();
            }

            public override int Read(byte[] buffer, int offset, int count)
            {
                var start = (int)(offset + Start);
                var adjustedCount = Math.Min(count, (int)(End - start + 1));
                return Inner.Read(buffer, offset, adjustedCount);
            }

            public override long Seek(long offset, SeekOrigin origin)
            {
                switch (origin)
                {
                    case SeekOrigin.Begin:
                        Inner.Seek(Start + offset, SeekOrigin.Begin);
                        break;
                    case SeekOrigin.Current:
                        break;
                    case SeekOrigin.End:
                        Inner.Seek(Inner.Length - End - 1 + offset, SeekOrigin.End);
                        break;
                    default:
                        throw new ArgumentOutOfRangeException(nameof(origin), origin, null);
                }
                return Position;
            }

            public override void SetLength(long value)
            {
                throw new NotSupportedException();
            }

            public override void Write(byte[] buffer, int offset, int count)
            {
                throw new NotSupportedException();
            }

            public override bool CanRead { get; } = true;
            public override bool CanSeek { get; } = false;
            public override bool CanWrite { get; } = false;
            public override long Length => End - Start + 1;

            public override long Position
            {
                get => Inner.Position - Start;
                set => throw new NotSupportedException();
            }

            void IDisposable.Dispose()
            {
                Inner.Dispose();
            }
        }

        public Task<Stream> GetAsync(string path, ByteRange byteRange = null)
        {
            var fs = File.OpenRead(GetLocalPath(path));
            if (byteRange != null)
            {
                return Task.FromResult<Stream>(new PartialFileStream(fs, byteRange));
            }
            return Task.FromResult<Stream>(fs);
        }

        public Task DeleteAsync(string path)
        {
            var localPath = GetLocalPath(path);
            if (File.Exists(localPath))
            {
                File.Delete(localPath);
            }
            return Task.CompletedTask;
        }

        private string RootPath => Path.Combine(Environment.ContentRootPath, Folder);

        private string GetLocalPath(string path)
        {
            var pathBuilder = new StringBuilder(path);
            var queryIndex = path.IndexOf('?');
            if (queryIndex > -1)
            {
                pathBuilder = pathBuilder.Remove(queryIndex, pathBuilder.Length - queryIndex);
            }
            return Path.Combine(RootPath, pathBuilder.ToString());
        }

        private string CreateLocalPath(string path)
        {
            var localPath = GetLocalPath(path);
            var directory = Path.GetDirectoryName(localPath);
            if (!Directory.Exists(directory))
            {
                Directory.CreateDirectory(directory);
            }
            return localPath;
        }
    }
}
