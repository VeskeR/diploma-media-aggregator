﻿using System;
using System.Threading.Tasks;
using MediaAggregator.Core.DataInterfaces;
using MediaAggregator.Core.Models;
using MediaAggregator.Core.Services;

namespace MediaAggregator.Api.Services
{
    public class InviteSender : IInviteSender
    {
        public MessageSystemFacade Messaging { get; }

        public InviteSender(MessageSystemFacade messaging)
        {
            if (messaging == null) throw new ArgumentNullException(nameof(messaging));
            Messaging = messaging;
        }

        public async Task SendAsync(IInvite invite)
        {
            if (invite == null) throw new ArgumentNullException(nameof(invite));
            if (!string.IsNullOrEmpty(invite.Contact.Email))
            {
                await Messaging.SendEmailAsync(x => x.CreateInviteMessage(invite));
            }
            if (!string.IsNullOrEmpty(invite.Contact.Phone))
            {
                await Messaging.SendSmsAsync(x => x.CreateInviteMessage(invite));
            }
        }
    }
}