﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MediaAggregator.Api.Identity.Data.Entities;
using MediaAggregator.Api.Identity.Services;
using MediaAggregator.Core.Mail;
using MediaAggregator.Core.Sms;

namespace MediaAggregator.Api.Services
{
    public class AccountConfirmationSender : IAccountConfirmationSender
    {
        public MessageSystemFacade Messaging { get; }

        public AccountConfirmationSender(MessageSystemFacade messaging)
        {
            if (messaging == null) throw new ArgumentNullException(nameof(messaging));
            Messaging = messaging;
        }

        public Task SendEmailAsync(User user, string code)
        {
            return Messaging.SendEmailAsync(x => x.CreateAccountConfirmationMessage(user, code));
        }

        public Task SendSmsAsync(User user, string code)
        {
            return Messaging.SendSmsAsync(x => x.CreateAccountConfirmationMessage(user, code));
        }
    }
}
