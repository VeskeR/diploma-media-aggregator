using System;
using System.Threading.Tasks;
using MediaAggregator.Api.Identity.Data.Entities;
using MediaAggregator.Api.Identity.Services;
using MediaAggregator.Core.Mail;
using MediaAggregator.Core.Sms;

namespace MediaAggregator.Api.Services
{
    public class ResetPasswordTokenSender : IResetPasswordTokenSender
    {
        public MessageSystemFacade Messaging { get; }

        public ResetPasswordTokenSender(MessageSystemFacade messaging)
        {
            if (messaging == null) throw new ArgumentNullException(nameof(messaging));
            Messaging = messaging;
        }

        public Task SendEmailAsync(User user, string token)
        {
            return Messaging.SendEmailAsync(x => x.CreateResetPasswordMessage(user, token));
        }

        public Task SendSmsAsync(User user, string token)
        {
            return Messaging.SendSmsAsync(x => x.CreateResetPasswordMessage(user, token));
        }
    }
}