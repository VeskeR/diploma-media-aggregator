﻿using System;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.AspNetCore.Mvc.ModelBinding;

namespace MediaAggregator.Api.Filters
{
    public class InvalidOperationExceptionFilter : ExceptionFilterAttribute
    {
        public override void OnException(ExceptionContext context)
        {
            if (context.Exception is InvalidOperationException ex)
            {
                var errors = new ModelStateDictionary();
                errors.AddModelError(string.Empty, ex.Message);
                context.Result = new BadRequestObjectResult(errors);
            }
        }
    }
}