﻿using System;
using Microsoft.AspNetCore.Http;

namespace MediaAggregator.Api.Filters
{
    [AttributeUsage(AttributeTargets.Method)]
    public class CreatedAttribute : StatusCodeAttribute
    {
        public CreatedAttribute()
            : base(StatusCodes.Status201Created)
        {

        }
    }
}