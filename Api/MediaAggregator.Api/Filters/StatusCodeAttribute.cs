using System;
using Microsoft.AspNetCore.Mvc.Filters;

namespace MediaAggregator.Api.Filters
{
    [AttributeUsage(AttributeTargets.Method)]
    public class StatusCodeAttribute : ResultFilterAttribute
    {
        public int StatusCode { get; }

        public StatusCodeAttribute(int statusCode)
        {
            StatusCode = statusCode;
        }

        public override void OnResultExecuting(ResultExecutingContext context)
        {
            context.HttpContext.Response.StatusCode = StatusCode;
            base.OnResultExecuting(context);
        }
    }
}