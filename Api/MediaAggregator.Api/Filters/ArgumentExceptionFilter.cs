﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.AspNetCore.Mvc.ModelBinding;

namespace MediaAggregator.Api.Filters
{
    public class ArgumentExceptionFilter : ExceptionFilterAttribute
    {
        public override void OnException(ExceptionContext context)
        {
            if (context.Exception is ArgumentException ex)
            {
                var errors = new ModelStateDictionary();
                errors.AddModelError(ex.ParamName ?? string.Empty, ex.Message);
                context.Result = new BadRequestObjectResult(errors);
            }
        }
    }
}
