﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using MediaAggregator.Api.Identity.Exceptions;

namespace MediaAggregator.Api.Filters
{
    public class IdentityExceptionFilter : ExceptionFilterAttribute
    {
        public override void OnException(ExceptionContext context)
        {
            if (context.Exception is IdentityException ex)
            {
                var errors = new ModelStateDictionary();
                foreach (var innerException in ex.InnerExceptions)
                {
                    errors.AddModelError(string.Empty, innerException.Message);
                }
                context.Result = new BadRequestObjectResult(errors);
            }
        }
    }
}
