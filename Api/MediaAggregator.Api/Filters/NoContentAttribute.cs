﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;

namespace MediaAggregator.Api.Filters
{
    [AttributeUsage(AttributeTargets.Method)]
    public class NoContentAttribute : StatusCodeAttribute
    {
        public NoContentAttribute()
            : base(StatusCodes.Status204NoContent)
        {
            
        }
    }
}
