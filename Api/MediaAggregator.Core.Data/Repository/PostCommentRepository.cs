﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using MediaAggregator.Core.Data.Entities;

namespace MediaAggregator.Core.Data.Repository
{
    internal class PostCommentRepository : IPostCommentRepository
    {
        public DbSet<PostComment> Set { get; }

        public PostCommentRepository(DbSet<PostComment> set)
        {
            Set = set;
        }

        public Task<PostComment[]> GetAsync(Post post)
        {
            if (post == null) throw new ArgumentNullException(nameof(post));
            return Set
                .Include(x => x.Author)
                .ThenInclude(x => x.Avatars)
                .ThenInclude(x => x.Media)
                .Where(x => x.Post.Id == post.Id)
                .OrderBy(x => x.CreationTime)
                .ToArrayAsync();
        }

        public Task<PostComment> FindByIdAsync(Guid? id)
        {
            return Set
                .Include(x => x.Author)
                .ThenInclude(x => x.Avatars)
                .ThenInclude(x => x.Media)
                .FindByIdAsync(id);
        }

        public void Add(PostComment entity)
        {
            Set.Add(entity);
        }

        public void Remove(PostComment entity)
        {
            Set.Remove(entity);
        }
    }
}
