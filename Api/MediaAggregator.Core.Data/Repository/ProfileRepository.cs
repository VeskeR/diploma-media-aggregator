using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using MediaAggregator.Core.Data;
using MediaAggregator.Core.Data.Entities;
using MediaAggregator.Core.DataInterfaces;
using MediaAggregator.Core.Enumerations;

namespace MediaAggregator.Core.Data.Repository
{
    internal class ProfileRepository : IProfileRepository
    {
        public DbSet<Profile> Set { get; }

        public ProfileRepository(DbSet<Profile> set)
        {
            Set = set;
        }

        public Task<Profile> FindByIdAsync(Guid? id)
        {
            return Set
                .Include(x => x.Parent)
                .ThenInclude(x => x.Contact)
                .Include(x => x.BackgroundImage)
                .IncludeSensitiveData()
                .IncludeAvatars()
                .FindByIdAsync(id);
        }

        public void Add(Profile entity)
        {
            Set.Add(entity);
        }

        public void Remove(Profile entity)
        {
            Set.Remove(entity);
        }

        public Task<Profile> FindByShortIdAsync(string shortId)
        {
            return Set
                .Include(x => x.BackgroundImage)
                .IncludeAvatars()
                .IncludeParent()
                .FindByShortIdAsync(shortId);
        }

        public Task<Profile[]> FindByShortIdsAsync(string[] shortIds)
        {
            return Set
                .Include(x => x.BackgroundImage)
                .IncludeAvatars()
                .IncludeParent()
                .Where(x => shortIds.Contains(x.ShortId))
                .ToArrayAsync();
        }

        public Task<IContact[]> FindExistingContactsAsync(IEnumerable<IContact> contacts)
        {
            return Set.FindExistingContactsAsync(contacts);
        }

        public Task<bool> EmailExistsAsync(string email)
        {
            return Set.AnyAsync(x => x.Contact.Email == email);
        }

        public Task<bool> NameExistsAsync(string name)
        {
            return Set.AnyAsync(x => x.DisplayName == name);
        }

        public Task<bool> ShortIdExistsAsync(string shortId)
        {
            return Set.AnyAsync(x => x.ShortId == shortId);
        }

        public Task<Profile[]> GetAsync(ProfileType? type, string phrase)
        {
            return Set
                .Include(x => x.BackgroundImage)
                .IncludeAvatars()
                .Where(x => type == null || x.Type == type)
                .Where(x => x.FirstName.Contains(phrase) ||
                            x.LastName.Contains(phrase) ||
                            x.DisplayName.Contains(phrase))
                .ToArrayAsync();
        }

        public Task<Profile[]> SearchFromFriends(ProfileType? type, IList<Guid> friendsIds, string phrase)
        {
            return Set
                .Include(x => x.BackgroundImage)
                .IncludeAvatars()
                .Where(x => friendsIds.Contains(x.Id))
                .Where(x => type == null || x.Type == type)
                .Where(x => x.FirstName.Contains(phrase) ||
                            x.LastName.Contains(phrase) ||
                            x.DisplayName.Contains(phrase))
                .ToArrayAsync();
        }

        public async Task<Profile> RemoveByIdAsync(Guid? id)
        {
            var profile = await Set
                .Include(x => x.BackgroundImage)
                .Include(x => x.Avatars)
                .Include(x => x.Media)
                .Include(x => x.Posts)
                .Include(x => x.PostComments)
                .Include(x => x.Teams)
                .FindByIdAsync(id);
            if (profile == null) throw new ArgumentException("Profile not found", nameof(id));

            //RemoveRange(profile.Avatars);
            //RemoveRange(profile.Media);
            //RemoveRange(profile.Posts);
            //RemoveRange(profile.PostComments);
            //RemoveRange(profile.Teams);

            Remove(profile);

            return profile;
        }

        public Task<Profile[]> GetByIdsAsync(IList<Guid> ids)
        {
            return Set
                .Include(x => x.BackgroundImage)
                .IncludeSensitiveData()
                .IncludeAvatars()
                .Where(x => ids.Contains(x.Id))
                .ToArrayAsync();
        }

        public Task<Profile[]> GetChildrenAsync(Guid? id)
        {
            return Set
                .Where(x => x.ParentId == id)
                .ToArrayAsync();
        }

        public Task<Profile> GetByEmailAsync(string email)
        {
            return Set.FirstOrDefaultAsync(r => r.Contact.Email == email);
        }
    }
}