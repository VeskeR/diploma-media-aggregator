using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using MediaAggregator.Core.Data.Entities;

namespace MediaAggregator.Core.Data.Repository
{
    internal class ProfileMediaRepository : RepositoryBase<ProfileMedia>, IProfileMediaRepository
    {
        public ProfileMediaRepository(DbSet<ProfileMedia> set) : base(set)
        {
        }

        public override Task<ProfileMedia> FindByIdAsync(Guid? id)
        {
            return Set.Include(x => x.Media).FindByIdAsync(id);
        }

        public async Task<Profile> LoadAsync(Profile profile)
        {
            profile.Media.Clear();
            profile.Media.AddRange(await Set.Include(x => x.Media).Where(x => x.Profile.Id == profile.Id).ToArrayAsync());
            return profile;
        }
    }
}