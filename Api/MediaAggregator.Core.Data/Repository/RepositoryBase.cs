﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using MediaAggregator.Core.Data.Entities;

namespace MediaAggregator.Core.Data.Repository
{
    internal abstract class RepositoryBase<T> : IRepository<T> where T : Entity
    {
        public DbSet<T> Set { get; }

        protected RepositoryBase(DbSet<T> set)
        {
            Set = set;
        }

        public virtual Task<T> FindByIdAsync(Guid? id)
        {
            return Set.FindByIdAsync(id);
        }

        public void Add(T entity)
        {
            Set.Add(entity);
        }

        public void Remove(T entity)
        {
            Set.Remove(entity);
        }
    }
}
