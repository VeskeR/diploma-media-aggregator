﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using MediaAggregator.Core.Data.Entities;

namespace MediaAggregator.Core.Data.Repository
{
    internal class PostMediaRepository : IPostMediaRepository
    {
        public DbSet<PostMedia> Set { get; }

        public PostMediaRepository(DbSet<PostMedia> set)
        {
            Set = set;
        }

        public Task<PostMedia> FindByIdAsync(Guid? id)
        {
            return Set
               .Include(x => x.Media)
               .FindByIdAsync(id);
        }

        public void Add(PostMedia entity)
        {
            Set.Add(entity);
        }

        public void Remove(PostMedia entity)
        {
            Set.Remove(entity);
        }
    }
}
