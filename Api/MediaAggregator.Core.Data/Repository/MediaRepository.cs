using Microsoft.EntityFrameworkCore;
using MediaAggregator.Core.Data.Entities;

namespace MediaAggregator.Core.Data.Repository
{
    internal class MediaRepository : RepositoryBase<Media>, IMediaRepository
    {
        public MediaRepository(DbSet<Media> set) : base(set)
        {
        }
    }
}