using System;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using MediaAggregator.Core.Data;
using MediaAggregator.Core.Data.Entities;

namespace MediaAggregator.Core.Data.Repository
{
    internal class AvatarRepository : RepositoryBase<Avatar>, IAvatarRepository
    {
        public AvatarRepository(DbSet<Avatar> set) : base(set)
        {
        }
    }
}