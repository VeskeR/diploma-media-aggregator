﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using MediaAggregator.Core.Data.Entities;
using MediaAggregator.Core.Data.Pagination;
using MediaAggregator.Core.Helpers.Pagination;

namespace MediaAggregator.Core.Data.Repository
{
    internal class PostRepository : IPostRepository
    {
        public DbSet<Post> Set { get; }

        public PostRepository(DbSet<Post> set)
        {
            Set = set;
        }

        public Task<DataPage<Post>> GetPostsAsync(Func<IQueryable<Post>, IQueryable<Post>> queryBuilder, string pageId, int pageSize)
        {
            var query = Set
                .Include(x => x.Media)
                .ThenInclude(x => x.Media)
                .Include(x => x.Group)
                .Include(x => x.Author)
                .ThenInclude(x => x.Avatars)
                .ThenInclude(x => x.Media)
                .Include(x => x.Comments)
                .ThenInclude(x => x.Author)
                .AsQueryable();

            query = queryBuilder(query);

            query = query.OrderByDescending(x => x.PublishTime);

            var paginator = new Paginator<Post, DateTimeOffsetTicks>(
                new DateTimeOffsetTicksPageProvider<Post>(
                    x => x.PublishTime, Expression.LessThanOrEqual));

            return paginator.GetPageAsync(query, pageSize, pageId);
        }

        public Task<Post> FindByIdAsync(Guid? id)
        {
            return Set
                .Include(x => x.Comments)
                .Include(x => x.Media)
                .ThenInclude(x => x.Media)
                .Include(x => x.Group)
                .Include(x => x.Author)

                .ThenInclude(x => x.Avatars)
                .ThenInclude(x => x.Media)
                .FindByIdAsync(id);
        }

        public void Add(Post post)
        {
            Set.Add(post);
        }

        public void Remove(Post entity)
        {
            Set.Remove(entity);
        }
    }
}
