﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using MediaAggregator.Core.Data.Entities;
using MediaAggregator.Core.DataInterfaces;

namespace MediaAggregator.Core.Data
{
    public static class ProfileQueryableExtensions
    {
        public static Task<bool> WithSameContactExistsAsync(this IQueryable<Profile> set, IContact contact)
        {
            if (contact == null) throw new ArgumentNullException(nameof(contact));
            return set
                .Where(ContactComparer.EqualsExpression<Profile>(x => x.Contact, contact.Email, contact.Phone))
                .AnyAsync();
        }

        public static IQueryable<Profile> IncludeAvatars(this IQueryable<Profile> query)
        {
            return query.Include(x => x.Avatars).ThenInclude(x => x.Media);
        }

        public static IQueryable<Profile> IncludeMedia(this IQueryable<Profile> query)
        {
            return query.Include(x => x.Media).ThenInclude(x => x.Media);
        }

        public static IQueryable<Profile> IncludeParent(this IQueryable<Profile> query)
        {
            return query.Include(x => x.Parent).ThenInclude(x => x.Contact);
        }
        public static IQueryable<Profile> IncludeTeams(this IQueryable<Profile> query)
        {
            return query
                .Include(x => x.Teams).ThenInclude(x => x.Team).ThenInclude(x => x.Members).ThenInclude(x => x.Member)
                .Include(x => x.Teams).ThenInclude(x => x.Team).ThenInclude(x => x.Members).ThenInclude(x => x.Assignments);
        }

        public static IQueryable<Profile> IncludeSensitiveData(this IQueryable<Profile> query)
        {
            return query.Include(x => x.Contact).Include(x => x.Address);
        }

        public static Task<IContact[]> FindExistingContactsAsync(this IQueryable<Profile> set, IEnumerable<IContact> contacts)
        {
            var contactParam = Expression.Parameter(typeof(IContact));
            Expression orExpression = Expression.Constant(false);
            foreach (var contact in contacts)
            {
                orExpression = Expression.Or(orExpression, ContactComparer.EqualsExpression(contactParam, contact));
            }
            Expression<Func<IContact, bool>> whereExpr = Expression.Lambda<Func<IContact, bool>>(orExpression, contactParam);

            return set.Select(x => x.Contact).Where(whereExpr).ToArrayAsync();
        }

        public static Task<Profile> FindByShortIdAsync(this IQueryable<Profile> query, string shortId)
        {
            return query.FindOneAsync(x => x.ShortId == shortId);
        }
    }
}
