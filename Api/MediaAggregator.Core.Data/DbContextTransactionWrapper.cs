using System;
using Microsoft.EntityFrameworkCore.Storage;

namespace MediaAggregator.Core.Data
{
    internal class DbContextTransactionWrapper : IDataTransaction
    {
        private readonly IDbContextTransaction _transaction;

        public DbContextTransactionWrapper(IDbContextTransaction transaction)
        {
            _transaction = transaction ?? throw new ArgumentNullException(nameof(transaction));
        }

        public void Dispose()
        {
            _transaction.Dispose();
        }

        public void Commit()
        {
            _transaction.Commit();
        }
    }
}