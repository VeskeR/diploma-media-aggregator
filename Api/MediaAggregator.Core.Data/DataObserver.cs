using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;

namespace MediaAggregator.Core.Data
{
    public class DataObserver<T> : IDataObserver
    {
        Task IDataObserver.NotifyAsync(EntityEntry entry)
        {
            if (entry.Entity is T entity)
            {
                switch (entry.State)
                {
                    case EntityState.Deleted:
                        return NotifyDeletedAsync(entity);
                    case EntityState.Modified:
                        return NotifyModifiedAsync(entry);
                    case EntityState.Added:
                        return NotifyAddedAsync(entity);
                }
            }
            return Task.CompletedTask;
        }

        public virtual Task NotifyAddedAsync(T entity) => Task.CompletedTask;
        public virtual Task NotifyModifiedAsync(EntityEntry entry) => Task.CompletedTask;
        public virtual Task NotifyDeletedAsync(T entity) => Task.CompletedTask;
    }
}