﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Threading;
using System.Threading.Tasks;
using EntityFrameworkCore.Triggers;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ValueGeneration;
using MediaAggregator.Core.Data.Entities;
using MediaAggregator.Core.Data.Repository;
using MediaAggregator.Core.DataInterfaces;
using MediaAggregator.Core.Enumerations;

namespace MediaAggregator.Core.Data
{
    public class DataContext : DbContext, IDataContext
    {
        public IDataObserver[] Observers { get; }

        private Lazy<IProfileRepository> _profileRepository;
        IProfileRepository IDataContext.Profiles => _profileRepository.Value;

        private Lazy<IChatMessageRepository> _chatMessageRepository;
        IChatMessageRepository IDataContext.ChatMessages => _chatMessageRepository.Value;

        private Lazy<ITeamRepository> _teamRepository;
        ITeamRepository IDataContext.Teams => _teamRepository.Value;

        private Lazy<ITeamMemberRepository> _teamMemberRepository;
        ITeamMemberRepository IDataContext.TeamMembers => _teamMemberRepository.Value;

        private Lazy<IPostRepository> _postRepository;
        IPostRepository IDataContext.Posts => _postRepository.Value;

        private Lazy<IPostCommentRepository> _postCommentRepository;
        IPostCommentRepository IDataContext.PostComments => _postCommentRepository.Value;

        private Lazy<IRelationshipRepository> _relationshipRepository;
        IRelationshipRepository IDataContext.Relationships => _relationshipRepository.Value;

        private Lazy<IInviteRepository> _inviteRepository;
        IInviteRepository IDataContext.Invites => _inviteRepository.Value;

        private Lazy<IAvatarRepository> _avatarRepository;
        IAvatarRepository IDataContext.Avatars => _avatarRepository.Value;

        private Lazy<IMediaRepository> _mediaRepository;
        IMediaRepository IDataContext.Media => _mediaRepository.Value;

        private Lazy<IProfileMediaRepository> _profileMediaRepository;
        IProfileMediaRepository IDataContext.ProfileMedia => _profileMediaRepository.Value;

        private Lazy<IGroupRepository> _groupRepository;
        IGroupRepository IDataContext.Groups => _groupRepository.Value;

        private Lazy<IGroupMembershipRepository> _groupMembershipRepository;
        IGroupMembershipRepository IDataContext.GroupMemberships => _groupMembershipRepository.Value;

        private Lazy<IPostMediaRepository> _postMediaRepository;
        IPostMediaRepository IDataContext.PostMedia => _postMediaRepository.Value;

        private Lazy<ISportRepository> _sportRepository;
        ISportRepository IDataContext.Sports => _sportRepository.Value;
        
        private Lazy<IChildUnder13Repository> _childUnder13Repository;
        IChildUnder13Repository IDataContext.ChildsUnder13 => _childUnder13Repository.Value;

        private Lazy<IAdvertisementRepository> _advertisementRepository;
        IAdvertisementRepository IDataContext.Advertisements => _advertisementRepository.Value;

        private Lazy<ILocationRepository> _locationRepository;
        ILocationRepository IDataContext.Locations => _locationRepository.Value;

        private Lazy<IOpponentRepository> _opponentRepository;
        IOpponentRepository IDataContext.Opponents => _opponentRepository.Value;

        private Lazy<IGameRepository> _gameRepository;
        IGameRepository IDataContext.Games => _gameRepository.Value;

        private Lazy<IEventRepository> _eventRepository;
        IEventRepository IDataContext.Events => _eventRepository.Value;

        private Lazy<IAssignmentRepository> _assignmentRepository;
        IAssignmentRepository IDataContext.Assignments => _assignmentRepository.Value;

        private Lazy<IGroupMessageRepository> _groupMessageRepository;
        IGroupMessageRepository IDataContext.GroupMessage => _groupMessageRepository.Value;

        private void InitRepositories()
        {
            _profileRepository = new Lazy<IProfileRepository>(() => new ProfileRepository(Profiles));
            _chatMessageRepository = new Lazy<IChatMessageRepository>(() => new ChatMessageRepository(ChatMessages));
            _teamRepository = new Lazy<ITeamRepository>(() => new TeamRepository(Teams));
            _teamMemberRepository = new Lazy<ITeamMemberRepository>(() => new TeamMemberRepository(TeamMembers, _profileRepository.Value));
            _inviteRepository = new Lazy<IInviteRepository>(() => new InviteRepository(Invites));
            _postRepository = new Lazy<IPostRepository>(() => new PostRepository(Posts));
            _postCommentRepository = new Lazy<IPostCommentRepository>(() => new PostCommentRepository(PostComments));
            _relationshipRepository = new Lazy<IRelationshipRepository>(() => new RelationshipRepository(Relationships));
            _avatarRepository = new Lazy<IAvatarRepository>(() => new AvatarRepository(Avatars));
            _mediaRepository = new Lazy<IMediaRepository>(() => new MediaRepository(Media));
            _profileMediaRepository = new Lazy<IProfileMediaRepository>(() => new ProfileMediaRepository(ProfileMedia));
            _groupRepository = new Lazy<IGroupRepository>(() => new GroupRepository(Groups));
            _groupMembershipRepository = new Lazy<IGroupMembershipRepository>(() => new GroupMembershipRepository(GroupMemberships));
            _postMediaRepository = new Lazy<IPostMediaRepository>(() => new PostMediaRepository(PostMedia));
            _sportRepository = new Lazy<ISportRepository>(() => new SportRepository(Sports));
            _childUnder13Repository = new Lazy<IChildUnder13Repository>(() => new ChildUnder13Repository(ChildrensUnder13));
            _advertisementRepository = new Lazy<IAdvertisementRepository>(() => new AdvertisementRepository(Advertisements));
            _locationRepository = new Lazy<ILocationRepository>(() => new LocationRepository(Locations));
            _opponentRepository = new Lazy<IOpponentRepository>(() => new OpponentRepository(Opponents));
            _gameRepository = new Lazy<IGameRepository>(() => new GameRepository(Games));
            _eventRepository = new Lazy<IEventRepository>(() => new EventRepository(Events));
            _assignmentRepository = new Lazy<IAssignmentRepository>(() => new AssignmentRepository(Assignments));
            _groupMessageRepository = new Lazy<IGroupMessageRepository>(() => new GroupMessageRepository(GroupMessages));
        }

        static DataContext()
        {
            //Triggers<PostComment>.Inserting += entry =>
            //{
            //    var comment = entry.Entity;
            //    if (comment.Post == null)
            //    {
            //        entry.Context.Entry(comment).Reference(x => x.Post).Load();
            //    }
            //    Debug.Assert(comment.Post != null);
            //    comment.Post.OnCommentAdded();
            //    entry.Context.Entry(comment.Post).State = EntityState.Modified;
            //};
        }

        public DataContext()
        {
            InitRepositories();
        }

        public DataContext(DbContextOptions options, IEnumerable<IDataObserver> observers)
            : base(options)
        {
            if (observers == null) throw new ArgumentNullException(nameof(observers));
            Observers = observers.ToArray();
            InitRepositories();
        }

        public DbSet<Team> Teams { get; set; }
        public DbSet<TeamMember> TeamMembers { get; set; }
        public DbSet<Profile> Profiles { get; set; }
        public DbSet<Avatar> Avatars { get; set; }
        public DbSet<ProfileMedia> ProfileMedia { get; set; }
        public DbSet<Media> Media { get; set; }
        public DbSet<Post> Posts { get; set; }
        public DbSet<PostComment> PostComments { get; set; }
        public DbSet<Relationship> Relationships { get; set; }
        public DbSet<ChatMessage> ChatMessages { get; set; }
        public DbSet<Invite> Invites { get; set; }
        public DbSet<Group> Groups { get; set; }
        public DbSet<GroupMembership> GroupMemberships { get; set; }
        public DbSet<Sport> Sports { get; set; }
        public DbSet<PostMedia> PostMedia { get; set; }
        public DbSet<ChildUnder13> ChildrensUnder13 { get; set; }
        public DbSet<Advertisement> Advertisements { get; set; }
        public DbSet<Location> Locations { get; set; }
        public DbSet<Opponent> Opponents { get; set; }
        public DbSet<Game> Games { get; set; }
        public DbSet<Event> Events { get; set; }
        public DbSet<Assignment> Assignments { get; set; }
        public DbSet<GroupMessage> GroupMessages { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<Avatar>().Property(x => x.Id).HasValueGenerator<GuidValueGenerator>();
            modelBuilder.Entity<Avatar>()
                .HasOne(x => x.Media)
                .WithMany()
                .IsRequired()
                .OnDelete(DeleteBehavior.Cascade);
            modelBuilder.Entity<Avatar>()
                .HasOne(x => x.Profile)
                .WithMany(x => x.Avatars)
                .IsRequired()
                .OnDelete(DeleteBehavior.Cascade);

            modelBuilder.Entity<TeamMember>().Property(x => x.Id).HasValueGenerator<GuidValueGenerator>();
            modelBuilder.Entity<TeamMember>()
                .HasOne(x => x.Team)
                .WithMany(x => x.Members)
                .IsRequired()
                .OnDelete(DeleteBehavior.Cascade);
            modelBuilder.Entity<TeamMember>()
                .HasOne(x => x.Member)
                .WithMany(x => x.Teams)
                .IsRequired()
                .OnDelete(DeleteBehavior.Cascade);

            modelBuilder.Entity<Team>().Property(x => x.Id).HasValueGenerator<GuidValueGenerator>();
            modelBuilder.Entity<Team>().Property(x => x.Name).HasMaxLength(Team.MaxNameLength).IsRequired();
            modelBuilder.Entity<Team>().HasIndex(x => x.Name).IsUnique();
            modelBuilder.Entity<Team>().Ignore(x => x.Coach);
            modelBuilder.Entity<Team>().Ignore(x => x.Players);
            modelBuilder.Entity<Team>().Ignore(x => x.Organization);

            modelBuilder.Entity<Profile>().Property(x => x.Id).HasValueGenerator<GuidValueGenerator>();
            modelBuilder.Entity<Profile>().Property(x => x.ShortId).IsRequired().HasMaxLength(Profile.MaxShortIdLength);
            modelBuilder.Entity<Profile>().HasIndex(x => x.ShortId).IsUnique();
            modelBuilder.Entity<Profile>().Property(x => x.About).IsUnicode().HasMaxLength(1024);
            modelBuilder.Entity<Profile>().OwnsOne(x => x.Contact);
            modelBuilder.Entity<Profile>().OwnsOne(x => x.Address);
            modelBuilder.Entity<Profile>().Ignore(x => x.Pictures);
            modelBuilder.Entity<Profile>().Ignore(x => x.Videos);

            modelBuilder.Entity<Profile>()
                .HasOne(x => x.Parent)
                .WithMany(x => x.Children)
                .HasForeignKey(x => x.ParentId)
                .IsRequired(false)
                .OnDelete(DeleteBehavior.Restrict);

            modelBuilder.Entity<Relationship>()
                .HasKey(x => new { x.Profile1Id, x.Profile2Id });
            modelBuilder.Entity<Relationship>()
                .HasIndex(x => x.Profile1Id);
            modelBuilder.Entity<Relationship>()
                .HasIndex(x => x.Profile2Id);
            modelBuilder.Entity<Relationship>()
                .HasOne(x => x.Profile1)
                .WithMany()
                .IsRequired()
                .HasForeignKey(x => x.Profile1Id)
                .OnDelete(DeleteBehavior.Restrict);
            modelBuilder.Entity<Relationship>()
                .HasOne(x => x.Profile2)
                .WithMany()
                .IsRequired()
                .HasForeignKey(x => x.Profile2Id)
                .OnDelete(DeleteBehavior.Restrict);


            modelBuilder.Entity<ProfileMedia>().Property(x => x.Id).HasValueGenerator<GuidValueGenerator>();
            modelBuilder.Entity<ProfileMedia>()
                .HasOne(x => x.Media)
                .WithMany()
                .IsRequired()
                .OnDelete(DeleteBehavior.Cascade);

            modelBuilder.Entity<ProfileMedia>().Property(x => x.Id).HasValueGenerator<GuidValueGenerator>();
            modelBuilder.Entity<ProfileMedia>()
                .HasOne(x => x.Profile)
                .WithMany(x => x.Media)
                .IsRequired()
                .OnDelete(DeleteBehavior.Cascade);

            modelBuilder.Entity<Post>().Property(x => x.Id).HasValueGenerator<GuidValueGenerator>();
            modelBuilder.Entity<Post>().Property(x => x.Text).IsRequired().IsUnicode().HasMaxLength(1024);
            modelBuilder.Entity<Post>()
                .HasOne(x => x.Author)
                .WithMany(x => x.Posts)
                .IsRequired()
                .OnDelete(DeleteBehavior.Restrict);
            modelBuilder.Entity<Post>()
                .HasOne(x => x.Group)
                .WithMany()
                .IsRequired(false)
                .HasForeignKey(x => x.GroupId)
                .OnDelete(DeleteBehavior.Restrict);

            modelBuilder.Entity<Post>()
                .HasOne(x => x.Team)
                .WithMany()
                .IsRequired(false)
                .OnDelete(DeleteBehavior.Restrict);

            modelBuilder.Entity<PostMedia>().Property(x => x.Id).HasValueGenerator<GuidValueGenerator>();
            modelBuilder.Entity<PostMedia>()
                .HasOne(x => x.Media)
                .WithMany()
                .IsRequired()
                .OnDelete(DeleteBehavior.Restrict);
            modelBuilder.Entity<PostMedia>()
                .HasOne(x => x.Post)
                .WithMany(x => x.Media)
                .IsRequired()
                .OnDelete(DeleteBehavior.Restrict);

            modelBuilder.Entity<Invite>().Property(x => x.Id).HasValueGenerator<GuidValueGenerator>();
            modelBuilder.Entity<Invite>().OwnsOne(x => x.Contact);
            modelBuilder.Entity<Invite>()
                .HasOne(x => x.Inviter)
                .WithMany()
                .IsRequired(false)
                .OnDelete(DeleteBehavior.SetNull);

            modelBuilder.Entity<PostComment>().Property(x => x.Id).HasValueGenerator<GuidValueGenerator>();
            modelBuilder.Entity<PostComment>().Property(x => x.Text).IsRequired().IsUnicode().HasMaxLength(1024);
            modelBuilder.Entity<PostComment>()
                .HasOne(x => x.Author)
                .WithMany(x => x.PostComments)
                .IsRequired()
                .OnDelete(DeleteBehavior.Restrict);

            modelBuilder.Entity<ChatMessage>().Property(x => x.Id).HasValueGenerator<GuidValueGenerator>();
            modelBuilder.Entity<ChatMessage>().Property(x => x.Text).IsRequired().IsUnicode();
            modelBuilder.Entity<ChatMessage>()
                .HasOne(x => x.From)
                .WithMany()
                .IsRequired()
                .OnDelete(DeleteBehavior.Restrict);
            modelBuilder.Entity<ChatMessage>()
                .HasOne(x => x.To)
                .WithMany()
                .IsRequired()
                .OnDelete(DeleteBehavior.Restrict);

            modelBuilder.Entity<Group>().HasOne(x => x.Owner);
            modelBuilder.Entity<Group>()
               .Property(x => x.Id).HasValueGenerator<GuidValueGenerator>();
            modelBuilder.Entity<Group>().Property(x => x.Name).HasMaxLength(Team.MaxNameLength);


            modelBuilder.Entity<GroupMembership>()
                .HasKey(x => new { x.GroupId, x.ProfileId });
            modelBuilder.Entity<GroupMembership>()
                .HasIndex(x => x.GroupId);
            modelBuilder.Entity<GroupMembership>()
                .HasIndex(x => x.ProfileId);
            modelBuilder.Entity<GroupMembership>()
                .HasOne(x => x.Group)
                .WithMany(x => x.Members)
                .IsRequired()
                .HasForeignKey(x => x.GroupId)
                .OnDelete(DeleteBehavior.Restrict);
            modelBuilder.Entity<GroupMembership>()
                .HasOne(x => x.Profile)
                .WithMany()
                .IsRequired()
                .HasForeignKey(x => x.ProfileId)
                .OnDelete(DeleteBehavior.Restrict);

            modelBuilder.Entity<Sport>().Property(x => x.Id).HasValueGenerator<GuidValueGenerator>();
            modelBuilder.Entity<Sport>().Property(x => x.Name).HasMaxLength(Sport.MaxNameLength);

            modelBuilder.Entity<ChildUnder13>().Property(x => x.Id).HasValueGenerator<GuidValueGenerator>();
            modelBuilder.Entity<ChildUnder13>().HasOne(r => r.Parent).WithMany();
            modelBuilder.Entity<ChildUnder13>().HasOne(r => r.Profile).WithMany().IsRequired().OnDelete(DeleteBehavior.Restrict);

            modelBuilder.Entity<Advertisement>().Property(x => x.Id).HasValueGenerator<GuidValueGenerator>();
            modelBuilder.Entity<Advertisement>().Property(x => x.Title).IsRequired().IsUnicode().HasMaxLength(Advertisement.MaxTitleLength);
            modelBuilder.Entity<Advertisement>().Property(x => x.Content).IsRequired().IsUnicode().HasMaxLength(Advertisement.MaxContentLength);
            modelBuilder.Entity<Advertisement>().Property(x => x.FocusGroups).IsRequired();
            modelBuilder.Entity<Advertisement>().Property(x => x.DateCreated).IsRequired();
            modelBuilder.Entity<Advertisement>().Property(x => x.EndsOn).IsRequired();
            modelBuilder.Entity<Advertisement>().Ignore(x => x.Status);
            modelBuilder.Entity<Advertisement>()
                .HasOne(x => x.Owner)
                .WithMany(x => x.Advertisements)
                .IsRequired()
                .OnDelete(DeleteBehavior.Cascade);
            modelBuilder.Entity<Advertisement>()
                .HasOne(x => x.Media)
                .WithMany()
                .IsRequired()
                .OnDelete(DeleteBehavior.Restrict);

            modelBuilder.Entity<Location>().Property(x => x.Id).HasValueGenerator<GuidValueGenerator>();
            modelBuilder.Entity<Location>().Property(x => x.Name).IsRequired();
            modelBuilder.Entity<Location>()
                .HasOne(x => x.Team)
                .WithMany(x => x.Locations)
                .IsRequired()
                .OnDelete(DeleteBehavior.Cascade);

            modelBuilder.Entity<Opponent>().Property(x => x.Id).HasValueGenerator<GuidValueGenerator>();
            modelBuilder.Entity<Opponent>().Property(x => x.Name).IsRequired();
            modelBuilder.Entity<Opponent>()
                .HasOne(x => x.Team)
                .WithMany(x => x.Opponents)
                .IsRequired()
                .OnDelete(DeleteBehavior.Cascade);

            modelBuilder.Entity<Game>().Property(x => x.Id).HasValueGenerator<GuidValueGenerator>();
            modelBuilder.Entity<Game>().Property(x => x.StartDate).IsRequired();
            modelBuilder.Entity<Game>()
                .HasOne(x => x.Team)
                .WithMany(x => x.Games)
                .IsRequired()
                .OnDelete(DeleteBehavior.Cascade);

            modelBuilder.Entity<Event>().Property(x => x.Id).HasValueGenerator<GuidValueGenerator>();
            modelBuilder.Entity<Event>().Property(x => x.Name).IsRequired();
            modelBuilder.Entity<Event>().Property(x => x.StartDate).IsRequired();
            modelBuilder.Entity<Event>()
                .HasOne(x => x.Team)
                .WithMany(x => x.Events)
                .IsRequired()
                .OnDelete(DeleteBehavior.Cascade);

            modelBuilder.Entity<Assignment>().Property(x => x.Id).HasValueGenerator<GuidValueGenerator>();
            modelBuilder.Entity<Assignment>().Property(x => x.Description).IsRequired();
            modelBuilder.Entity<Assignment>()
                .HasOne(x => x.TeamMember)
                .WithMany(x => x.Assignments)
                .IsRequired()
                .OnDelete(DeleteBehavior.Cascade);
            modelBuilder.Entity<Assignment>()
                .HasOne(x => x.Event)
                .WithMany(x => x.Assignments)
                .IsRequired(false)
                .OnDelete(DeleteBehavior.Restrict);
            modelBuilder.Entity<Assignment>()
                .HasOne(x => x.Game)
                .WithMany(x => x.Assignments)
                .IsRequired(false)
                .OnDelete(DeleteBehavior.Restrict);

            modelBuilder.Entity<GroupMessage>().Property(x => x.Id).HasValueGenerator<GuidValueGenerator>();
            modelBuilder.Entity<GroupMessage>().Property(x => x.Text).IsRequired().IsUnicode();
            modelBuilder.Entity<GroupMessage>()
                .HasOne(x => x.From)
                .WithMany()
                .IsRequired()
                .OnDelete(DeleteBehavior.Restrict);
            modelBuilder.Entity<GroupMessage>()
                .HasOne(x => x.To)
                .WithMany()
                .IsRequired()
                .OnDelete(DeleteBehavior.Restrict);
        }

        public override async Task<int> SaveChangesAsync(CancellationToken cancellationToken = new CancellationToken())
        {
            if (Observers != null)
            {
                var entries = base.ChangeTracker.Entries().Where(x => x.State != EntityState.Unchanged).ToArray();
                foreach (var entry in entries)
                {
                    foreach (var observer in Observers)
                    {
                        await observer.NotifyAsync(entry);
                    }
                }
            }
            return await base.SaveChangesAsync(cancellationToken);
        }

        public async Task<IDataTransaction> BeginTransactionAsync()
        {
            var transaction = await Database.BeginTransactionAsync();
            return new DbContextTransactionWrapper(transaction);
        }
    }
}
