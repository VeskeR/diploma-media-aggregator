﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using MediaAggregator.Core.Data.Entities;

namespace MediaAggregator.Core.Data
{
    public static class QueryableExtensions
    {
        public static Task<T> FindByIdAsync<T>(this IQueryable<T> source, Guid? id) where T : Entity
        {
            return source.FindOneAsync(x => x.Id == id);
        }

        public static Task<T> FindOneAsync<T>(this IQueryable<T> source, Expression<Func<T, bool>> predicate) where T : class
        {
            return source.Where(predicate).SingleOrDefaultAsync();
        }

        public static Task<T[]> FindAsync<T>(this IQueryable<T> source, Expression<Func<T, bool>> predicate) where T : class
        {
            return source.Where(predicate).ToArrayAsync();
        }
    }
}
