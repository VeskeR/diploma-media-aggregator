﻿using System;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore.ChangeTracking;

namespace MediaAggregator.Core.Data
{
    public interface IDataObserver
    {
        Task NotifyAsync(EntityEntry entry);
    }
}