﻿namespace MediaAggregator.Core.Data
{
    public interface IDataContextFactory
    {
        DataContext Create();
    }
}