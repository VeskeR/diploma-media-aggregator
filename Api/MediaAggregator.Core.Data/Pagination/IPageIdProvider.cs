﻿using System;
using System.Linq.Expressions;

namespace MediaAggregator.Core.Helpers.Pagination
{
    internal interface IPageIdProvider<T, TPageId>
    {
        bool TryParse(string s, out TPageId pageId);
        TPageId Get(T value);
        Expression<Func<T, bool>> GetFilterExpression(TPageId pageId);
    }
}