﻿using System;
using System.Linq;
using System.Linq.Expressions;
using MediaAggregator.Core.Data;

namespace MediaAggregator.Core.Helpers.Pagination
{
    internal class DateTimeOffsetTicksPageProvider<T> : IPageIdProvider<T, DateTimeOffsetTicks>
    {
        private readonly Func<Expression, Expression, BinaryExpression> _compareOperator;
        private readonly Expression<Func<T, DateTimeOffset>> _memberExpression;
        private readonly Lazy<Func<T, DateTimeOffset>> _memberFunc;

        public DateTimeOffsetTicksPageProvider(Expression<Func<T, DateTimeOffset>> memberExpression, Func<Expression, Expression, BinaryExpression> compareOperator)
        {
            _compareOperator = compareOperator ?? throw new ArgumentNullException(nameof(compareOperator));
            _memberExpression = memberExpression ?? throw new ArgumentNullException(nameof(memberExpression));
            _memberFunc = new Lazy<Func<T, DateTimeOffset>>(() => _memberExpression.Compile());
        }

        public bool TryParse(string s, out DateTimeOffsetTicks pageId)
        {
            return DateTimeOffsetTicks.TryParse(s, out pageId);
        }

        public DateTimeOffsetTicks Get(T value)
        {
            return new DateTimeOffsetTicks(GetDateTimeOffset(value).ToUniversalTime());
        }

        public Expression<Func<T, bool>> GetFilterExpression(DateTimeOffsetTicks pageId)
        {
            var valueExpression = Expression.Constant(pageId.Value);
            var expression = _compareOperator(_memberExpression.Body, valueExpression);
            return Expression.Lambda<Func<T, bool>>(expression, _memberExpression.Parameters.Single());
        }

        private DateTimeOffset GetDateTimeOffset(T value)
        {
            return _memberFunc.Value(value);
        }
    }
}