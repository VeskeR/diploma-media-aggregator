﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using MediaAggregator.Core.Helpers.Pagination;

namespace MediaAggregator.Core.Data.Pagination
{
    internal class Paginator<T, TPageId>
    {
        private readonly IPageIdProvider<T, TPageId> _pageIdProvider;

        public Paginator(IPageIdProvider<T, TPageId> pageIdProvider)
        {
            _pageIdProvider = pageIdProvider ?? throw new ArgumentNullException(nameof(pageIdProvider));
        }

        public async Task<DataPage<T>> GetPageAsync(IQueryable<T> query, int batchSize, string pageId)
        {
            if (_pageIdProvider.TryParse(pageId, out TPageId parsedPageId))
            {
                query = query.Where(_pageIdProvider.GetFilterExpression(parsedPageId));
            }

            var data = await query
                .Take(batchSize + 1)
                .ToArrayAsync();

            string nextPageId = null;
            if (data.Length > batchSize)
            {
                nextPageId = _pageIdProvider.Get(data.Last()).ToString();
                Array.Resize(ref data, batchSize);
            }

            return new DataPage<T>(data, nextPageId);
        }
    }
}
