﻿using System;

namespace MediaAggregator.Core.Data
{
    internal class DateTimeOffsetTicks
    {
        public DateTimeOffsetTicks(DateTimeOffset value)
        {
            Value = value;
        }

        public DateTimeOffset Value { get; }

        public static bool TryParse(string s, out DateTimeOffsetTicks pageId)
        {
            if (long.TryParse(s, out long ticks))
            {
                pageId = new DateTimeOffsetTicks(new DateTimeOffset(ticks, TimeSpan.Zero));
                return true;
            }
            pageId = null;
            return false;
        }


        public override string ToString()
        {
            return Value.ToUniversalTime().Ticks.ToString();
        }
    }
}