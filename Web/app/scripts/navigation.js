var pageInfo = require('./page-info');
var parser = require('./parser/parser');
var settings = require('./settings/settings');
var settingsManager = require('./settings/settings-manager');

var $navigation = $('.navigation');
var $navigationTop = $('.navigation-top');
var $navigationBottom = $('.navigation-bottom');

function drawNavigation(picsCount) {
  var pageCount = picsCount === 'unknown' ? 0 : Math.ceil(picsCount / parser.getImagesCountPerPage());

  $navigation.empty();
  if (pageInfo.data.pageNumber > 1) {
    var $prev = getLinkBox(pageInfo.data.pageNumber - 1, '<');
    $navigation.append($prev);
  }

  var paginationCount = +settings.get('paginationCount');

  for (var i = pageInfo.data.pageNumber - paginationCount; i < pageInfo.data.pageNumber + paginationCount + 1; i++) {
    if (i === pageInfo.data.pageNumber || i > 0 && (picsCount === 'unknown' || i <= pageCount)) {
      var $box = getLinkBox(i, i, i === pageInfo.data.pageNumber);
      $navigation.append($box);
    }
  }

  if (picsCount === 'unknown' || pageInfo.data.pageNumber < pageCount) {
    var $next = getLinkBox(pageInfo.data.pageNumber + 1, '>');
    $navigation.append($next);
  }

  showNavigation();

  settingsManager.applyShowTopNavigation(settings.get('showTopNavigation'));
  settingsManager.applyShowBottomNavigation(settings.get('showBottomNavigation'));
}

function getLink(pageNumber) {
  // var link = '';
  //
  // if (pageInfo.getTagsLength() > 0) {
  //   link += '/tags/' + pageInfo.getURLTags();
  // }
  //
  // link += '/page/' + pageNumber + '/';
  // return link;

  return parser.createHash({
    pageNumber: pageNumber,
    tags: pageInfo.data.tags,
    filter: pageInfo.data.filter
  });
}

function getLinkBox(number, text, current) {
  var link = getLink(number);

  var $box = $("<div class='box'></div>");
  var $link = $("<a class='link'></a>");
  $link.attr('href', '#' + link);
  $link.html(text);
  if (current) {
    $link.addClass('current');
  }

  $box.append($link);
  return $box;
}

function showNavigation() {
  $navigation.show();
}

function hideNavigation() {
  $navigation.hide();
}

module.exports = {
  draw: drawNavigation,
  show: showNavigation,
  hide: hideNavigation,
  getLink: getLink
};
