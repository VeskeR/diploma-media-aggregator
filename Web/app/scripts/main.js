var base = require('./custom-pages/base');
var debug = require('./custom-pages/debug');
var page = require('./page');
var pageInfo = require('./page-info');

var initialized = false;

setTitle();

if (document.hasFocus()) {
  bootstrap();
} else {
  $(window).on('focus', function () {
    if (!initialized) {
      bootstrap();
    }
  });
}

function setTitle() {
  pageInfo.parsePageInfo();
  if (pageInfo.data.pageType === 'mainpage') {
    document.title = page.getMainPageTitle();
  } else if (pageInfo.data.pageType === 'imagepage') {
    document.title = 'Image page';
  } else {
    document.title = '?';
  }
}

function bootstrap() {
  initialized = true;
  var type = getCustomPageType();

  switch (type) {
    case 'debug':
      debug.start();
      break;
      break;
    case 'base':
      base.start();
      break;
    default:
      utils.log('Unknown custom page type');
      break;
  }
}

function getCustomPageType() {
  var hash = location.hash.substr(1);
  if (hash.startsWith('models')) {
    return 'models';
  } else if (/^debug|^gifs|^pics/.test(hash)) {
    return hash;
  } else {
    return 'base';
  }
}
