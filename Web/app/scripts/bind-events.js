var page = require('./page');
var pageInfo = require('./page-info');
var parser = require('./parser/parser');
var popup = require('./popup');
var settings = require('./settings/settings');
var tabNavigator = require('./tab-navigator');
var utils = require('./utils');

function bindEvents() {
  $(window).on('hashchange', function () {
    page.show();
  });

  $('.open-all-images').on('click', function () {
    if (confirm('Do you want to open all images on page?')) {
      var $thumbs = $('.thumb');

      if (!settings.get('openVisitedImages')) {
        $thumbs = _.filter($thumbs, function (v) {
          var $thumb = $(v);
          return !$thumb.find('.thumb-img').hasClass('dimmed');
        });
      }

      var commonKeywords = settings.get('commonKeywords');
      for (var i = $thumbs.length - 1; i >= 0; i--) {
        var $thumb = $($thumbs[i]);

        var keywords = $thumb.attr('data-keywords').split(',');
        if (!(settings.get('showCommonKeywords') && settings.get('showCommonKeywordsInTabs'))) {
          keywords = _.difference(keywords, commonKeywords);
        }

        if (settings.get('debug')) {
          keywords = new Array(keywords.length).fill('placeholder');
        }

        var title = 'Image page: ' + keywords.join(', ');
        var image = {
          smallSrc: $thumb.attr('data-smallsrc')
        };

        var link = $thumb.find('.thumb-link').attr('href');

        tabNavigator.createFullTab(link, title, image, tabNavigator.currentTab + 1);
      }
    }
  });

  (function () {
    var lastTouch = null;
    document.addEventListener('touchmove', function (e) {
      if (settings.get('preventPullRefresh') &&
          window.pageYOffset === 0 &&
          ($(e.target).closest('.tab-navigator').length === 0 ||
          $(e.target).closest('.tab-wrapper')[0].scrollTop === 0) &&
          ($(e.target).closest('.settings-menu').length === 0 ||
          $(e.target).closest('.settings-menu')[0].scrollTop === 0) &&
          lastTouch &&
          e.touches[0].clientY - lastTouch.touches[0].clientY > 0) {
        e.preventDefault();
      }
      lastTouch = e;
    }, {passive: false});
  })();

  document.addEventListener('contextmenu', function (e) {
    if (settings.get('disableContextMenu')) {
      e.preventDefault();
    }
  }, {passive: false});
}

module.exports = bindEvents;
