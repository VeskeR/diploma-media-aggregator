var parser = require('./parser/parser');
var settings = require('./settings/settings');
var utils = require('./utils');

var pageInfo = {
  defaultData: {
    host: settings.get('host'),
    pageType: 'mainpage',
    pageNumber: 1,
    tags: [],
    filter: [],
    link: ''
  },
  data: {
    host: settings.get('host'),
    pageType: 'mainpage',
    pageNumber: 1,
    tags: [],
    filter: [],
    link: ''
  },
  parsePageInfo: function (hash) {
    hash = hash || location.hash.substr(1);
    var data = parser.parseHash(hash);
    this.data = _.assign({}, this.defaultData, data);
    this.data.pageNumber = +this.data.pageNumber;
  },
  getStringTags: function (hideIfNeeded) {
    var mergedTags = _.concat([], [this.data.tags], this.data.filter);
    var tags = _.map(mergedTags, function (orTags) {
      return ((hideIfNeeded && settings.get('debug')) ? orTags.map(function (tag) {
        return 'placeholder';
      }) : orTags).join(' ' + settings.get('orSplitter') + ' ');
    }).filter(function (tag) {
      return tag.length > 0;
    }).join(', ');

    return tags;
  },
  isUsingFilter: function () {
    return this.getFilterLength() > 0;
  },
  parseTags: function (str) {
    var tags = [];

    tags = _.reduce(str.split(settings.get('andSplitter')), function (result, e) {
      result.push(e.split(settings.get('orSplitter')).map(function (tag) {
        return tag.replace(/\//g, '').replace(/\+/g, ' ').trim();
      }).filter(function (tag) {
        return tag.length > 0;
      }));
      return result;
    }, []);

    return tags;

    // var keyword = '';
    // var mode = 'and';
    // for (var i = 0; i < str.length; i++) {
    //   var symbol = str[i];
    //   switch (symbol) {
    //     case settings.get('andSplitter'):
    //       addKeyword();
    //       mode = 'and';
    //       break;
    //     case settings.get('orSplitter'):
    //       addKeyword();
    //       mode = 'or';
    //       break;
    //     default:
    //       keyword += symbol;
    //       break;
    //   }
    // }
    //
    // addKeyword();
    //
    // return tags;
    //
    // function addKeyword() {
    //   keyword = keyword.replace(/\//g, '').replace(/\+/g, ' ').trim();
    //   if (keyword.length > 1) {
    //     switch (mode) {
    //       case 'and':
    //         tags.push([keyword]);
    //         break;
    //       case 'or':
    //         if (!Array.isArray(tags[tags.length - 1])) {
    //           tags.push([]);
    //         }
    //         tags[tags.length - 1].push(keyword);
    //         break;
    //     }
    //   }
    //   keyword = '';
    // }
  },
  getFilterLength: function () {
    return _.flatten(this.data.filter).length;
  }
};

window.pageInfo = pageInfo;

module.exports = pageInfo;
