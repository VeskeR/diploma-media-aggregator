var coreUtils = {
  throttle: function (func, ms) {
    var isThrottled = false;
    var savedArgs;
    var savedThis;

    function wrapper() {
      if (isThrottled) {
        savedArgs = arguments;
        savedThis = this;
        return;
      }

      func.apply(this, arguments);

      isThrottled = true;

      setTimeout(function() {
        isThrottled = false;
        if (savedArgs) {
          wrapper.apply(savedThis, savedArgs);
          savedArgs = savedThis = null;
        }
      }, ms);
    }

    return wrapper;
  },
  hold: function (func, ms) {
    var timeout = -1;

    function wrapper() {
      var self = this;
      var args = arguments;
      clearTimeout(timeout);

      timeout = setTimeout(function () {
        func.apply(self, args);
      }, ms);

      return timeout;
    }

    return wrapper;
  },
  getHashCode: function (str) {
    var hash = 0;
    var i;
    var chr;
    var len;
    if (str.length === 0) return hash;
    for (i = 0, len = str.length; i < len; i++) {
      chr   = str.charCodeAt(i);
      hash  = ((hash << 5) - hash) + chr;
      hash |= 0; // Convert to 32bit integer
    }
    return hash;
  },
  camelCaseToDash: function (str) {
    return str.replace(/([a-z])([A-Z])/g, '$1-$2').toLowerCase();
  },
  camelCaseToNatural: function (str) {
    var splitted = str.replace(/([A-Z])/g, " $1");
    var result = splitted.charAt(0).toUpperCase() + splitted.slice(1).toLowerCase();
    return result;
  },
  capitalizeFirstLetter: function (str) {
    return str.charAt(0).toUpperCase() + str.slice(1);
  },
  json: function (obj) {
    return JSON.stringify(obj, null, ' ');
  },
  makeTextFile: function (text) {
    var data = new Blob([text], {type: 'text/plain'});

    // // If we are replacing a previously generated file we need to
    // // manually revoke the object URL to avoid memory leaks.
    // if (textFile !== null) {
    //   window.URL.revokeObjectURL(textFile);
    // }

    var textFile = window.URL.createObjectURL(data);

    return textFile;
  },
  toggleFullScreen: function () {
    var doc = window.document;
    var docEl = doc.documentElement;

    var requestFullScreen = docEl.requestFullscreen || docEl.mozRequestFullScreen || docEl.webkitRequestFullScreen || docEl.msRequestFullscreen;
    var cancelFullScreen = doc.exitFullscreen || doc.mozCancelFullScreen || doc.webkitExitFullscreen || doc.msExitFullscreen;

    if(!doc.fullscreenElement && !doc.mozFullScreenElement && !doc.webkitFullscreenElement && !doc.msFullscreenElement) {
      requestFullScreen.call(docEl);
    }
    else {
      cancelFullScreen.call(doc);
    }
  },
  randomBetween: function (a, b) {
    return Math.floor((Math.random() * b) + a);
  },
  getUUID: function () {
    var d = new Date().getTime();
    var uuid = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
        var r = (d + Math.random()*16)%16 | 0;
        d = Math.floor(d/16);
        return (c=='x' ? r : (r&0x3|0x8)).toString(16);
    });
    return uuid;
  },
  isInViewport: function (el) {
    var rect = el.getBoundingClientRect();

    return (
      rect.bottom >= 0 &&
      rect.right >= 0 &&

      rect.top <= (
      window.innerHeight ||
      document.documentElement.clientHeight) &&

      rect.left <= (
      window.innerWidth ||
      document.documentElement.clientWidth)
    );
  },
  isInViewportX: function (el) {
    var rect = el.getBoundingClientRect();

    return (
      rect.bottom >= 0 &&

      rect.top <= (
      window.innerHeight ||
      document.documentElement.clientHeight)
    );
  },
  isInViewportY: function (el) {
    var rect = el.getBoundingClientRect();

    return (
      rect.right >= 0 &&

      rect.left <= (
      window.innerWidth ||
      document.documentElement.clientWidth)
    );
  },
  createHash: function (params) {
    var hash = _.map(params, function (value, key) {
      return key + '=' + value;
    }).join('&');
    return hash;
  },
  parseHash: function (hash) {
    var params = _.reduce(hash.split('&'), function (result, param) {
      result[param.split('=')[0]] = param.split('=')[1];
      return result;
    }, {});
    return params;
  },
  setHashLink: function (link, host) {
    location.hash = this.createHash({
      link: link,
      host: host || settings.get('host')
    });
  },
  setHash: function (hash) {
    hash = hash.trim().replace(/\s+/g, '+').toLowerCase();
    location.hash = hash;
  },
  getHash: function () {
    var hash = location.hash.substr(1);
    hash = hash.trim().replace(/\+/g, ' ').toLowerCase();
    return hash;
  },
  normalizeObj: function (obj, abbreviationMap) {
    var newObj = {};
    for (var key in obj) {
      if (abbreviationMap.decode[key]) {
        newObj[abbreviationMap.decode[key]] = obj[key];
      } else {
        newObj[key] = obj[key];
      }
    }
    return newObj;
  },
  minifyObj: function (obj, abbreviationMap) {
    var newObj = {};
    for (var key in obj) {
      if (abbreviationMap.code[key]) {
        newObj[abbreviationMap.code[key]] = obj[key];
      } else {
        newObj[key] = obj[key];
      }
    }
    return newObj;
  },
  getUrlInfo: function (url) {
    var pathArray = url.split( '/' );
    var protocol = pathArray[0];
    var domain = pathArray[2];

    return {
      protocol: protocol,
      domain: domain,
      baseUrl: protocol + '//' + domain,
      getRelativeUrl: function (drop) {
        return protocol + '//' + domain + '/' + _.drop(_.dropRight(pathArray, drop), 3).join('/')
      },
      original: url
    };
  }
};

window.coreUtils = coreUtils;

module.exports = coreUtils;
