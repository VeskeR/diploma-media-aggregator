var ui = {
  gridSize: [6, 6],
  standart: {
    nextImage: [
      [1, 5],
      [3, 6]
    ],
    prevImage: [
      [1, 1],
      [3, 2]
    ],
    fullscreenDownloadLink: [
      [2, 3],
      [2, 4]
    ],
    goToImage: [
      [3, 3],
      [4, 4]
    ],
    exitFullscreen: [
      [5, 3],
      [6, 4]
    ],
    addSeenImage: [
      [6, 5],
      [6, 6]
    ],
    removeSeenImage: [
      [6, 1],
      [6, 2]
    ],
    addAwesomeImage: [
      [5, 5],
      [5, 6]
    ],
    removeAwesomeImage: [
      [5, 1],
      [5, 2]
    ],
    addBestImage: [
      [4, 5],
      [4, 6]
    ],
    removeBestImage: [
      [4, 1],
      [4, 2]
    ],
    toggleUiHelp: [
      [1, 3],
      [1, 4]
    ]
  },
  yesNo: {
    yesChoice: [
      [1, 5],
      [3, 6]
    ],
    noChoice: [
      [1, 1],
      [3, 2]
    ],
    fullscreenDownloadLink: [
      [2, 3],
      [2, 4]
    ],
    goToImage: [
      [3, 3],
      [4, 4]
    ],
    exitFullscreen: [
      [5, 3],
      [6, 4]
    ],
    addSeenImage: [
      [6, 5],
      [6, 6]
    ],
    removeSeenImage: [
      [6, 1],
      [6, 2]
    ],
    addAwesomeImage: [
      [5, 5],
      [5, 6]
    ],
    removeAwesomeImage: [
      [5, 1],
      [5, 2]
    ],
    addBestImage: [
      [4, 5],
      [4, 6]
    ],
    removeBestImage: [
      [4, 1],
      [4, 2]
    ],
    toggleUiHelp: [
      [1, 3],
      [1, 4]
    ]
  },
  advanced: {
    nextImage: [
      [1, 1],
      [2, 2]
    ],
    createNewTab: [
      [3, 1],
      [4, 2]
    ],
    prevImage: [
      [5, 1],
      [6, 2]
    ],
    fullscreenDownloadLink: [
      [2, 3],
      [2, 4]
    ],
    goToImage: [
      [3, 3],
      [4, 4]
    ],
    exitFullscreen: [
      [5, 3],
      [6, 4]
    ],
    addSeenImage: [
      [1, 5],
      [1, 6]
    ],
    removeSeenImage: [
      [2, 5],
      [2, 6]
    ],
    addAwesomeImage: [
      [3, 5],
      [3, 6]
    ],
    removeAwesomeImage: [
      [4, 5],
      [4, 6]
    ],
    addBestImage: [
      [5, 5],
      [5, 6]
    ],
    removeBestImage: [
      [6, 5],
      [6, 6]
    ],
    toggleUiHelp: [
      [1, 3],
      [1, 4]
    ]
  }
};

module.exports = ui;
