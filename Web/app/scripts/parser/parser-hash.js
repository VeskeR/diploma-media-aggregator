var utils = require('../utils');

var parserHash = {
  abbreviationMap: {
    code: {
      host: 'h',
      pageType: 'pt',
      pageNumber: 'pn',
      tags: 't',
      filter: 'f',
      link: 'l'
    },
    decode: {
      h: 'host',
      pt: 'pageType',
      pn: 'pageNumber',
      t: 'tags',
      f: 'filter',
      l: 'link'
    }
  },
  createHash: function (data) {
    data = _.assign({
      host: settings.get('host'),
      pageType: 'mainpage',
      pageNumber: 1,
      tags: [],
      filter: [],
      link: ''
    }, data);

    data.tags = data.tags.filter(function (tag) {
      return tag.length > 0;
    }).map(function (tag) {
      return tag.trim().replace(/\s+/g, '+').toLowerCase();
    }).join(',');
    data.filter = data.filter.map(function (orFilter) {
      return orFilter.filter(function (tag) {
        return tag.length > 0;
      }).map(function (tag) {
        return tag.trim().replace(/\s+/g, '+').toLowerCase();
      }).join(settings.get('orSplitter'));
    }).join(settings.get('andSplitter'));

    _.forEach(data, function (value, key) {
      data[key] = encodeURIComponent(value);
    });

    data = utils.minifyObj(data, this.abbreviationMap);

    var link = utils.createHash(data);
    return link;
  },
  parseHash: function (hash) {
    hash = hash || location.hash;
    hash = hash[0] === '#' ? hash.substr(1) : hash;

    var data = {
      host: settings.get('host'),
      pageType: 'mainpage',
      pageNumber: 1,
      tags: '',
      filter: '',
      link: ''
    };

    if (!hash) {
      data.tags = [];
      data.filter = [];
      return data;
    }

    var parsedData = utils.parseHash(hash);
    parsedData = utils.normalizeObj(parsedData, this.abbreviationMap);

    data = _.assign({}, data, parsedData);

    _.forEach(data, function (value, key) {
      data[key] = decodeURIComponent(value);
    });

    data.tags = data.tags.split(',').map(function (tag) {
      return tag.trim().replace(/\+/g, ' ').toLowerCase();
    }).filter(function (tag) {
      return tag.length > 0;
    });
    data.filter = _.reduce(data.filter.split(settings.get('andSplitter')), function (result, e) {
      result.push(e.split(settings.get('orSplitter')).map(function (tag) {
        return tag.trim().replace(/\+/g, ' ').toLowerCase();
      }).filter(function (tag) {
        return tag.length > 0;
      }));
      return result;
    }, []);

    data.pageNumber = +data.pageNumber;

    return data;
  }
};

window.parserHash = parserHash;

module.exports = parserHash;
