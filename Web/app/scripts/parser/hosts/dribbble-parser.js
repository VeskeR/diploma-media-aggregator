var parserHash = require('../parser-hash');
var settings = require('../../settings/settings');
var utils = require('../../utils');

var dribbbleParser = {
  base: 'https://dribbble.com',
  pageTypes: {
    mainPage: 'mainpage',
    imagePage: 'imagepage',
    unknown: 'unknown'
  },
  parse: function (urlsContent, data) {
    utils.log('Parsing dribbble.com page');
    var $doc = $(utils.replaceSrcs(urlsContent[0].contents));
    var pageType = data.pageType;
    switch (pageType) {
      case this.pageTypes.mainPage:
        return this.parseMainPage($doc);
        break;
      case this.pageTypes.imagePage:
        return this.parseImagePage($doc);
        break;
      case this.pageTypes.unknown:
      default:
        utils.log('Cannot parse dribbble.com page. Unkown type');
        break;
    }
  },
  parseImagesCount: function ($doc) {
    return 'unknown';
  },
  parseMainPage: function ($doc) {
    var images = [];

    $doc.find('.dribbble').each(function (i, v) {
      var $image = $(v);

      var image = {
        link: $image.find('.dribbble-link').attr('href'),
        smallSrc: $image.find('.dribbble-link img').attr('data-replaced-src'),
        keywords: []
      };

      image.link = parserHash.createHash({
        gps: 'dribbble.com',
        pageType: 'imagepage',
        link: image.link
      });
      image.smallSrc = utils.changePicSrc(image.smallSrc, 'small')
      image.largeSrc = utils.changePicSrc(image.smallSrc, 'large');
      image.hdSrc = utils.changePicSrc(image.smallSrc, 'hd');
      image.downloadLink = utils.getDownloadLink(image.hdSrc);

      if (settings.get('lowercaseTags')) {
        image.keywords = _.map(image.keywords, function (kw) {
          return kw.toLowerCase();
        });
      }

      image.keywords = _.uniq(image.keywords);

      image.keywords = _.filter(image.keywords, function (kw) {
        return !/[а-яА-ЯЁёіІїЇ]/.test(kw);
      });

      if (settings.get('keywordsSortBy') === 'alphabet') {
        image.keywords.sort();
      }

      images.push(image);
    });

    var result = {
      images: images,
      imagesPerPage: this.getImagesCountPerPage(),
      totalImages: this.parseImagesCount($doc)
    };
    result.totalPages = result.totalImages === 'unknown' ? 0 : Math.ceil(result.totalImages / result.imagesPerPage);

    return result;
  },
  parseImagePage: function ($doc) {
    var image = {
      link: location.hash,
      smallSrc: $doc.find('.the-shot img').attr('data-replaced-src'),
      similarImages: []
    };

    image.keywords = [];

    _.forEach($doc.find('.tag'), function (t) {
      var $t = $(t);
      image.keywords.push($t.text().trim());
    });

    image.smallSrc = utils.changePicSrc(image.smallSrc, 'small');
    image.largeSrc = utils.changePicSrc(image.smallSrc, 'large');
    image.hdSrc = utils.changePicSrc(image.smallSrc, 'hd');
    image.downloadLink = utils.getDownloadLink(image.hdSrc);

    if (settings.get('lowercaseTags')) {
      image.keywords = _.map(image.keywords, function (kw) {
        return kw.toLowerCase();
      });
    }

    image.keywords = _.uniq(image.keywords);

    image.keywords = _.filter(image.keywords, function (kw) {
      return !/[а-яА-ЯЁёіІїЇ]/.test(kw);
    });

    if (settings.get('keywordsSortBy') === 'alphabet') {
      image.keywords.sort();
    }

    $doc.find('.more-thumbs a').each(function (i, v) {
      var $similarImage = $(v);

      var similarImage = {
        link: $similarImage.attr('href'),
        smallSrc: $similarImage.find('img').attr('data-replaced-src'),
        keywords: []
      };

      similarImage.link = parserHash.createHash({
        gps: 'dribbble.com',
        pageType: 'imagepage',
        link: similarImage.link
      });

      similarImage.smallSrc = utils.changePicSrc(similarImage.smallSrc, 'small');
      similarImage.largeSrc = utils.changePicSrc(similarImage.smallSrc, 'large');
      similarImage.hdSrc = utils.changePicSrc(similarImage.smallSrc, 'hd');
      similarImage.downloadLink = utils.getDownloadLink(similarImage.hdSrc);

      if (settings.get('lowercaseTags')) {
        similarImage.keywords = _.map(similarImage.keywords, function (kw) {
          return kw.toLowerCase();
        });
      }

      similarImage.keywords = _.uniq(similarImage.keywords);

      similarImage.keywords = _.filter(similarImage.keywords, function (kw) {
        return !/[а-яА-ЯЁёіІїЇ]/.test(kw);
      });

      if (settings.get('keywordsSortBy') === 'alphabet') {
        similarImage.keywords.sort();
      }

      image.similarImages.push(similarImage);
    });

    return image;
  },
  createLink: function (data) {
    switch (data.pageType) {
      case 'mainpage':
        return this.createMainPageLink(data);
        break;
      case 'imagepage':
        return this.createImagePageLink(data);
        break;
      default:
        utils.log('Unknown page type "' + data.pageType + '". Cannot create dribbble.com link');
        break;
    }
  },
  createMainPageLink: function (data) {
    var link = this.base;

    if (data.tags.length > 0) {
      link += '/tags/' + _.map(data.tags, function (tag) {
        return tag.replace(/\s/g, '_');
      }).join(',');
    }

    link += '?page=' + data.pageNumber + '&per_page=' + this.getImagesCountPerPage();

    if (data.tags.length === 0) {
      link += '&sort=recent';
    }

    return link;
  },
  createImagePageLink: function (data) {
    return this.base + data.link;
  },
  getImagesCountPerPage: function () {
    return 12;
  }
};

window.dribbbleParser = dribbbleParser;

module.exports = dribbbleParser;
