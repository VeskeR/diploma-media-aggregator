var parserHash = require('./parser-hash');
var settings = require('../settings/settings');
var utils = require('../utils');

var parser = _.assign({}, parserHash, {
  parsers: {
    'dribbble.com': require('./hosts/dribbble-parser'),
  },
  parse: function (urlsContent, data, host) {
    var self = this;
    return new Promise(function (resolve, reject) {
      utils.log('Trying to parse pages:');
      utils.log(urlsContent);
      host = host || data.host || settings.get('host');
      if (self.parsers[host]) {
        resolve(self.parsers[host].parse(urlsContent, data));
      } else {
        utils.log('Unknown host "' + host + '". Cannot parse pages');
        reject();
      }
    });
  },
  createLink: function (data, host) {
    host = host || data.host || settings.get('host');
    if (this.parsers[host]) {
      return this.parsers[host].createLink(data);
    } else {
      utils.log('Unknown host "' + host + '". Cannot create link from data:');
      utils.log(utils.json(data));
    }
  },
  getImagesCountPerPage: function (host) {
    host = host || settings.get('host');
    if (this.parsers[host]) {
      return this.parsers[host].getImagesCountPerPage();
    } else {
      utils.log('Unknown host "' + host + '". Cannot return images count per page');
    }
  }
});

window.parser = parser;

module.exports = parser;
