var parser = require('./parser/parser');
var fullscreenUI = require('./fullscreen-ui');
var navigation = require('./navigation');
var pageInfo = require('./page-info');
var popup = require('./popup');
var settings = require('./settings/settings');
var settingsManager = require('./settings/settings-manager');
var settingsMenu = require('./settings/settings-menu');
var tabNavigator = require('./tab-navigator');
var utils = require('./utils');

var $settingsMenu = $('.settings-menu');
var $header = $('.header');
var $imagesCount = $('.images-count');
var $openAllImages = $('.open-all-images');
var $mainContainer = $('.main-container');
var $navigation = $('.navigation');

var fullscreenImages = [];
var fullscreenTotalImagesCount = 0;
var currentFullscreenImage = 0;
var initialFullscreenPage = 0;
var currentFullscreenPage = 0;
var lowestParsedFullscreenPage = 0;
var fetchingLowest = false;
var highestParsedFullscreenPage = 0;
var fetchingHighest = false;
var reachedTop = false;
var autoSlideTimeout = null;
var fetchingMultiPage = false;
var fullscreenTransitioning = false;

var page = {
  show: showPage,
  goFullscreen: goFullscreen,
  getMainPageTitle: getMainPageTitle,
  getImagePageTitle: getImagePageTitle,
  setTitle: setTitle,
  images: null,
  image: null,
  fullscreenImages: null,
  currentFullscreenImage: 0,
  checkOpenedTags: checkOpenedTags
};

function showPage(hash) {
  hash = hash || location.hash.substr(1);
  pageInfo.parsePageInfo(hash);
  utils.log('Page information:');
  utils.log(utils.json(pageInfo.data));

  if (pageInfo.data.tags.length > 0) {
    settings.addVisitedImages(pageInfo.data.tags[0]);
  }

  utils.getUrlsContent(parser.createLink(pageInfo.data))
  .then(renderPage);
}

function renderPage(urlsContent) {
  window.urlsContent = urlsContent;

  $mainContainer.empty();
  $mainContainer.removeClass('fullscreen');
  $mainContainer.removeClass('fullscreen-background');
  $header.show();

  if (pageInfo.data.pageType === 'mainpage') {
    renderMainPage(urlsContent);
  } else {
    hideImagesCount();
    navigation.hide();
    renderImagePage(urlsContent);
  }
  utils.log('Rendering page has been finished');
}

function renderMainPage(urlsContent) {
  utils.log('Rendering main page number: ' + pageInfo.data.pageNumber + '. Tags: ' + pageInfo.getStringTags());
  var data = _.assign({}, pageInfo.data);
  data.fetchAllImages = false;
  parser.parse(urlsContent, data)
  .then(function (result) {
    var images = result.images;
    var imagesCount = result.totalImages;

    navigation.draw(imagesCount);

    var hideSeen = settings.get('hideImagesOnMainPage');
    var hideWithKeywords = settings.get('hideImagesWithKeywords');
    var dimSeenImages = settings.get('dimSeenImages');
    var seenImages = settings.get('seenImages');
    var commonKeywords = settings.get('commonKeywords');
    images = _.filter(images, function (image) {
      var show = true;

      if (hideSeen) {
        if (hideWithKeywords) {
          show = !_.includes(seenImages, utils.getImageId(image.downloadLink));
        } else {
          show = !(_.includes(seenImages, utils.getImageId(image.downloadLink)) &&
                  _.difference(image.keywords, commonKeywords).length === 0);
        }
      }

      if (show === false && dimSeenImages) {
        show = true;
        image.dim = true;
      }

      return show && filterTags(image.keywords);
    });

    page.images = images;

    displayImages(images);
    settingsManager.applyThumbsSize(settings.get('thumbsSize'));
    if (settings.get('hideImagesCountOnMainPage') && (imagesCount === 'unknown' || pageInfo.data.tags.length === 0)) {
      hideImagesCount();
    } else {
      displayImagesCount(imagesCount);
    }

    var title = getMainPageTitle(imagesCount);
    setTitle(title);
    tabNavigator.updateCurrentTab(null, title, images[0], 'mainpage');
  });
}

function displayImages(images) {
  var $images = images.map(function (image) {
    var $image = $("<div class='thumb'></div>");
    for (var prop in image) {
      $image.attr('data-' + prop, image[prop]);
    }

    var $imageLink = $("<a class='thumb-link'></a>");
    $imageLink.attr('href', '#' + image.link);

    var $imageImg = $("<img class='thumb-img'>");
    if (image.dim) {
      $imageImg.addClass('dimmed');
    }
    utils.setImgSizingBehaviour($imageImg);
    $imageImg.attr('src', utils.getImgSrc(image.smallSrc, image.largeSrc, null, 300, 200));

    $imageLink.append($imageImg);
    $image.append($imageLink);

    $imageLink.on('click', function () {
      if (settings.get('pickSeenImages')) {
        settings.addSeenImages(utils.getImageId(image.downloadLink));
      }
    });

    return $image;
  });

  $mainContainer.empty();
  $images.forEach(function ($image) {
    $mainContainer.append($image);
  });
}

function displayImagesCount(imagesCount) {
  if (settings.get('debug')) {
    $imagesCount.html('Wallpaper: ' + pageInfo.getStringTags(true) + ' (' + (imagesCount === 'unknown' ? 0 : imagesCount) +' placeholder, ' + getPageCount(imagesCount) + ' pages)');
  } else {
    $imagesCount.html('Wallpaper: ' + pageInfo.getStringTags(true) + ' (' + (imagesCount === 'unknown' ? 0 : imagesCount) +' images, ' + getPageCount(imagesCount) + ' pages)');
  }
  $imagesCount.show();

  var tag = pageInfo.data.tags[0];

  if (settings.get('openAllImagesButton')) {
    $openAllImages.show();
  }
}

function getPageCount(imagesCount) {
  return imagesCount === 'unknown' ? 0 : Math.ceil(imagesCount / parser.getImagesCountPerPage());
}

function hideImagesCount() {
  $imagesCount.hide();
  $openAllImages.hide();
}

function renderImagePage(urlsContent) {
  utils.log('Rendering image page');
  parser.parse(urlsContent, pageInfo.data)
  .then(function (image) {
    console.log(image);

    if (settings.get('hideImagesOnImagePage')) {
      var seenImages = settings.get('seenImages');
      var commonKeywords = settings.get('commonKeywords');

      image.similarImages = _.filter(image.similarImages, function (image) {
        var show = true;

        if (settings.get('hideImagesWithKeywords')) {
          show = !_.includes(seenImages, utils.getImageId(image.downloadLink));
        } else {
          show = !(_.includes(seenImages, utils.getImageId(image.downloadLink)) &&
                  _.difference(image.keywords, commonKeywords).length === 0);
        }

        if (show === false && settings.get('dimSeenImages')) {
          show = true;
          image.dim = true;
        }

        return show;
      });
    }

    page.image = image;

    displayImage(image);
    settingsManager.applyThumbsSize(settings.get('thumbsSize'));

    var title = getImagePageTitle(image);
    setTitle(title);
    tabNavigator.updateCurrentTab(image.link, title, image, 'imagepage');
  });
}

function displayImage(image) {
  var $image = $("<div class='image'></div>");
  for (var prop in image) {
    if (prop !== 'similarImages' && prop !== 'downloadLink') {
      $image.attr('data-' + prop, image[prop]);
    }
  }

  var $imageDownloadLink = $("<a class='image-download-link'></a>");
  $imageDownloadLink.attr('href', image.downloadLink);
  $imageDownloadLink.attr('download', utils.getDownloadFilename(image.hdSrc));
  if (settings.get('disableImageDownload')) {
    $imageDownloadLink.css('pointer-events', 'none');
  }

  var $imageImg = $("<img class='image-img'>");
  utils.setImgSizingBehaviour($imageImg);
  $imageImg.attr('src', utils.getImgSrc(image.smallSrc, image.largeSrc, image.hdSrc, 1920, 1080));

  $imageDownloadLink.append($imageImg);
  $image.append($imageDownloadLink);

  var $keywordsWrapper = $("<div class='regular-keywords keywords border'></div>");
  var $commonKeywordsWrapper = $("<div class='common-keywords keywords'></div>");

  if (!settings.get('showCommonKeywords')) {
    $keywordsWrapper.removeClass('border');
    $commonKeywordsWrapper.addClass('hidden');
    // $commonKeywordsWrapper.hide();
  }

  var $keywords = image.keywords.map(function (keyword, i, keywords) {
    var $keyword = $("<a class='keyword'></a>");
    $keyword.attr('href', '#' + parser.createHash({
      host: pageInfo.data.host,
      pageType: 'mainpage',
      pageNumber: 1,
      tags: [keyword],
      filter: [],
      link: ''
    }));
    $keyword.attr('data-text', keyword);
    if (settings.get('debug')) {
      $keyword.html('placeholder');
    } else {
      $keyword.html(keyword);
    }
    if (i < keywords.length - 1) {
      $keyword.html($keyword.html() + ', ');
    }
    return $keyword;
  });

  var commonKeywords = _.intersection(settings.get('commonKeywords'), image.keywords);
  var visitedImages = settings.get('visitedImages');

  $keywords.forEach(function ($keyword) {
    var $clonedKeyword = $keyword.clone();

    $keyword.addClass('regular-keyword');
    $clonedKeyword.addClass('common-keyword');

    var kwText = settings.get('debug') ? 'placeholder' : $keyword.attr('data-text');

    if (_.includes(tabNavigator.getOpenedTags(), kwText)) {
      $keyword.addClass('opened-tab');
      $clonedKeyword.addClass('opened-tab');
    }

    if (_.includes(visitedImages, kwText)) {
      $keyword.addClass('visited');
      $clonedKeyword.addClass('visited');
    }

    $keywordsWrapper.append($keyword);
    $commonKeywordsWrapper.append($clonedKeyword);

    if (_.indexOf(commonKeywords, $keyword.attr('data-text')) !== -1) {
      $keyword.hide();
    } else {
      $clonedKeyword.hide();
    }
  });

  $keywordsWrapper.add($commonKeywordsWrapper).on('click', function (event) {
    if (settings.get('pickCommonKeywords')) {
      event.preventDefault();
      var $keyword = $(event.target);
      if ($keyword.hasClass('keyword')) {
        var text = $keyword.attr('data-text');

        if ($keyword.hasClass('regular-keyword')) {
          settings.addCommonKeywords(text);
          $keyword.hide();
          $commonKeywordsWrapper.find('[data-text="' + text + '"]').show();
        } else {
          settings.removeCommonKeywords(text);
          $keyword.hide();
          $keywordsWrapper.find('[data-text="' + text + '"]').show();
        }
      }
    }
  });

  var $openNewKeywordsBtn = $('<a class="open-new-keywords-btn">Open new keywords</a>');
  $openNewKeywordsBtn.on('click', function () {
    var keywordsToOpen = [];

    image.keywords.forEach(function (kw) {
      kw = settings.get('debug') ? 'placeholder' : kw;

      if (!_.includes(tabNavigator.getOpenedTags(), kw) && !_.includes(visitedImages, kw)) {
        keywordsToOpen.push(kw);
      }
    });

    keywordsToOpen.forEach(function (kw) {
      var link = parser.createHash({
        pageNumber: 1,
        tags: [kw],
        filter: []
      });

      tabNavigator.createFullTab(link, '? images: ' + kw, image, tabNavigator.currentTab + 1);
    });

    checkOpenedTags();
  });

  if (!settings.get('openNewKeywordsBtn')) {
    $openNewKeywordsBtn.hide();
  }

  var $similarImagesWrapper = $("<div class='similar-images'></div>");
  var $similarImages = image.similarImages.map(function (similarImage) {
    var $similarImage = $("<div class='thumb similar-image'></div>");
    for (var prop in similarImage) {
      $similarImage.attr('data-' + prop, similarImage[prop]);
    }

    var $similarImageLink = $("<a class='thumb-link similar-image-link'></a>");
    $similarImageLink.attr('href', '#' + similarImage.link);

    var $similarImageImg = $("<img class='thumb-img similar-image-img'>");
    if (similarImage.dim) {
      $similarImageImg.addClass('dimmed');
    }
    utils.setImgSizingBehaviour($similarImageImg);
    $similarImageImg.attr('src', utils.getImgSrc(similarImage.smallSrc, similarImage.largeSrc, null, 300, 200));

    $similarImageLink.append($similarImageImg);
    $similarImage.append($similarImageLink);

    $similarImage.on('click', function () {
      if (settings.get('pickSeenImages')) {
        settings.addSeenImages(utils.getImageId(similarImage.downloadLink));
      }
    });

    return $similarImage;
  });
  $similarImages.forEach(function ($similarImage) {
    $similarImagesWrapper.append($similarImage);
  });

  $mainContainer.empty();
  $mainContainer.append($image)
  .append($openNewKeywordsBtn)
  .append($keywordsWrapper)
  .append($commonKeywordsWrapper);

  $mainContainer.append($similarImagesWrapper);

  if (settings.get('pickSeenImages')) {
    settings.addSeenImages(utils.getImageId(image.downloadLink));
  }
}

function getMainPageTitle(imagesCount) {
  imagesCount = imagesCount || 'unknown';
  if (settings.get('hideImagesCountOnMainPage') && imagesCount === 'unknown' && pageInfo.data.tags.length === 0) {
    return 'Media aggregator';
  } else {
    imagesCount = imagesCount === 'unknown' ? 0 : imagesCount;
    if (settings.get('debug')) {
      return imagesCount + ' placeholder: ' + pageInfo.getStringTags(true);
    } else {
      return imagesCount + ' images: ' + pageInfo.getStringTags(true);
    }
  }
}

function getImagePageTitle(image) {
  var keywords = image.keywords;
  if (!(settings.get('showCommonKeywords') && settings.get('showCommonKeywordsInTabs'))) {
    keywords = _.difference(keywords, settings.get('commonKeywords'));
  }
  if (settings.get('debug')) {
    keywords = keywords.map(function () { return 'placeholder'; });
  }
  return keywords.join(', ');
}

function goFullscreen() {
  utils.log('Entered fullscreen mode');

  fullscreenImages = [];
  fullscreenTotalImagesCount = 0;
  currentFullscreenImage = -1;
  initialFullscreenPage = pageInfo.data.pageNumber;
  currentFullscreenPage = pageInfo.data.pageNumber;
  lowestParsedFullscreenPage = pageInfo.data.pageNumber;
  highestParsedFullscreenPage = pageInfo.data.pageNumber;
  fetchingLowest = false;
  fetchingHighest = false;
  reachedTop = false;
  fetchingMultiPage = false;
  fullscreenTransitioning = false;

  $mainContainer.empty();
  $mainContainer.addClass('fullscreen');
  if (settings.get('useFullscreenBackground')) {
    $mainContainer.addClass('fullscreen-background');
  }
  settingsMenu.close();
  $header.hide();
  $imagesCount.hide();
  navigation.hide();

  page.fullscreenImages = fullscreenImages;

  drawFullscreenUI();
  getFullscreenImages(pageInfo.data.pageNumber, function () {
    currentFullscreenImage = -1;
    nextFullscreenImage();
  });
}

function drawFullscreenUI() {
  var $background = $('<div class="fullscreen-background-fill"></div>');
  var $image = $("<div class='fullscreen-image fullscreen-image-fading'></div>");
  var $imageFading = $("<div class='fullscreen-image-fading support'></div>");
  var $imageImgCenter = $("<img class='fullscreen-image-img'></div>");
  var $imageImgRight = $("<img class='fullscreen-image-img'></div>");

  utils.setImgSizingBehaviour($imageImgCenter);
  utils.setImgSizingBehaviour($imageImgRight);

  switch (settings.get('fullscreenTransitionType')) {
    case 'switch':
      $imageImgCenter.addClass('current');
      $imageImgRight.addClass('next');
      break;
    case 'smooth':
      var transitionSpeed = settings.get('fullscreenTransitionSpeed');
      $imageImgCenter.css('transition', transitionSpeed + 's');
      $imageImgRight.css('transition', transitionSpeed + 's');
      $imageImgCenter.css('position', 'absolute');
      $image.css('overflow', 'hidden');

      $imageImgCenter.addClass('center');
      $imageImgRight.addClass('right');
      break;
    case 'fading':
      var speed = settings.get('fullscreenTransitionSpeed');

      if (settings.get('useFullscreenBackground')) {
        $image.addClass('current');
        $imageFading.addClass('next');
        $image.addClass('fading');
        $imageFading.addClass('fading');

        // $image.css('transition', 'background-image ' + speed + 's ease-in');
        // $imageFading.css('transition', 'background-image ' + speed + 's ease-in');
        $image.css('transition', 'opacity ' + speed + 's ease-in');
        $imageFading.css('transition', 'opacity ' + speed + 's ease-in');
      } else {
        $imageImgCenter.addClass('current');
        $imageImgRight.addClass('next');
        $imageImgCenter.addClass('fading');
        $imageImgRight.addClass('fading');

        $imageImgCenter.css('transition', 'opacity ' + speed + 's ease-in');
        $imageImgRight.css('transition', 'opacity ' + speed + 's ease-in');
      }
      break;
    case 'default':
    default:
      $imageImgCenter.addClass('center');
      $imageImgRight.addClass('right');
      break;
  }

  $imageImgRight.css('position', 'absolute');

  $image.append($imageImgCenter);
  $image.append($imageImgRight);

  var $controls = [];

  var mode = settings.get('fullscreenMode');
  for (var uiName in fullscreenUI[mode]) {
    if (fullscreenUI[mode].hasOwnProperty(uiName)) {
      var $control;
      switch (uiName) {
        case 'fullscreenDownloadLink':
          $control = appendTextControl($("<a class='fullscreen-control fullscreen-download-link' download></a>"), 'downloadLink');
          break;
        case 'goToImage':
          $control = appendTextControl($("<a class='fullscreen-control go-to-image'></a>"), 'goToImage');
          break;
        default:
          $control = createFullscreenUIControl(uiName);
          break;
      }

      if (settings.get('showFullscreenUIHelp')) {
        $control.addClass('show-help');
      }

      // $control.css('background-color', 'rgba(' + utils.randomBetween(0, 255) + ', ' + utils.randomBetween(0, 255) + ', ' + utils.randomBetween(0, 255) + ', 0.5)');

      $controls.push($control);
    }
  }

  var $messageBox = $("<div class='fullscreen-control message-box'></div>");
  var $title = $("<div class='fullscreen-control title'></div>");

  $mainContainer.append($background)
  .append($image)
  .append($imageFading)
  .append($messageBox)
  .append($title);

  $controls.forEach(function ($control) {
    $mainContainer.append($control);
  });

  if (settings.get('disableImageDownload')) {
    $mainContainer.find('.fullscreen-download-link').css('pointer-events', 'none');
  }

  $messageBox.hide();

  applyUILayout();
  bindFullscreenEvents();
}

function createFullscreenUIControl(name) {
  var control = $("<div class='fullscreen-control " + utils.camelCaseToDash(name) + "'></div>");
  appendTextControl(control, name);
  if (settings.get('showFullscreenUIHelp')) {
    control.addClass('show-help');
  }
  return control;
}

function appendTextControl(control, name) {
  var text = settings.get('debug') ? 'placeholder' : utils.camelCaseToNatural(name);
  var controlText = $('<div class="fullscreen-control-text ' + utils.camelCaseToDash(name) + '-text">' + text + '</div>');
  control.append(controlText);
  return control;
}

function applyUILayout() {
  var gs = fullscreenUI.gridSize;
  var mode = settings.get('fullscreenMode');
  for (var uiName in fullscreenUI[mode]) {
    if (fullscreenUI[mode].hasOwnProperty(uiName)) {
      var p = fullscreenUI[mode][uiName];
      var left = ((p[0][1] - 1) / gs[1] * 100) + '%';
      var top = ((p[0][0] - 1) / gs[0] * 100) + '%';
      var width = ((p[1][1] - p[0][1] + 1) / gs[1] * 100) + '%';
      var height = ((p[1][0] - p[0][0] + 1) / gs[0] * 100) + '%';
      $('.' + utils.camelCaseToDash(uiName)).css({
        'left': left,
        'top': top,
        'width': width,
        'height': height
      });
    }
  }
}

function bindFullscreenEvents() {
  $('.next-image').on('click', function (e) {
    if (!fullscreenTransitioning) {
      nextFullscreenImage();
    } else {
      e.preventDefault();
    }
  });

  $('.prev-image').on('click', function (e) {
    if (!fullscreenTransitioning) {
      prevFullscreenImage();
    } else {
      e.preventDefault();
    }
  });

  $('.yes-choice').on('click', function (e) {
    if (!fullscreenTransitioning) {
      downloadFullscreenImage();
      nextFullscreenImage();
    } else {
      e.preventDefault();
    }
  });

  $('.no-choice').on('click', function (e) {
    if (!fullscreenTransitioning) {
      nextFullscreenImage();
    } else {
      e.preventDefault();
    }
  });

  $('.exit-fullscreen').on('click', function (e) {
    if (!fullscreenTransitioning) {
      exitFullscreenWithRedirect();
    } else {
      e.preventDefault();
    }
  });

  $('.toggle-ui-help').on('click', function (e) {
    toggleUIHelp();
  });

  $('.add-seen-image').on('click', function () {
    settings.addSeenImages(utils.getImageId(fullscreenImages[currentFullscreenImage].downloadLink));
    popup.show('Added seen image');
  });

  $('.remove-seen-image').on('click', function () {
    settings.removeSeenImages(utils.getImageId(fullscreenImages[currentFullscreenImage].downloadLink));
    popup.show('Removed seen image');
  });

  $('.create-new-tab').on('click', function () {
    if (settings.get('preventLastSlideTabCreation') && currentFullscreenImage >= fullscreenImages.length - 1) {
      return;
    } else {
      var image = fullscreenImages[currentFullscreenImage];
      tabNavigator.createFullTab(image.link, getImagePageTitle(image), image, tabNavigator.currentTab + 1);
    }
  });

  // $('.go-to-image').on('click', function () {
  //   exitFullscreen();
  // });
}

function toggleUIHelp() {
  $('.fullscreen-control').toggleClass('show-help');
}

function getFullscreenImages(pageNumber, callback) {
  if (!pageNumber) return;
  var highest = false;
  if (pageNumber > highestParsedFullscreenPage) {
    fetchingHighest = true;
    highest = true;
    highestParsedFullscreenPage = pageNumber;
  }
  if (pageNumber < lowestParsedFullscreenPage) {
    fetchingLowest = true;
    lowestParsedFullscreenPage = pageNumber;
  }
  utils.log('Fetching images for fullscreen from page number ' + pageNumber + '. Tags: ' + pageInfo.getStringTags());
  utils.getUrlsContent(parser.createLink(_.assign({}, pageInfo.data, { pageNumber: pageNumber })))
  .then(function (urlsContent) {
    parseFullscreenImages(urlsContent, function (images, unfilteredImages, totalImages) {
      if (settings.get('preloadImages')) {
        preloadFullscreenImages(images);
      }
      fullscreenTotalImagesCount = totalImages;
      addNewImages(pageNumber, images);
      if (highest) {
        if (unfilteredImages.length === 0) {
          reachedTop = true;
        }
        fetchingHighest = false;
      } else {
        fetchingLowest = false;
      }
      utils.log('Fetched and parsed ' + images.length + ' new images from page number ' +
                  pageNumber + '. Tags: ' + pageInfo.getStringTags() + '. New number of images: ' +
                  fullscreenImages.length);
      setTitle(getFullscreenTitle());
      setFullscreenUITitle();
      if (callback) {
        callback();
      }
    });
  });
}

function parseFullscreenImages(urlsContent, callback) {
  var data = _.assign({}, pageInfo.data);
  data.fetchAllImages = true;
  parser.parse(urlsContent, data)
  .then(function (result) {
    var images = result.images;
    var totalImages = result.totalImages;
    var filteredImages = _.filter(images, function (image) {
      return filterTags(image.keywords);
    });

    var completed = 0;
    for (var i = 0; i < filteredImages.length; i++) {
      (function () {
        var image = filteredImages[i];
        utils.getDownloadLink(image.hdSrc, function (link) {
          image.downloadLink = link;
          completed++;
          if (completed === filteredImages.length) {

            if (settings.get('hideSeenImages')) {
              var seenImages = settings.get('seenImages');
              filteredImages = _.filter(filteredImages, function (image) {
                return !_.includes(seenImages, utils.getImageId(image.downloadLink));
              });
            }

            callback(filteredImages, images, totalImages);
          }
        });
      })();
    }

    if (filteredImages.length === 0) {
      callback(filteredImages, images, totalImages);
    }
  });
}

function preloadFullscreenImages(images) {
  var images = [];
  for (i = 0; i < images.length; i++) {
    images[i] = new Image();
    images[i].src = images[i].hdSrc;
  }
}

function addNewImages(pageNumber, images) {
  if (pageNumber > initialFullscreenPage) {
    Array.prototype.push.apply(fullscreenImages, images);
  } else {
    Array.prototype.unshift.apply(fullscreenImages, images);
    currentFullscreenImage += images.length;
  }
}

function nextFullscreenImage() {
  utils.log('Going to next fullscreen image');
  clearTimeout(autoSlideTimeout);

  changeCurrentImageIndex(1);
  changeCurrentImage(currentFullscreenImage);
  currentFullscreenPage = getCurrentFullscreenPage();
  checkForExtremeIndex();
  setTitle(getFullscreenTitle());
  setFullscreenUITitle();
  if (settings.get('autoSlideInFullscreen')) {
    queueAutoSlide();
  }
}

function prevFullscreenImage() {
  utils.log('Going to previous fullscreen image');
  changeCurrentImageIndex(-1);
  changeCurrentImage(currentFullscreenImage);
  currentFullscreenPage = getCurrentFullscreenPage();
  checkForExtremeIndex();
  setTitle(getFullscreenTitle());
  setFullscreenUITitle();
}

function getFullscreenTitle() {
  var title = '';
  var tags = '';
  tags = pageInfo.getStringTags(true);

  if (pageInfo.isUsingFilter() || settings.get('hideSeenImages')) {
    if (fullscreenTotalImagesCount === 'unknown') {
      title += (currentFullscreenImage + 1) + '/' + fullscreenImages.length;
    } else {
      title += (currentFullscreenImage + 1) + '/' + fullscreenImages.length + '/' + (getPageCount(fullscreenTotalImagesCount) - highestParsedFullscreenPage);
    }
  } else {
    var currentPage = currentFullscreenPage;
    var currentImage = currentFullscreenImage % parser.getImagesCountPerPage() + 1;
    var leftImages = fullscreenTotalImagesCount === 'unknown' ? 'unknown' : fullscreenTotalImagesCount - ((currentPage - 1) * parser.getImagesCountPerPage() + currentImage);
    if (settings.get('expansiveFullsreenTitle')) {
      title += 'page ' + currentPage + ', image ' + currentImage;
      if (fullscreenTotalImagesCount !== 'unknown') {
        title += ', left ' + leftImages;
      }
    } else {
      title += currentPage + '/' + currentImage;
      if (fullscreenTotalImagesCount !== 'unknown') {
        title += '/' + leftImages;
      }
    }
  }

  if (tags) {
    title += ' ' + tags;
  }

  return title;
}

function changeCurrentImageIndex(shift) {
  currentFullscreenImage += shift;
  if (currentFullscreenImage < 0) {
    currentFullscreenImage = 0;
    utils.log('Reached first fullscreen image');
    showMessageBox('First slide', 1000);
  }
  if (currentFullscreenImage >= fullscreenImages.length) {
    currentFullscreenImage = fullscreenImages.length > 0 ? fullscreenImages.length - 1 : 0;
    utils.log('Reached last fullscreen image');
    showMessageBox('Last slide', 1000);
  }
}

function showMessageBox(message, ms) {
  popup.show(message, ms);

  // $messageBox = $('.message-box');
  // $messageBox.html(message);
  // $messageBox.show();
  // setTimeout(function () {
  //   $messageBox.hide();
  // }, ms);
}

function changeCurrentImage(i) {
  if (fullscreenImages[i]) {
    var currentImage = fullscreenImages[i];
    var nextImage = (i + 1) < fullscreenImages.length ? fullscreenImages[i + 1] : currentImage;
    var $downloadLink = $('.fullscreen-download-link');

    var $image = $('.fullscreen-image');
    for (var prop in currentImage) {
      if (prop !== 'downloadLink') {
        $image.attr('data-' + prop, currentImage[prop]);
      }
    }

    var $imageImgCenter = $('.fullscreen-image-img.center');
    var $imageImgRight = $('.fullscreen-image-img.right');

    var $imageImgCurrent = $('.fullscreen-image-img.current');
    var $imageImgNext = $('.fullscreen-image-img.next');

    var currentImageSrc = utils.getImgSrc(currentImage.smallSrc, currentImage.largeSrc, currentImage.hdSrc, 1920, 1080);
    var nextImageSrc = utils.getImgSrc(nextImage.smallSrc, nextImage.largeSrc, nextImage.hdSrc, 1920, 1080);

    if (settings.get('useFullscreenBackground')) {
      if (settings.get('fullscreenTransitionType') === 'fading') {
        var $imageCurrent = $('.fullscreen-image-fading.current');
        var $imageNext = $('.fullscreen-image-fading.next');

        if (!$imageCurrent.css('background-image').includes(currentImageSrc)) {
          $imageNext.css('background-image', "url('" + currentImageSrc + "')");
          fullscreenTransitioning = true;

          $imageNext.addClass('current');
          $imageCurrent.removeClass('current');
          $imageNext.removeClass('next');
          $imageCurrent.addClass('next');

          setTimeout(function () {
            $imageCurrent.css('background-image', "url('" + nextImageSrc + "')");
            fullscreenTransitioning = false;
          }, settings.get('fullscreenTransitionSpeed') * 1000);
        }
      } else {
        $image.css('background-image', "url('" + currentImageSrc + "')");
        (new Image()).src = nextImageSrc;
      }
    } else {
      switch (settings.get('fullscreenTransitionType')) {
        case 'switch':
          $imageImgNext.addClass('current');
          $imageImgCurrent.removeClass('current');
          $imageImgNext.removeClass('next');
          $imageImgCurrent.addClass('next');

          $imageImgCurrent.attr('src', nextImageSrc);
          $imageImgNext.attr('src', currentImageSrc);
          break;
        case 'smooth':
          fullscreenTransitioning = true;

          $imageImgCenter.removeClass('current');
          $imageImgRight.addClass('current');

          $imageImgRight.attr('src', currentImageSrc);

          var transitionSpeed = settings.get('fullscreenTransitionSpeed');

          $imageImgCenter.removeClass('center');
          $imageImgCenter.addClass('left');

          $imageImgRight.removeClass('right');
          $imageImgRight.addClass('center');

          setTimeout(function () {
            $imageImgCenter.attr('src', nextImageSrc);
            $imageImgCenter.addClass('force-right');
            $imageImgCenter.removeClass('left');
            $imageImgCenter.addClass('right');
            $imageImgCenter.removeClass('force-right');

            setTimeout(function () {
              fullscreenTransitioning = false;
            }, settings.get('fullscreenTransitioningThreshold'));
          }, transitionSpeed * 1000);
          break;
        case 'fading':
          if ($imageImgCurrent.attr('src') !== currentImageSrc) {
            $imageImgNext.attr('src', currentImageSrc);
            fullscreenTransitioning = true;

            $imageImgNext.addClass('current');
            $imageImgCurrent.removeClass('current');
            $imageImgNext.removeClass('next');
            $imageImgCurrent.addClass('next');

            setTimeout(function () {
              $imageImgCurrent.attr('src', nextImageSrc);
              fullscreenTransitioning = false;
            }, settings.get('fullscreenTransitionSpeed') * 1000);
          }
          break;
        case 'default':
          $imageImgCenter.attr('src', currentImageSrc);
          $imageImgRight.attr('src', nextImageSrc);
          break;
      }
    }

    $downloadLink.attr('href', currentImage.downloadLink);
    $downloadLink.attr('download', utils.getDownloadFilename(currentImage.hdSrc));
    $('.go-to-image').attr('href', '#' + currentImage.link);

    if (settings.get('pickSeenImages')) {
      settings.addSeenImages(utils.getImageId(currentImage.downloadLink));
    }

    if (settings.get('autoDownloadInFullscreen')) {
      downloadFullscreenImage();
    }

    page.currentFullscreenImage = i;
    utils.log('Current fullscreen image index: ' + i);
  } else {
    utils.log('No fullscreen image with index: ' + i);
  }

  utils.log('Total count of images: ' + fullscreenImages.length);
}

function checkForExtremeIndex() {
  if (!fetchingLowest && lowestParsedFullscreenPage > 1 && currentFullscreenImage < parser.getImagesCountPerPage()) {
    getFullscreenImages(lowestParsedFullscreenPage - 1);
  }

  if (!fetchingHighest && (pageInfo.data.tags.length === 0 || highestParsedFullscreenPage <= getPageCount(fullscreenTotalImagesCount)) && currentFullscreenImage >= fullscreenImages.length - parser.getImagesCountPerPage()) {
    if (pageInfo.isUsingFilter() || settings.get('hideSeenImages')) {
      if (!fetchingMultiPage) {
        fetchingMultiPage = true;
        var completed = 0;
        var baseIndex = highestParsedFullscreenPage;
        var multiPageFetchCount = parseInt(settings.get('multiPageFetchCount'));
        for (var i = 0; i < multiPageFetchCount; i++) {
          var pageNumber = baseIndex + 1 + i;
          if (fullscreenTotalImagesCount === 'unknown' || pageNumber <= getPageCount(fullscreenTotalImagesCount)) {
            getFullscreenImages(pageNumber, function () {
              completed++;
              if (completed === multiPageFetchCount) {
                fetchingMultiPage = false;
              }
            });
          }
        }
      }
    } else {
      getFullscreenImages(highestParsedFullscreenPage + 1);
    }
  }
}

function exitFullscreenWithRedirect(link) {
  var currFsPage = currentFullscreenPage;
  exitFullscreen();

  link = link || parser.createHash(_.assign({}, pageInfo.data, {
    pageNumber: currFsPage
  }));
  // link = link || getLink(currFsPage, pageInfo.getURLTags());
  if (location.hash.substr(1) !== link) {
    location.hash = link;
  } else {
    showPage(link);
  }
}

function exitFullscreen() {
  utils.log('Closed fullscreen mode');

  clearTimeout(autoSlideTimeout);

  fullscreenImages = [];
  fullscreenTotalImagesCount = 0;
  currentFullscreenImage = 0;
  initialFullscreenPage = 0;
  currentFullscreenPage = 0;
  lowestParsedFullscreenPage = 0;
  highestParsedFullscreenPage = 0;
  fetchingLowest = false;
  fetchingHighest = false;
  reachedTop = false;
  fetchingMultiPage = false;
  fullscreenTransitioning = false;

  // $mainContainer.empty();
  // $mainContainer.removeClass('fullscreen');
  // $header.show();
}

function queueAutoSlide() {
  autoSlideTimeout = setTimeout(function () {
    if (isFullscreen()) {
      nextFullscreenImage();
    }
  }, settings.get('autoSlideTime'));
}

function getCurrentFullscreenPage() {
  return lowestParsedFullscreenPage + Math.floor(currentFullscreenImage / parser.getImagesCountPerPage());
}

function downloadFullscreenImage() {
  $('.fullscreen-download-link').append('<div id="click"></div>').find('#click').trigger('click').remove();
}

function isFullscreen() {
  return $mainContainer.hasClass('fullscreen');
}

function setTitle(title) {
  document.title = title;
}

function setFullscreenUITitle() {
  var title = getFullscreenTitle();
  $('.title').html(title);
}

function filterTags(tags) {
  if (pageInfo.isUsingFilter()) {
    return _.every(pageInfo.data.filter, function (orTags) {
      return _.some(orTags, function (tag) {
        return _.includes(tags, tag);
      })
    });
  } else {
    return true;
  }
}

function checkOpenedTags() {
  _.forEach($('.keyword'), function (kw) {
    var $kw = $(kw);
    var kwText = settings.get('debug') ? 'placeholder' : $kw.attr('data-text');

    if (_.includes(tabNavigator.getOpenedTags(), kwText)) {
      $kw.addClass('opened-tab');
    } else {
      $kw.removeClass('opened-tab');
    }
  });
}

window.page = page;

module.exports = page;
