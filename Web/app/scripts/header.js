var page = require('./page');
var parser = require('./parser/parser');
var settings = require('./settings/settings');
var settingsMenu = require('./settings/settings-menu');
var tabNavigator = require('./tab-navigator');
var utils = require('./utils');

var header = {
  initialize: function () {
    this.bindEvents();
  },
  bindEvents: function () {
    $('.search-input').on('keyup', function (event) {
      if (event.keyCode === 13) {
        $('.search-button').click();
      }
    });

    $('.search-button').on('click', function () {
      var $input = $('.search-input');
      var rawInput = $input.val();
      var tags = rawInput.split(settings.get('andSplitter'));

      var filter =  _.reduce(tags.slice(1), function (result, e) {
        result.push(e.split(settings.get('orSplitter')));
        return result;
      }, []);
      tags = [tags[0]];

      if (settings.get('newTabOnSearch')) {
        var link = parser.createHash({
          pageNumber: 1,
          tags: tags,
          filter: filter
        });

        tabNavigator.createFullTab(link, '? images: ' + tags[0], null, tabNavigator.currentTab + 1);

        if (settings.get('goToNewTabOnSearch')) {
          tabNavigator.goToTab(tabNavigator.currentTab + 1);
        }
      } else {
        utils.setHash(parser.createHash({
          pageNumber: 1,
          tags: tags,
          filter: filter
        }));
      }

      $input.val('');
    });

    $('.home').on('click', function () {
      safeCloseControlsBar();
    });

    $('.open-tab-navigator').on('click', function () {
      safeCloseControlsBar();
    });

    $('.open-settings').on('click', function () {
      if (settingsMenu.isOpen()) {
        settingsMenu.close();
      } else {
        settingsMenu.open();
      }
      safeCloseControlsBar();
    });

    $('.go-images-fullscreen').on('click', function () {
      page.goFullscreen();
      tabNavigator.hide();
      closeControlsBar();
    });

    $('.go-browser-fullscreen').on('click', function () {
      utils.toggleFullScreen();
      safeCloseControlsBar();
    });

    $('.history-back').on('click', function () {
      history.back();
      safeCloseControlsBar();
    });

    $('.history-forward').on('click', function () {
      history.forward();
      safeCloseControlsBar();
    });

    $('.refresh').on('click', function () {
      location.reload();
      safeCloseControlsBar();
    });

    $('.sort-tabs').on('click', function () {
      tabNavigator.sort();
      safeCloseControlsBar();
    });

    $('.save-settings').on('click', function () {
      settings.save();
      safeCloseControlsBar();
    });

    $('.open-visited-images').on('click', function () {
      if (confirm('Do you want to open visited images?')) {
        openVisitedImages();
      }
      safeCloseControlsBar();
    });

    $('.switch-controls-bar').on('click', function () {
      var $bar = $('.controls-bar.mobile');
      if ($bar.hasClass('opened')) {
        $bar.removeClass('opened');
      } else {
        $bar.addClass('opened');
      }
    });

    function safeCloseControlsBar() {
      if (settings.get('closeControlsBarOnAction')) {
        closeControlsBar();
      }
    }

    function closeControlsBar() {
      $('.controls-bar.mobile').removeClass('opened');
    }

    function openVisitedImages() {
      var visitedImages = settings.get('visitedImages');
      var openedImages = tabNavigator.getOpenedTags();

      var diff = _.difference(visitedImages, openedImages);
      diff.sort();

      var tabs = [];
      for (var i = 0; i < diff.length; i++) {
        var tags = [diff[i]];

        var link = parser.createHash({
          pageNumber: 1,
          tags: tags,
          filter: []
        });

        tabs.push({
          link: link,
          title: '? images: ' + tags[0],
          image: null
        });
      }
      tabNavigator.createFullTabs(tabs, tabNavigator.currentTab + 1);
    }
  }
};

window.header = header;

module.exports = header;
