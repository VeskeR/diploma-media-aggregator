var pageInfo = require('./page-info');
var parser = require('./parser/parser');
var settings = require('./settings/settings');
var utils = require('./utils');

var $tabNavigator = $('.tab-navigator');
var $tabWrapper = $('.tab-wrapper');

var $openTabNavigator = $('.open-tab-navigator');
var $closeTabNavigator = $('.close-tab-navigator');

var tabNavigator = {
  tabsInfo: [],
  currentTab: 0,
  currentBatch: 0,
  isOpen: false,
  closeSize: 0,
  traversingTabs: false,
  traversingTabsTimeout: 0,
  traversionErrorThreshold: 15000,
  openedTags: [],
  $tabIndex: null,
  abbreviationMap: {
    code: {
      age: 'a',
      current: 'c',
      link: 'l',
      title: 't'
    },
    decode: {
      a: 'age',
      c: 'current',
      l: 'link',
      t: 'title'
    }
  },

  initialize: function () {
    var self = this;

    this.$tabIndex = $('<div class="tab-index"></div>');
    $tabWrapper.append(this.$tabIndex);

    if (settings.get('tabNavigator')) {
      this.parseTabsInfo();

      if (settings.get('useTabSaving') && this.tabsInfo.length > 0) {
        this.processTabsInfo();
      } else {
        var tabInfo = this.createTabInfo(location.hash, 'Home', null);
        this.addTabInfo(tabInfo, 0);
      }

      this.initControls();
      this.goToBatch(this.currentBatch);
      this.goToTab(this.currentTab);
      this.scrollToCurrentTab();

      this.bindEvents();
    } else {
      this.hide();
    }
  },
  parseTabsInfo: function () {
    var tabsInfo = settings.get('tabs');
    var tabs;
    if (typeof tabsInfo === 'string') {
      tabs = JSON.parse(tabsInfo);
    } else {
      tabs = tabsInfo;
    }
    this.tabsInfo = this.normalizeTabs(tabs);
    utils.log('Parsed ' + this.tabsInfo.length + ' tabs');
  },
  normalizeTabs: function (tabs) {
    var self = this;
    var normalizedTabs = [];
    _.forEach(tabs, function (tab) {
      normalizedTabs.push(utils.normalizeObj(tab, self.abbreviationMap));
    });
    return normalizedTabs;
  },
  minifyTabs: function (tabs) {
    var self = this;
    var minifiedTabs = [];
    _.forEach(tabs, function (tab) {
      minifiedTabs.push(utils.minifyObj(tab, self.abbreviationMap));
    });
    return minifiedTabs;
  },
  processTabsInfo: function () {
    var self = this;

    utils.log('Processing loaded tabs');

    if (settings.get('closeExpiredTabs')) {
      var expirationLength = settings.get('tabExpirationLength');
      this.tabsInfo = _.filter(this.tabsInfo, function (ti) {
        if (!ti.age) {
          ti.age = 1;
        } else {
          ti.age++;
        }
        return ti.current || ti.age < expirationLength;
      });
    }

    this.tabsInfo = _.uniqBy(this.tabsInfo, function (ti) {
      if (ti.current) {
        return -1;
      } else {
        return ti.link;
      }
    });

    this.tabsInfo = _.uniqBy(this.tabsInfo, function (ti) {
      if (ti.current) {
        return -1;
      } else {
        var res = /(^[0-9?]+ images:)(.*)/.exec(ti.title);
        if (res) {
          return 'check for multiple tags opened: ' + res[2].trim();
        } else {
          return ti.title;
        }
      }
    });

    this.tabsInfo = this.tabsInfo.filter(function (ti) {
      return ti.current || !/[а-яА-ЯЁёіІїЇ]/.test(ti.title);
    });

    this.addOpenedTags(this.tabsInfo);

    if (settings.get('closeTabsWithVisitedOrOpenedTags')) {
      var visitedImages = settings.get('visitedImages');
      var openedImages = this.openedTags;
      this.tabsInfo = _.filter(this.tabsInfo, function (ti) {
        var tags = self.getTagsForImagePage(ti);
        if (tags.length > 0) {
          return ti.current || _.difference(tags, visitedImages, openedImages).length > 0;
        } else {
          return true;
        }
      });

      this.openedTags = [];
      this.addOpenedTags(this.tabsInfo);
    }

    this.currentTab = _.findIndex(this.tabsInfo, function (ti) {
      return ti.current;
    });

    if (this.currentTab === -1) {
      this.currentTab = 0;
    }

    this.currentBatch = Math.floor(this.currentTab / settings.get('tabNavigatorBatchSize'));

    utils.log('Processed ' + this.tabsInfo.length + ' tabs');
  },
  initControls: function () {
    var $prevControl = this.createBatchControl('<', 'previous');
    var $nextControl = this.createBatchControl('>', 'next');
    $tabWrapper.append($prevControl);
    $tabWrapper.append($nextControl);
  },
  createBatchControl: function (text, className) {
    var $control = $('<div class="batch-control ' + className + '">' + text + '</div>');
    return $control;
  },
  goToBatch: function (i) {
    var batchCount = this.getBatchCount();
    if (i < 0 || i > batchCount - 1) {
      return;
    }

    utils.log('Displaying batch #' + (i + 1));

    var batchSize = settings.get('tabNavigatorBatchSize');
    var batch = this.getBatch(i * batchSize, (i + 1) * batchSize);
    var $target = $tabWrapper.find('.batch-control').eq(1);

    $tabWrapper.find('.tab').remove();

    _.forEach(batch, function (tab) {
      $target.before(tab);
    });

    this.currentBatch = i;

    this.checkScroll();
    this.checkVisibleTabs();
    this.checkBatchControls();
  },
  getBatch: function (start, end) {
    var batch = [];
    for (var i = start; i < end; i++) {
      if (i >= 0 && i < this.tabsInfo.length) {
        batch.push(this.getTab(i));
      }
    }
    return batch;
  },
  nextBatch: function () {
    this.goToBatch(this.currentBatch + 1);
    $tabWrapper[0].scrollTop = 0;
  },
  prevBatch: function () {
    this.goToBatch(this.currentBatch - 1);
    $tabWrapper[0].scrollTop = $tabWrapper[0].scrollHeight;
  },
  getBatchCount: function () {
    return Math.max(1, Math.ceil(this.tabsInfo.length / settings.get('tabNavigatorBatchSize')));
  },
  checkBatchControls: function () {
    var $prevControl = $tabWrapper.find('.batch-control.previous');
    var $nextControl = $tabWrapper.find('.batch-control.next');

    $prevControl.add($nextControl).removeClass('disabled');

    var batchCount = this.getBatchCount();
    if (this.currentBatch === 0) {
      $prevControl.addClass('disabled');
    }
    if (this.currentBatch === batchCount - 1) {
      $nextControl.addClass('disabled');
    }
  },
  checkBatchOverflow: function () {
    var batchSize = settings.get('tabNavigatorBatchSize');
    var $tabs = $tabWrapper.find('.tab');

    if ($tabs.length > batchSize) {
      for (var i = batchSize; i < $tabs.length; i++) {
        $tabs.eq(i).remove();
      }
      this.checkBatchControls();
    }
  },
  checkBatchLack: function () {
    var batchCount = this.getBatchCount();
    var batchSize = settings.get('tabNavigatorBatchSize');
    var $tabs = $tabWrapper.find('.tab');

    if (this.currentBatch >= batchCount - 1) {
      if ($tabs.length === 0) {
        this.prevBatch();
      } else if ($tabs.length < (this.tabsInfo.length - this.currentBatch * batchSize)) {
        $tabs.last().after(this.getTab((this.currentBatch + 1) * batchSize - 1));
        this.checkBatchControls();
      }
    } else if ($tabs.length < batchSize) {
      $tabs.last().after(this.getTab((this.currentBatch + 1) * batchSize - 1));
      this.checkBatchControls();
    }
  },
  createTabInfo: function (link, title, image) {
    var tabInfo = {
      link: link,
      title: title,
      age: 0
    };

    if (image) {
      tabInfo.image = {
        smallSrc: image.smallSrc,
        link: image.link
      }
    }

    return tabInfo;
  },
  getTab: function (i) {
    if (i >= 0 && i < this.tabsInfo.length) {
      if (!this.tabsInfo[i].$) {
        var $tab = this.createTab(this.tabsInfo[i]);
        this.setTab($tab, i);
      }
      return this.tabsInfo[i].$;
    } else {
      console.error('No tab with index #' + i);
      return null;
    }
  },
  createTab: function (tabInfo) {
    var link = tabInfo.link;
    var title = tabInfo.title;
    var image = tabInfo.image;

    var $tab = $('<div class="tab"></div>');
    var $tabClose = $('<div class="tab-close">X</div>');
    var $tabLink = $('<div class="tab-link"></div>');
    var $title = $('<div class="tab-title"></div>');
    var $imageWrapper = $('<div class="image-wrapper"></div>');
    var $image = $('<img class="tab-image">');

    var src = image && image.smallSrc ? image.smallSrc : '';
    var imageSrc = utils.getImgSrc(src, null, null, 300, 200);

    $tab.attr('data-uuid', utils.getUUID());

    $tabLink.attr('data-link', link);

    $title.html(title);

    $image.attr('data-src', imageSrc);
    // Makes tabs download images on creation and this takes too long
    // $image.attr('src', imageSrc);

    utils.setImgSizingBehaviour($image);

    $imageWrapper.append($image);
    $tabLink.append($title);
    $tabLink.append($imageWrapper);
    $tab.append($tabClose);
    $tab.append($tabLink);

    this.setTabCloseSize(settings.get('tabNavigatorTabsCloseSize'), $tab);

    return $tab;
  },
  createFullTab: function (link, title, image, i) {
    var tabInfo = this.createTabInfo(link, title, image);
    this.addTabInfo(tabInfo, i);
    this.addTabToView(i);
  },
  createFullTabs: function (tabs, i) {
    var self = this;
    var tabInfos = _.map(tabs, function (t) {
      return self.createTabInfo(t.link, t.title, t.image);
    });
    this.addTabInfos(tabInfos, i);
  },
  setTab: function ($tab, i) {
    this.tabsInfo[i].$ = $tab;
  },
  addTabInfo: function (tabInfo, i) {
    utils.log('Adding new tab to tab navigator at position: ' + i);

    if (this.tabsInfo.length > 0) {
      this.tabsInfo.splice(i, 0, tabInfo);
    } else {
      this.tabsInfo.push(tabInfo);
    }

    this.updateTabIndex(this.currentTab);
    this.updateOpenedTags(tabInfo.title, null, 'add');
    this.saveTabs();
  },
  addTabInfos: function (tabInfos, i) {
    var self = this;
    utils.log('Adding ' + tabInfos.length + ' new tabs to tab navigator at position: ' + i);

    if (this.tabsInfo.length > 0) {
      Array.prototype.splice.apply(this.tabsInfo, _.concat([i, 0], tabInfos));
    } else {
      Array.prototype.splice.push(this.tabsInfo, tabInfos);
    }

    this.updateTabIndex(this.currentTab);
    this.addOpenedTags(tabInfos);
    this.saveTabs();
  },
  addTabToView: function (i) {
    var $target = $tabWrapper.find('.tab.current');
    $target.after(this.getTab(i));

    this.checkBatchOverflow();
    this.checkScroll();
    this.checkVisibleTabs();
  },
  addTabsToView: function (i, length) {
    var $target = $tabWrapper.find('.tab.current');
    for (var j = i + length - 1; j >= i; j--) {
      $target.after(this.getTab(j));
    }

    this.checkBatchOverflow();
    this.checkScroll();
    this.checkVisibleTabs();
  },
  selectTab: function (i) {
    if (this.tabsInfo[this.currentTab]) {
      this.getTab(this.currentTab).removeClass('current');
      this.tabsInfo[this.currentTab].current = false;
    }
    this.currentTab = i;
    this.getTab(this.currentTab).addClass('current');
    this.tabsInfo[this.currentTab].current = true;
  },
  goToTab: function (i) {
    utils.log('Going to tab #' + (i + 1));

    if (i >= 0 && i < this.tabsInfo.length) {
      var $target = this.getTab(i);
      location.hash = $target.find('.tab-link').attr('data-link');
      this.selectTab(i);
      this.updateTabIndex(i);
    } else {
      utils.log('No tab with index #' + i);
    }
  },
  nextTab: function () {
    this.goToTab(this.currentTab + 1);
  },
  previousTab: function () {
    this.goToTab(this.currentTab - 1);
  },
  updateCurrentTab: function (link, title, image, pageType) {
    var self = this;
    this.updateTab(this.currentTab, link, title, image, pageType);
    page.checkOpenedTags();
    if (this.traversingTabs) {
      setTimeout(function () {
        self._traverse();
      }, settings.get('traversionDelay'));
    }
  },
  updateTab: function (i, link, title, image, pageType) {
    var $tab = this.getTab(i);

    var $tabTitle = $tab.find('.tab-title');
    var $tabLink = $tab.find('.tab-link');
    var $tabImage = $tab.find('.tab-image');

    $tabTitle.html(title);

    // if (link) {
    //   $tabLink.attr('data-link', link);
    // } else if (pageType === 'imagepage' && image && image.link) {
    //   $tabLink.attr('data-link', image.link);
    // } else {
    //   $tabLink.attr('data-link', location.hash);
    // }

    if (link) {
      $tabLink.attr('data-link', link);
    } else {
      $tabLink.attr('data-link', location.hash);
    }

    if (!settings.get('useGifsAlways')) {
      var src = image && image.smallSrc ? image.smallSrc : '';
      var imageSrc = utils.getImgSrc(src, null, null, 300, 200);
      $tabImage.attr('data-src', imageSrc);
      $tabImage.attr('src', imageSrc);
    }

    this.updateTabInfo(i, link, title, image, pageType);
  },
  updateCurrentTabInfo: function (link, title, image, pageType) {
    this.updateTabInfo(this.currentTab, link, title, image, pageType);
  },
  updateTabInfo: function (i, link, title, image, pageType) {
    var tabInfo = this.tabsInfo[i];

    var oldTitle = tabInfo.title;

    tabInfo.age = 0;
    tabInfo.title = title;

    if (link) {
      tabInfo.link = link;
    } else if (pageType === 'imagepage' && image && image.link) {
      tabInfo.link = image.link;
    } else {
      tabInfo.link = location.hash;
    }

    if (image) {
      tabInfo.image = {
        smallSrc: image.smallSrc,
        link: image.link
      };
    }

    this.updateOpenedTags(title, oldTitle, 'update');
    this.saveTabs();
  },
  closeTab: function (i) {
    if (this.tabsInfo.length > 1) {
      var length = this.tabsInfo.length;

      var $target = this.getTab(i);
      $target.remove();

      var removedTabInfo = this.tabsInfo.splice(i, 1)[0];

      if (i < this.currentTab) {
        this.currentTab--;
      } else if (i === this.currentTab) {
        if (i === length - 1) {
          this.goToTab(i - 1);
        } else {
          this.goToTab(i);
        }
      }

      this.checkBatchLack();
      this.updateTabIndex(this.currentTab);
      this.updateOpenedTags(null, removedTabInfo.title ,'remove');
      this.saveTabs();
      this.checkScroll();
      this.checkVisibleTabs();
    }
  },
  setPosition: function (value) {
    $tabNavigator.removeClass('top right bottom left');
    $tabNavigator.addClass(value);
    this.checkScroll();
    this.setCloseOffset();
  },
  setClosePosition: function (value) {
    $closeTabNavigator.removeClass('left-top right-bottom');
    $closeTabNavigator.addClass(value);

    this.setCloseOffset();
  },
  setCloseSize: function (value) {
    this.closeSize = value;

    $closeTabNavigator.css('width', value + 'rem');
    $closeTabNavigator.css('height', value + 'rem');
    $closeTabNavigator.css('line-height', value + 'rem');
    $closeTabNavigator.css('font-size', value * 0.75 + 'rem');

    this.setCloseOffset();
  },
  setCloseOffset: function () {
    var tabPosition = settings.get('tabNavigatorPosition');
    var closePosition = settings.get('tabNavigatorClosePosition');
    var offset = '-' + this.closeSize + 'rem';

    $closeTabNavigator.css('top', 'initial');
    $closeTabNavigator.css('right', 'initial');
    $closeTabNavigator.css('bottom', 'initial');
    $closeTabNavigator.css('left', 'initial');

    switch (tabPosition) {
      case 'top':
        $closeTabNavigator.css('bottom', offset);
        if (closePosition === 'left-top') {
          $closeTabNavigator.css('left', 0);
        } else {
          $closeTabNavigator.css('right', 0);
        }
        break;
      case 'right':
        $closeTabNavigator.css('left', offset);
        if (closePosition === 'left-top') {
          $closeTabNavigator.css('top', 0);
        } else {
          $closeTabNavigator.css('bottom', 0);
        }
        break;
      case 'bottom':
        $closeTabNavigator.css('top', offset);
        if (closePosition === 'left-top') {
          $closeTabNavigator.css('left', 0);
        } else {
          $closeTabNavigator.css('right', 0);
        }
        break;
      case 'left':
        $closeTabNavigator.css('right', offset);
        if (closePosition === 'left-top') {
          $closeTabNavigator.css('top', 0);
        } else {
          $closeTabNavigator.css('bottom', 0);
        }
        break;
    }
  },
  setTabsCloseSize: function (value) {
    var self = this;

    _.forEach(this.tabsInfo, function (tabInfo) {
      if (tabInfo.$) {
        self.setTabCloseSize(value, tabInfo.$);
      }
    });

    this.checkScroll();
  },
  setTabCloseSize: function (value, $tab) {
    var $tabClose = $tab.find('.tab-close');

    $tabClose.css('width', value + 'rem');
    $tabClose.css('height', value + 'rem');
    $tabClose.css('line-height', value + 'rem');
    $tabClose.css('font-size', value + 'rem');
    $tab.css('padding-top', value + 'rem');
  },
  setBackgroundColor: function (cssValue) {
    $tabNavigator.css('background-color', cssValue);
    $closeTabNavigator.css('background-color', cssValue);
  },
  getTabIndex: function ($tab) {
    // var uuid = $tab.attr('data-uuid');
    // return _.findIndex(this.$tabs, function (tab) {
    //   return tab.attr('data-uuid') === uuid;
    // });
    return _.findIndex(this.tabsInfo, function (tabInfo) {
      return tabInfo.$ && tabInfo.$.attr('data-uuid') === $tab.attr('data-uuid');
    });
  },
  checkScroll: function () {
    var tabWrapper = $tabWrapper[0];
    var scrollWidth = Math.max(tabWrapper.offsetWidth - tabWrapper.clientWidth, tabWrapper.offsetHeight - tabWrapper.clientHeight);
    if (scrollWidth !== 0) {
      this.hideScrolls(scrollWidth);
    } else {
      this.fixPositiong();
    }
  },
  hideScrolls: function (scrollWidth) {
    $tabWrapper.css('right', -scrollWidth + 'px');
    $tabWrapper.css('bottom', -scrollWidth + 'px');
    if (/top|bottom/.test(settings.get('tabNavigatorPosition'))) {
      $tabWrapper.css('margin-right', scrollWidth + 'px');
      $tabWrapper.css('padding-bottom', 0);
    } else {
      $tabWrapper.css('margin-right', 0);
      $tabWrapper.css('padding-bottom', scrollWidth + 'px');
    }
  },
  fixPositiong: function () {
    $tabWrapper.css('right', 0);
    $tabWrapper.css('bottom', 0);
    $tabWrapper.css('margin-right', 0);
    $tabWrapper.css('padding-bottom', 0);
  },
  bindEvents: function () {
    var self = this;

    this.bindTabCreationEvent();
    this.bindSwipeEvent();

    $tabNavigator.on('click', function (e) {
      var $tab;
      var $tabClose = $(e.target).closest('.tab-close');
      if ($tabClose.length > 0) {
        $tab = $(e.target).closest('.tab');
        self.closeTab(self.getTabIndex($tab));
        return;
      }

      $tab = $(e.target).closest('.tab');
      if ($tab.length > 0) {
        self.goToTab(self.getTabIndex($tab));
      }
    });

    $openTabNavigator.on('click', function () {
      if (self.isOpen) {
        self.hide();
      } else {
        self.show();
      }
    });

    $closeTabNavigator.on('click', function () {
      self.hide();
    });

    $tabWrapper.on('scroll', function () {
      self.checkVisibleTabs();
    });

    $tabWrapper.find('.batch-control').on('click', function () {
      var $e = $(this);
      if ($e.hasClass('previous')) {
        self.prevBatch();
      } else if ($e.hasClass('next')) {
        self.nextBatch();
      }
    });
  },
  bindTabCreationEvent: function () {
    var self = this;

    (function () {
      var lastPress = null;
      var tabCreationTimeout = null;
      var processingTouch = false;

      document.addEventListener('touchstart', function (e) {
        processingTouch = true;
        startTabCreation(e);
      }, {passive: false});

      $(document).on('mousedown', function (e) {
        if (!processingTouch) {
          startTabCreation(e);
        }
      });

      function startTabCreation(e) {
        var link = $(e.target).closest('a[href^="#"]');

        if (link.length > 0) {
          lastPress = e;
          tabCreationTimeout = setTimeout(function () {
            if (lastPress) {
              var linkInfo = self.getLinkInfo(link);
              utils.log('Creating new navigator tab with next information:');
              utils.log(linkInfo);
              self.createFullTab(link.attr('href'), linkInfo.title, linkInfo.image, self.currentTab + 1);
            }
          }, settings.get('linkOpenDelay'));
        }
      }

      $(document).on('touchmove', function (e) {
        if (lastPress && lastPress.touches && lastPress.touches[0]) {
          var lt = lastPress.touches[0];
          var ct = e.touches[0];
          var delta = Math.abs(lt.clientX - ct.clientX) + Math.abs(lt.clientY - ct.clientY);
          if (Math.abs(delta) > settings.get('cancelTabCreationThreshold')) {
            clearTimeout(tabCreationTimeout);
            lastPress = null;
          }
        }
      });

      document.addEventListener('click', function (e) {
        if (lastPress && (e.timeStamp - lastPress.timeStamp > settings.get('linkOpenDelay'))) {
          e.preventDefault();
        }
        clearTimeout(tabCreationTimeout);
        lastPress = null;
      }, {passive: false});

      $(document).on('touchend', function (e) {
        processingTouch = false;
      });
    })();
  },
  bindSwipeEvent: function () {
    var self = this;
    var lastTouch = null;

    $(document).on('touchstart', function (e) {
      lastTouch = e.touches[0];
    });

    $(document).on('touchmove', function (e) {
      var position = settings.get('tabNavigatorPosition');

      if (lastTouch && settings.get('tabNavigatorSwipeOpen') && !$('.main-container').hasClass('fullscreen')) {
        var currentTouch = e.touches[0];

        var deltaX = currentTouch.clientX - lastTouch.clientX;
        var deltaY = currentTouch.clientY - lastTouch.clientY;

        var threshold = settings.get('tabNavigatorSwipeThreshold');

        switch (position) {
          case 'left':
          case 'right':
            if (Math.abs(deltaX) > threshold) {
              if (deltaX > 0) {
                if (position === 'left') {
                  self.show();
                } else {
                  self.hide();
                }
              } else {
                if (position === 'left') {
                  self.hide();
                } else {
                  self.show();
                }
              }

              lastTouch = null;
            }
            break;
          case 'top':
          case 'bottom':
            if (Math.abs(deltaY) > threshold) {
              if (deltaY > 0) {
                if (position === 'top') {
                  self.show();
                } else {
                  self.hide();
                }
              } else {
                if (position === 'bottom') {
                  self.hide();
                } else {
                  self.show();
                }
              }

              lastTouch = null;
            }
            break;
        }
      }
    });
  },
  getLinkInfo: function (link) {
    var linkInfo = {
      title: '',
      image: null
    };
    var commonKeywords = settings.get('commonKeywords');
    var keywords = null;

    if (link.siblings('.fullscreen-image').length > 0) {
      // Link to image page
      keywords = link.siblings('.fullscreen-image').attr('data-keywords').split(',');
      if (!(settings.get('showCommonKeywords') && settings.get('showCommonKeywordsInTabs'))) {
        keywords = _.difference(keywords, commonKeywords);
      }
      if (settings.get('debug')) {
        keywords = new Array(keywords.length).fill('placeholder');
      }
      linkInfo.title = 'Image page: ' + keywords.join(', ');
      linkInfo.image = {
        smallSrc: link.siblings('.fullscreen-image').attr('data-smallsrc')
      };

      utils.log('Trying to open image page from Fullscreen mod');
    } else if (link.closest('.thumb').length > 0) {
      // Link to image page
      keywords = link.closest('.thumb').attr('data-keywords').split(',');
      if (!(settings.get('showCommonKeywords') && settings.get('showCommonKeywordsInTabs'))) {
        keywords = _.difference(keywords, commonKeywords);
      }
      if (settings.get('debug')) {
        keywords = new Array(keywords.length).fill('placeholder');
      }
      linkInfo.title = 'Image page: ' + keywords.join(', ');
      linkInfo.image = {
        smallSrc: link.closest('.thumb').attr('data-smallsrc')
      };

      utils.log('Trying to open image page from thumb');
    } else if (link.closest('.keywords').length > 0) {
      // Link to page with keywords
      var keyword = settings.get('debug') ? 'placeholder' : link.attr('data-text');
      linkInfo.title = '? images: ' + keyword;
      linkInfo.image = {
        smallSrc: link.closest('.keywords').siblings('.image').attr('data-smallsrc')
      };

      utils.log('Trying to open page via keywords link');
    } else if (link.closest('.navigation').length > 0) {
      // Link to page with keywords
      keywords = settings.get('debug') ? 'placeholder' : pageInfo.getStringTags();
      linkInfo.title = '? images: ' + keywords;

      utils.log('Trying to open page via navigation');
    } else if (link.closest('.home').length > 0) {
      // Link to home page
      linkInfo.title = '? images';

      utils.log('Trying to open home page');
    }

    return linkInfo;
  },
  traverseTabs: function () {
    if (!this.traversingTabs) {
      utils.log('Started automated tabs traversion');
      this.traversingTabs = true;
      this._traverse();
    }
  },
  _traverse: function () {
    var self = this;

    clearTimeout(this.traversingTabsTimeout);

    if (self.currentTab === self.tabsInfo.length - 1 || !this.traversingTabs) {
      self.stopTraversingTabs();
      return;
    }

    self.nextTab();
    var batchIndex = Math.floor(self.currentTab / settings.get('tabNavigatorBatchSize'));
    if (batchIndex !== self.currentBatch) {
      self.goToBatch(batchIndex);
    }
    self.scrollToCurrentTab();

    this.traversingTabsTimeout = setTimeout(function () {
      self._traverse();
    }, this.traversionErrorThreshold);
  },
  stopTraversingTabs: function () {
    if (this.traversingTabs) {
      console.log('Stopped tabs traversion');
      clearTimeout(this.traversingTabsTimeout);
      this.traversingTabs = false;
    }
  },
  show: function () {
    if (!this.isOpen) {
      $tabNavigator.removeClass('hidden');
      $tabNavigator.addClass('visible');
      this.isOpen = true;
      this.checkVisibleTabs();
    }
  },
  hide: function () {
    if (this.isOpen) {
      $tabNavigator.removeClass('visible');
      $tabNavigator.addClass('hidden');
      this.isOpen = false;
    }
  },
  saveTabs: function () {
    var tabs = this.minifyTabs(this.tabsInfo);
    settings.setTabs(JSON.stringify(tabs, function (key, value) {
      if (key === '$' || key === 'image') {
        return undefined;
      } else {
        return value;
      }
    }));
  },
  checkVisibleTabs: function () {
    this.holdedLoadTabImage();
  },
  scrollToCurrentTab: function () {
    $tabWrapper[0].scrollTop += this.getTab(this.currentTab).offset().top;
  },
  sort: function () {
    var tagTabs = this.tabsInfo.filter(function (ti) {
      return /^[0-9]+ images/.test(ti.title);
    });
    var imageTabs = _.difference(this.tabsInfo, tagTabs);
    tagTabs = _.sortBy(tagTabs, function (ti) {
      return -parseInt(ti.title);
    });
    imageTabs = _.sortBy(imageTabs, function (ti) {
      return ti.title;
    });
    this.tabsInfo = _.concat(tagTabs, imageTabs);
    tabNavigator.saveTabs();
  },
  updateOpenedTags: function (newTitle, oldTitle, action) {
    var self = this;
    var addTags = !!newTitle ? this.getTagsForTagPage(newTitle) : [];
    var removeTags = !!oldTitle ? this.getTagsForTagPage(oldTitle) : [];

    var removeIndexes = [];
    _.forEach(removeTags, function (t) {
      var i = _.indexOf(self.openedTags, t);
      if (i !== -1) {
        removeIndexes = _.union(removeIndexes, [i]);
      }
    });

    switch (action) {
      case 'add':
        if (addTags.length > 0) {
          utils.log('Adding opened tags: ' + JSON.stringify(addTags));
          this.openedTags = _.concat(this.openedTags, addTags);
        }
        break;
      case 'update':
        if (addTags.length > 0 || removeTags.length > 0) {
          utils.log('Updating opened tags: ' + JSON.stringify(addTags) + ', ' + JSON.stringify(removeTags));
          _.pullAt(this.openedTags, removeIndexes);
          this.openedTags = _.concat(this.openedTags, addTags);
        }
        break;
      case 'remove':
        if (removeTags.length > 0) {
          utils.log('Removing opened tags: ' + JSON.stringify(removeTags));
          _.pullAt(this.openedTags, removeIndexes);
        }
        break;
    }
  },
  addOpenedTags: function (tabInfos) {
    var openedTags = this.parseOpenedTags(tabInfos);
    console.log('Adding '+ openedTags.length + ' opened tags: ' + JSON.stringify(openedTags));
    this.openedTags = _.union(this.openedTags, openedTags);
  },
  parseOpenedTags: function (tabInfos) {
    var self = this;
    var tags;
    if (tabInfos.length > 0) {
      tags = _.map(tabInfos, function (ti) {
        return self.getTagsForTagPage(ti.title);
      });
    }
    return _.flatten(tags);
  },
  getTagsForTagPage: function (title) {
    var tags = [];

    if (/(^\d+ images:)|(^\d+ placeholder:)|(^\? images:)/gi.test(title)) {
      tags = title.replace(/(^\d+ images:)|(^\d+ placeholder:)|(^\? images:)/gi, '').split(',')
      .map(function (kw) { return kw.trim(); })
      .filter(function (kw) { return kw.length > 0; });
    }

    return tags;
  },
  getTagsForImagePage: function (image) {
    var tags = [];
    var title = image.title;

    if (parser.parseHash(image.link).pageType === 'imagepage') {
      if (title.startsWith('Image page: ')) {
        title = title.substr('Image page: '.length);
      }
      tags = title.split(',').map(function (t) { return t.trim(); });
    }

    return tags;
  },
  getOpenedTags: function () {
    return _.uniq(this.openedTags);
  },
  updateTabIndex: function (i) {
    this.$tabIndex.text((i + 1) + '/' + this.tabsInfo.length);
  },
  holdedLoadTabImage: utils.hold(function () {
    var batchSize = settings.get('tabNavigatorBatchSize');
    for (var i = this.currentBatch * batchSize; i < (this.currentBatch + 1) * batchSize; i++) {
      if (i >= 0 && i < this.tabsInfo.length) {
        var $tab = this.tabsInfo[i].$;
        if ($tab && utils.isInViewport($tab[0])) {
          var $tabImage = $tab.find('.tab-image');
          $tabImage.attr('src', $tabImage.attr('data-src'));
        }
      }
    }
  }, settings.get('checkVisibleTabsDelay'))
};

window.tabNavigator = tabNavigator;

module.exports = tabNavigator;
