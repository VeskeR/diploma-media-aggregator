var settingsInputs = require('./settings-inputs');
var utils = require('../core-utils');

var crossHostKeys = [
  'backgroundColorMin',
  'backgroundColorMax',
  'defaultWidth',
  'defaultHeight',
  'tumblrBlogIds'
];

var hostSpecificKeys = ['commonKeywords', 'seenImages', 'tabs', 'visitedImages'];
var dangerKeys = [];

var defaultSettings = $.extend(_.reduce(settingsInputs, function (result, input) {
  if (!_.isUndefined(input.defaultValue)) {
    result[input.name] = input.defaultValue;
  }
  if (input.crossHost) {
    crossHostKeys.push(input.name);

    if (input.danger) {
      dangerKeys.push(input.name);
    }
  }
  if (input.hostSpecific) {
    hostSpecificKeys.push(input.name);
  }
  return result;
}, {}), {
  backgroundColorMin: 0,
  backgroundColorMax: 100,
  defaultWidth: 180,
  defaultHeight: 113,

  commonKeywords: [],
  seenImages: [],
  tabs: [],
  visitedImages: []
});

var cache = {};

var settings = $.extend({}, defaultSettings);

function get(name) {
  if (!settings[name]) {
    var lsSettings;
    if (_.includes(crossHostKeys, name)) {
      lsSettings = getItemWithDecompression('settings');
    } else {
      lsSettings = getItemWithDecompression(getHost());
    }
    if (lsSettings) {
      settings[name] = JSON.parse(lsSettings)[name];
    } else {
      settings[name] = defaultSettings[name];
    }
  }

  return settings[name];
}

function set(name, value) {
  if (_.includes(dangerKeys, name)) {
    saveDangerKey(name, value);
  } else {
    settings[name] = value;
    var naturalName = utils.camelCaseToNatural(name);
    if (name !== 'tabs') {
      log(naturalName + ' has been set to: ' + value);
    }
    initSave();
  }
}

function add(name, value) {
  if (!settings[name] || !Array.isArray(settings[name])) {
    settings[name] = [];
  }
  if (!Array.isArray(value)) {
    value = [value];
  }
  cache[name + 'AddCache'] = _.concat(cache[name + 'AddCache'], value);
  // settings[name] = _.union(settings[name], value);

  var naturalName = utils.camelCaseToNatural(name).toLowerCase();

  log("'" + value + "' has been added to " + naturalName);
  initSave();
}

function remove(name, value) {
  if (!settings[name] || !Array.isArray(settings[name])) {
    settings[name] = [];
  }
  if (!Array.isArray(value)) {
    value = [value];
  }
  cache[name + 'RemoveCache'] = _.concat(cache[name + 'RemoveCache'], value);
  // _.pullAll(settings[name], value);

  var naturalName = utils.camelCaseToNatural(name).toLowerCase();

  log("'" + value + "' has been removed from " + naturalName);
  initSave();
}

function getAll() {
  return settings;
}

function getCrossHostSettings() {
  var crossHostSettings = _.reduce(settings, function (r, v, k) {
    if (_.includes(crossHostKeys, k)) {
      r[k] = v;
    }
    return r;
  }, {});

  return crossHostSettings;
}

function getInvertedCrossHostSettings() {
  var crossHostSettings = _.reduce(settings, function (r, v, k) {
    if (!_.includes(crossHostKeys, k)) {
      r[k] = v;
    }
    return r;
  }, {});

  return crossHostSettings;
}

function getGeneralSettings() {
  var generalSettings = _.reduce(settings, function (r, v, k) {
    if (!_.includes(hostSpecificKeys, k)) {
      r[k] = v;
    }
    return r;
  }, {});

  return generalSettings;
}

function getHostSpecificSettings() {
  var hostSpecificSettings = _.reduce(settings, function (r, v, k) {
    if (_.includes(hostSpecificKeys, k)) {
      r[k] = v;
    }
    return r;
  }, {});

  return hostSpecificSettings;
}

function loadAll(obj) {
  settings = {};
  for (var prop in defaultSettings) {
    settings[prop] = defaultSettings[prop];
  }

  var crossHostSettings = getItemWithDecompression('settings');
  if (crossHostSettings) {
    $.extend(settings, JSON.parse(crossHostSettings));

    var hostSpecificSettings = getItemWithDecompression(getHost());
    if (hostSpecificSettings) {
      $.extend(settings, JSON.parse(hostSpecificSettings));
    }
  }

  $.extend(settings, obj);
  log('Settings has been loaded from local storage');
  initSave();
  return settings;
}

function restoreCrossHostSettings() {
  settings = $.extend({}, defaultSettings, getInvertedCrossHostSettings());
  initSave();
  log('Default cross host settings has been restored');
}

function restoreGeneralSettings() {
  settings = $.extend({}, defaultSettings, getHostSpecificSettings());
  initSave();
  log('Default general settings has been restored');
}

function restoreHostSpecificSettings() {
  settings = $.extend({}, defaultSettings, getGeneralSettings());
  initSave();
  log('Default host specific settings has been restored');
}

function applySettingsToCrossHost() {
  var crossHostSettings = getCrossHostSettings();
  var lsSettings = getItemWithDecompression('settings');
  if (lsSettings) {
    crossHostSettings = $.extend({}, JSON.parse(lsSettings), crossHostSettings);
  }

  crossHostSettings = $.extend({}, getGeneralSettings(), crossHostSettings);
  setItemWithCompression('settings', JSON.stringify(crossHostSettings))
}

function clearAll() {
  localStorage.clear();
  settings = $.extend({}, defaultSettings);
  initSave();
  log('Default settings has been restored');
}

function clearHost() {
  localStorage.removeItem(getHost());
  settings = $.extend({}, defaultSettings, JSON.parse(getItemWithDecompression('settings') || '{}'));
  initSave();
  log('Host specific settings has been cleared');
}

var holdedSave = utils.hold(function () {
  save();
}, settings.settingsSaveDelay);

var saveTimeout;

function initSave() {
  if (settings.useSettingsSaveDelay) {
    saveTimeout = holdedSave();
  } else {
    save();
  }
}

function save() {
  clearTimeout(saveTimeout);

  for (var prop in settings) {
    if (settings.hasOwnProperty(prop) && settings[prop] && Array.isArray(settings[prop]) &&
        cache[prop + 'AddCache'] && cache[prop + 'RemoveCache']) {
      settings[prop] = _.union(settings[prop], cache[prop + 'AddCache']);
      _.pullAll(settings[prop], cache[prop + 'RemoveCache']);
      cache[prop + 'AddCache'] = [];
      cache[prop + 'RemoveCache'] = [];
    }
  }

  var crossHostSettings = getCrossHostSettings();
  var lsSettings = getItemWithDecompression('settings');
  if (lsSettings) {
    crossHostSettings = $.extend({}, JSON.parse(lsSettings), crossHostSettings);
  }
  var otherSettings = getInvertedCrossHostSettings();

  setItemWithCompression('settings', JSON.stringify(crossHostSettings));
  setItemWithCompression(getHost(), JSON.stringify(otherSettings));
  log('Settings has been saved to local storage');
}

function getHost() {
  var host = settings.host;
  return host;
}

function saveDangerKey(name, value) {
  settings[name] = value;

  var crossHostSettings = getCrossHostSettings();
  var lsSettings = getItemWithDecompression('settings');
  if (lsSettings) {
    crossHostSettings = $.extend({}, JSON.parse(lsSettings), crossHostSettings);
  }

  setItemWithCompression('settings', JSON.stringify(crossHostSettings));
  log('Danger key ' + name + ' has been set to: ' + value);
  log('General settings has been saved');
}

function setItemWithCompression(key, string) {
  if (isCompressed()) {
    localStorage.setItem(key, LZString.compress(string));
  } else {
    localStorage.setItem(key, string);
  }
}

function getItemWithDecompression(key) {
  if (isCompressed()) {
    return LZString.decompress(localStorage.getItem(key));
  } else {
    return localStorage.getItem(key);
  }
}

function compressLocalStorage() {
  if (isCompressed()) {
    return;
  }
  for (var i = 0; i < localStorage.length; i++) {
    var key = localStorage.key(i);
    if (key !== 'dataIsCompressed') {
      var str = localStorage.getItem(key);
      localStorage.setItem(key, LZString.compress(str));
    }
  }
  localStorage.setItem('dataIsCompressed', 'true');
}

function decompressLocalStorage() {
  if (!isCompressed()) {
    return;
  }
  for (var i = 0; i < localStorage.length; i++) {
    var key = localStorage.key(i);
    if (key !== 'dataIsCompressed') {
      var str = LZString.decompress(localStorage.getItem(key));
      localStorage.setItem(key, str);
    }
  }
  localStorage.setItem('dataIsCompressed', 'false');
}

function isCompressed() {
  return localStorage.getItem('dataIsCompressed') === 'true';
}

function getCache() {
  return cache;
}

function log() {
  if (settings.debug || settings.forceLogging) {
    console.log.apply(console, arguments);
  }
}

var expObj = {
  default: defaultSettings,
  set: set,
  get: get,
  add: add,
  remove: remove,
  loadAll: loadAll,
  getAll: getAll,
  restoreCrossHostSettings: restoreCrossHostSettings,
  restoreGeneralSettings: restoreGeneralSettings,
  restoreHostSpecificSettings: restoreHostSpecificSettings,
  clearAll: clearAll,
  getCache: getCache,
  hostSpecificKeys: hostSpecificKeys,
  getCrossHostSettings: getCrossHostSettings,
  getInvertedCrossHostSettings: getInvertedCrossHostSettings,
  getGeneralSettings: getGeneralSettings,
  getHostSpecificSettings: getHostSpecificSettings,
  applySettingsToCrossHost: applySettingsToCrossHost,
  getItemWithDecompression: getItemWithDecompression,
  setItemWithCompression: setItemWithCompression,
  save: save,
  clearHost: clearHost,
  compressLocalStorage: compressLocalStorage,
  decompressLocalStorage: decompressLocalStorage
};

for (var prop in defaultSettings) {
  if (defaultSettings.hasOwnProperty(prop)) {
    (function() {
      var propSaved = prop;
      var capProp = propSaved.charAt(0).toUpperCase() + propSaved.substr(1);
      var getFuncName = 'get' + capProp;
      var setFuncName = 'set' + capProp;
      expObj[getFuncName] = function () {
        return get(propSaved);
      };
      expObj[setFuncName] = function (value) {
        set(propSaved, value);
      };

      if (Array.isArray(defaultSettings[prop])) {
        var addFuncName = 'add' + capProp;
        var removeFuncName = 'remove' + capProp;

        expObj[addFuncName] = function (value) {
          return add(propSaved, value);
        };
        expObj[removeFuncName] = function (value) {
          return remove(propSaved, value);
        };

        cache[prop + 'AddCache'] = [];
        cache[prop + 'RemoveCache'] = [];
      }
    })();
  }
}

window.settings = expObj;

module.exports = expObj;
