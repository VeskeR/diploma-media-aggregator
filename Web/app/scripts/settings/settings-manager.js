var settings = require('./settings');
var tabNavigator = require('../tab-navigator');
var utils = require('../utils');

var settingsManager = {
  applyAll: function (settings) {
    for (var func in this) {
      if (func !== 'applyAll' && this.hasOwnProperty(func)) {
        var propName = func.substr('apply'.length).charAt(0).toLowerCase() + func.substr('apply'.length + 1);
        this[func](settings[propName]);
      }
    }
  },
  applyBackgroundColor: function (colorValue) {
    colorValue = parseInt(colorValue);
    var bcrMin = settings.default.backgroundColorMin;
    var bcrMax = settings.default.backgroundColorMax;
    colorValue = colorValue < bcrMin ? bcrMin : colorValue > bcrMax ? bcrMax : colorValue;
    var rgbValue = Math.floor(colorValue / bcrMax * 255);
    var cssValue = 'rgb(' + rgbValue + ', ' + rgbValue + ', ' + rgbValue + ')';
    $('html').css('background-color', cssValue);
    $('.header .controls-bar.mobile').css('background-color', cssValue);
    tabNavigator.setBackgroundColor(cssValue);
  },
  applyThumbsSize: function (sizeValue) {
    if ($('.thumb').css('width')) {
      sizeValue = parseInt(sizeValue);
      var defaultWidth = settings.default.defaultWidth;
      var defaultHeight = settings.default.defaultHeight;

      var newWidth = sizeValue / 100 * defaultWidth;
      var newHeight = sizeValue / 100 * defaultHeight;

      $('.thumb').css('width', newWidth + 'px');
      $('.thumb').css('height', newHeight + 'px');
    }
  },
  applyShowTopNavigation: function (show) {
    $topNavigation = $('.navigation-top');
    if (show) {
      $topNavigation.show();
    } else {
      $topNavigation.hide();
    }
  },
  applyShowBottomNavigation: function (show) {
    $bottomNavigation = $('.navigation-bottom');
    if (show) {
      $bottomNavigation.show();
    } else {
      $bottomNavigation.hide();
    }
  },
  applyShowCommonKeywords: function (show) {
    $regularKeywords = $('.regular-keywords');
    $commonKeywords = $('.common-keywords');

    if ($regularKeywords.length > 0 && $commonKeywords.length > 0) {
      if (show) {
        $regularKeywords.addClass('border');
        $commonKeywords.show();
      } else {
        $regularKeywords.removeClass('border');
        $commonKeywords.hide();
      }
    }
  },
  applyTabNavigatorPosition: function (value) {
    tabNavigator.setPosition(value);
  },
  applyTabNavigatorClosePosition: function (value) {
    tabNavigator.setClosePosition(value);
  },
  applyTabNavigatorCloseSize: function (value) {
    tabNavigator.setCloseSize(value);
  },
  applyTabNavigatorTabsCloseSize: function (value) {
    tabNavigator.setTabsCloseSize(value);
  },
  applyHdResolution: function (value) {
    utils.setHdResolution(value);
  },
  applyOpenNewKeywordsBtn: function (show) {
    if (show && $('.open-new-keywords-btn').length > 0) {
      $('.open-new-keywords-btn').show();
    } else {
      $('.open-new-keywords-btn').hide();
    }
  }
};

module.exports = settingsManager;
