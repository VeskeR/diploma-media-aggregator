module.exports = [
  {
    name: 'backgroundColor',
    label: 'Background color:',
    type: 'text',
    immediateEffect: true,
    defaultValue: 100
  },
  {
    name: 'thumbsSize',
    label: 'Thumbs size:',
    type: 'text',
    immediateEffect: true,
    defaultValue: 100
  },
  {
    name: 'myRedirectHost',
    label: 'My redirect host:',
    type: 'text',
    defaultValue: '192.168.1.103:8080',
    crossHost: true
  },
  {
    name: 'autoSlideTime',
    label: 'Auto slide time:',
    type: 'text',
    defaultValue: 3000
  },
  {
    name: 'linkOpenDelay',
    label: 'Link open delay:',
    type: 'text',
    defaultValue: 700
  },
  {
    name: 'tabNavigatorCloseSize',
    label: 'Tab navigator close size:',
    type: 'text',
    defaultValue: 4,
    immediateEffect: true
  },
  {
    name: 'tabNavigatorTabsCloseSize',
    label: 'Tab navigator tabs close size:',
    type: 'text',
    defaultValue: 3,
    immediateEffect: true
  },
  {
    name: 'tabNavigatorSwipeThreshold',
    label: 'Tab navigator swipe threshold:',
    type: 'text',
    defaultValue: 100
  },
  {
    name: 'cancelTabCreationThreshold',
    label: 'Cancel tab creation threshold:',
    type: 'text',
    defaultValue: 15
  },
  {
    name: 'fullscreenTransitionSpeed',
    label: 'Fullscreen transition speed:',
    type: 'text',
    defaultValue: 0.7
  },
  {
    name: 'fullscreenTransitioningThreshold',
    label: 'Fullscreen transitioning threshold:',
    type: 'text',
    defaultValue: 1000
  },
  {
    name: 'tabNavigatorBatchSize',
    label: 'Tab navigator batch size:',
    type: 'text',
    defaultValue: 25
  },
  {
    name: 'checkVisibleTabsDelay',
    label: 'Check visible tabs delay:',
    type: 'text',
    defaultValue: 4000
  },
  {
    name: 'tabExpirationLength',
    label: 'Tab expiration length:',
    type: 'text',
    defaultValue: 3
  },
  {
    name: 'andSplitter',
    label: '"And" splitter:',
    type: 'text',
    defaultValue: ',',
    crossHost: true
  },
  {
    name: 'orSplitter',
    label: '"Or" splitter:',
    type: 'text',
    defaultValue: '/',
    crossHost: true
  },
  {
    name: 'paginationCount',
    label: 'Pagination count:',
    type: 'text',
    defaultValue: 10
  },
  {
    name: 'multiPageFetchCount',
    label: 'Multi page fetch count:',
    type: 'text',
    defaultValue: 5
  },
  {
    name: 'settingsSaveDelay',
    label: 'Settings save delay:',
    type: 'text',
    defaultValue: 4000,
    crossHost: true
  },
  {
    name: 'tumblrImagesPerPage',
    label: 'Tubmlr images per page:',
    type: 'text',
    defaultValue: 15
  },
  {
    name: 'traversionDelay',
    label: 'Traversion delay:',
    type: 'text',
    defaultValue: 250
  },
  {
    name: 'addTumblrBlogId',
    label: 'Add tumblr blog id:',
    type: 'confirmationInput',
    bindedTo: 'tumblrBlogIds',
    lowercase: true,
    immediateEffect: true
  },
  {
    name: 'redirectService',
    label: 'Redirect service:',
    type: 'select',
    defaultValue: 'herokuEU',
    crossHost: true,
    values: [
      {
        name: 'whateverorigin',
        label: 'Whateverorigin',
        value: 'whateverorigin'
      },
      {
        name: 'local',
        label: 'Local',
        value: 'local'
      },
      {
        name: 'herokuUS',
        label: 'Heroku US',
        value: 'herokuUS'
      },
      {
        name: 'herokuEU',
        label: 'Heroku EU',
        value: 'herokuEU'
      }
    ]
  },
  {
    name: 'fullscreenMode',
    label: 'Fullscreen mode:',
    type: 'select',
    defaultValue: 'standart',
    values: [
      {
        name: 'standart',
        label: 'Standart',
        value: 'standart'
      },
      {
        name: 'yesNo',
        label: 'Yes / No',
        value: 'yesNo'
      },
      {
        name: 'advanced',
        label: 'Advanced',
        value: 'advanced'
      }
    ]
  },
  {
    name: 'tabNavigatorPosition',
    label: 'Tab navigator position:',
    type: 'select',
    immediateEffect: true,
    defaultValue: 'left',
    values: [
      {
        name: 'top',
        label: 'Top',
        value: 'top'
      },
      {
        name: 'right',
        label: 'Right',
        value: 'right'
      },
      {
        name: 'bottom',
        label: 'Bottom',
        value: 'bottom'
      },
      {
        name: 'left',
        label: 'Left',
        value: 'left'
      }
    ]
  },
  {
    name: 'tabNavigatorClosePosition',
    label: 'Tab navigator X position:',
    type: 'select',
    immediateEffect: true,
    defaultValue: 'left-top',
    values: [
      {
        name: 'leftTop',
        label: 'Left / Top',
        value: 'left-top'
      },
      {
        name: 'rightBottom',
        label: 'Right / Bottom',
        value: 'right-bottom'
      }
    ]
  },
  {
    name: 'keywordsSortBy',
    label: 'Sort keywords by:',
    type: 'select',
    defaultValue: 'alphabet',
    values: [
      {
        name: 'origin',
        label: 'Origin',
        value: 'origin'
      },
      {
        name: 'alphabet',
        label: 'Alphabet',
        value: 'alphabet'
      }
    ]
  },
  {
    name: 'hdResolution',
    label: 'HD resolution:',
    type: 'select',
    immediateEffect: true,
    defaultValue: '1920x1080',
    values: [
      {
        name: '1920x1080',
        label: '1920x1080',
        value: '1920x1080'
      },
      {
        name: '1680x1050',
        label: '1680x1050',
        value: '1680x1050'
      },
      {
        name: '1600x1200',
        label: '1600x1200',
        value: '1600x1200'
      },
      {
        name: '1600x900',
        label: '1600x900',
        value: '1600x900'
      },
      {
        name: '1440x900',
        label: '1440x900',
        value: '1440x900'
      },
      {
        name: '1366x768',
        label: '1366x768',
        value: '1366x768'
      },
      {
        name: '1280x1024',
        label: '1280x1024',
        value: '1280x1024'
      },
      {
        name: '1280x800',
        label: '1280x800',
        value: '1280x800'
      },
      {
        name: '1024x768',
        label: '1024x768',
        value: '1024x768'
      }
    ]
  },
  {
    name: 'fullscreenTransitionType',
    label: 'Fullscreen transition type:',
    type: 'select',
    defaultValue: 'switch',
    values: [
      {
        name: 'default',
        label: 'Default',
        value: 'default'
      },
      {
        name: 'switch',
        label: 'Switch',
        value: 'switch'
      },
      {
        name: 'smooth',
        label: 'Smooth',
        value: 'smooth'
      },
      {
        name: 'fading',
        label: 'Fading',
        value: 'fading'
      }
    ]
  },
  {
    name: 'host',
    label: 'Host:',
    type: 'select',
    defaultValue: 'dribbble.com',
    crossHost: true,
    danger: true,
    values: [
      {
        name: 'dribbble.com',
        label: 'dribbble.com',
        value: 'dribbble.com'
      },
    ]
  },
  {
    name: 'colorfulImages',
    label: 'Colorful images',
    type: 'checkbox',
    defaultValue: true
  },
  {
    name: 'largeThumbs',
    label: 'Large thumbs',
    type: 'checkbox',
    defaultValue: true
  },
  {
    name: 'expansiveFullsreenTitle',
    label: 'Expansive fullscreen title',
    type: 'checkbox',
    defaultValue: false
  },
  {
    name: 'keepImagesInBounds',
    label: 'Keep images in bounds',
    type: 'checkbox',
    defaultValue: false
  },
  {
    name: 'preloadImages',
    label: 'Preload images',
    type: 'checkbox',
    defaultValue: false
  },
  {
    name: 'downloadEveryImage',
    label: 'Download every image',
    type: 'checkbox',
    defaultValue: false
  },
  {
    name: 'autoDownloadInFullscreen',
    label: 'Auto download in fullscreen',
    type: 'checkbox',
    defaultValue: false
  },
  {
    name: 'autoSlideInFullscreen',
    label: 'Auto slide in fullscreen',
    type: 'checkbox',
    defaultValue: false
  },
  {
    name: 'showTopNavigation',
    label: 'Show top navigation',
    immediateEffect: true,
    type: 'checkbox',
    defaultValue: false
  },
  {
    name: 'showBottomNavigation',
    label: 'Show bottom navigation',
    immediateEffect: true,
    type: 'checkbox',
    defaultValue: true
  },
  {
    name: 'pickCommonKeywords',
    label: 'Pick common keywords',
    type: 'checkbox',
    defaultValue: true
  },
  {
    name: 'showCommonKeywords',
    label: 'Show common keywords',
    immediateEffect: true,
    type: 'checkbox',
    defaultValue: true
  },
  {
    name: 'pickSeenImages',
    label: 'Pick seen images',
    type: 'checkbox',
    defaultValue: true
  },
  {
    name: 'hideSeenImages',
    label: 'Hide seen images',
    type: 'checkbox',
    defaultValue: true
  },
  {
    name: 'hideImagesOnMainPage',
    label: 'Hide images on main page',
    type: 'checkbox',
    defaultValue: true
  },
  {
    name: 'hideImagesOnImagePage',
    label: 'Hide images on image page',
    type: 'checkbox',
    defaultValue: true
  },
  {
    name: 'tabNavigator',
    label: 'Use tab navigator',
    type: 'checkbox',
    defaultValue: true
  },
  {
    name: 'preventPullRefresh',
    label: 'Prevent pull refresh',
    type: 'checkbox',
    defaultValue: true
  },
  {
    name: 'disableContextMenu',
    label: 'Disable context menu',
    type: 'checkbox',
    defaultValue: true
  },
  {
    name: 'tabNavigatorSwipeOpen',
    label: 'Open tab navigator with swipes',
    type: 'checkbox',
    defaultValue: true
  },
  {
    name: 'useGifs',
    label: 'Use gifs',
    type: 'checkbox',
    defaultValue: false
  },
  {
    name: 'useGifsForAllNew',
    label: 'Use gifs for all new',
    type: 'checkbox',
    defaultValue: false
  },
  {
    name: 'useGifsAlways',
    label: 'Use gifs always',
    type: 'checkbox',
    defaultValue: false
  },
  {
    name: 'useTabSaving',
    label: 'Use tab saving',
    type: 'checkbox',
    defaultValue: true
  },
  {
    name: 'closeExpiredTabs',
    label: 'Close expired tabs',
    type: 'checkbox',
    defaultValue: false
  },
  {
    name: 'useFullscreenBackground',
    label: 'Use fullscreen background',
    type: 'checkbox',
    defaultValue: true
  },
  {
    name: 'useSettingsSaveDelay',
    label: 'Use settings save delay',
    type: 'checkbox',
    defaultValue: false,
    crossHost: true
  },
  {
    name: 'showGifsOnImagePage',
    label: 'Show gifs on image page',
    type: 'checkbox',
    defaultValue: true
  },
  {
    name: 'lowercaseTags',
    label: 'Lowercase tags',
    type: 'checkbox',
    defaultValue: true
  },
  {
    name: 'newTabOnSearch',
    label: 'Create new tab on search',
    type: 'checkbox',
    defaultValue: false
  },
  {
    name: 'goToNewTabOnSearch',
    label: 'Go to new tab on search',
    type: 'checkbox',
    defaultValue: false
  },
  {
    name: 'hideImagesCountOnMainPage',
    label: 'Hide images count on main page',
    type: 'checkbox',
    defaultValue: false
  },
  {
    name: 'showTumblrSource',
    label: 'Show tumblr source',
    type: 'checkbox',
    defaultValue: true
  },
  {
    name: 'disableImageDownload',
    label: 'Disable image download',
    type: 'checkbox',
    defaultValue: true
  },
  {
    name: 'closeControlsBarOnAction',
    label: 'Close controls bar on action',
    type: 'checkbox',
    defaultValue: true
  },
  {
    name: 'showCommonKeywordsInTabs',
    label: 'Show common keywords in tabs',
    type: 'checkbox',
    defaultValue: false
  },
  {
    name: 'showFullscreenUIHelp',
    label: 'Show fullscreen UI help',
    type: 'checkbox',
    defaultValue: false
  },
  {
    name: 'openNewKeywordsBtn',
    label: 'Open new keywords button',
    type: 'checkbox',
    defaultValue: true,
    immediateEffect: true
  },
  {
    name: 'preventLastSlideTabCreation',
    label: 'Prevent last slide tab creation',
    type: 'checkbox',
    defaultValue: true
  },
  {
    name: 'closeTabsWithVisitedOrOpenedTags',
    label: 'Close tabs with visited or opened tags',
    type: 'checkbox',
    defaultValue: true
  },
  {
    name: 'hideImagesWithKeywords',
    label: 'Hide images with keywords',
    type: 'checkbox',
    defaultValue: true
  },
  {
    name: 'dimSeenImages',
    label: 'Dim seen images',
    type: 'checkbox',
    defaultValue: true
  },
  {
    name: 'useContainForGifs',
    label: 'Use contain for gifs',
    type: 'checkbox',
    defaultValue: true
  },
  {
    name: 'openAllImagesButton',
    label: 'Open all images button',
    type: 'checkbox',
    defaultValue: true
  },
  {
    name: 'openVisitedImages',
    label: 'Open visited images',
    type: 'checkbox',
    defaultValue: true
  },
  {
    name: 'forceLogging',
    label: 'Force logging',
    type: 'checkbox',
    defaultValue: true,
    crossHost: true
  },
  {
    name: 'debug',
    label: 'Debug',
    type: 'checkbox',
    defaultValue: true,
    crossHost: true
  },
  {
    name: 'go-to-tab-btn',
    label: 'Go to tab',
    type: 'button',
    buttonGroup: '-10'
  },
  {
    name: 'go-to-tab',
    label: 'Tab index:',
    type: 'text',
    defaultValue: 1
  },
  {
    name: 'clear-visited-images',
    label: 'Clear visited images',
    type: 'button',
    buttonGroup: '-4'
  },
  {
    name: 'clear-common-keywords',
    label: 'Clear common keywords',
    type: 'button',
    buttonGroup: '-4'
  },
  {
    name: 'clear-tabs',
    label: 'Clear tabs',
    type: 'button',
    buttonGroup: '-3'
  },
  {
    name: 'clear-seen-images',
    label: 'Clear seen images',
    type: 'button',
    buttonGroup: '-3'
  },
  {
    name: 'import-visited-images-from-host-btn',
    label: 'Import visited images from host',
    type: 'button',
    buttonGroup: '-2'
  },
  {
    name: 'import-common-keywords-from-host-btn',
    label: 'Import common keywords from host',
    type: 'button',
    buttonGroup: '-1'
  },
  {
    name: 'import-tabs-from-host-btn',
    label: 'Import tabs from host',
    type: 'button',
    buttonGroup: '0'
  },
  {
    name: 'import-seen-images-from-host-btn',
    label: 'Import seen images from host',
    type: 'button',
    buttonGroup: '1'
  },
  {
    name: 'import-all-settings-from-host-btn',
    label: 'Import all settings from host',
    type: 'button',
    buttonGroup: '2'
  },
  {
    name: 'import-settings-from-host',
    label: 'Choose host to import from:',
    type: 'select',
    defaultValue: 'dribbble.com',
    values: [
      {
        name: 'dribbble.com',
        label: 'dribbble.com',
        value: 'dribbble.com'
      }
    ]
  },
  {
    name: 'compress-local-storage',
    label: 'Compress local storage',
    type: 'button',
    buttonGroup: '3'
  },
  {
    name: 'decompress-local-storage',
    label: 'Decompress local storage',
    type: 'button',
    buttonGroup: '3'
  },
  {
    name: 'clear-all-settings',
    label: 'Clear all settings',
    type: 'button',
    buttonGroup: '4'
  },
  {
    name: 'clear-host-settings',
    label: 'Clear host settings',
    type: 'button',
    buttonGroup: '4'
  },
  {
    name: 'traverse-tabs',
    label: 'Traverse tabs',
    type: 'button',
    buttonGroup: '5'
  },
  {
    name: 'stop-traversing-tabs',
    label: 'Stop traversing',
    type: 'button',
    buttonGroup: '5'
  },
  {
    name: 'download-settings',
    label: 'Download settings',
    type: 'button',
    buttonGroup: '8'
  },
  {
    name: 'upload-settings',
    label: 'Upload settings',
    type: 'button',
    buttonGroup: '8'
  },
  {
    name: 'download-general-settings',
    label: 'Download gen. setts',
    type: 'button',
    buttonGroup: '9'
  },
  {
    name: 'download-host-history',
    label: 'Download host history',
    type: 'button',
    buttonGroup: '9'
  },
  {
    name: 'restore-cross-host-settings',
    label: 'Restore X-host setts.',
    type: 'button',
    buttonGroup: '10'
  },
  {
    name: 'apply-settings-to-cross-host',
    label: 'Set setts. as X-host',
    type: 'button',
    buttonGroup: '10'
  },
  {
    name: 'restore-general-settings',
    label: 'Restore gen. setts.',
    type: 'button',
    buttonGroup: '11'
  },
  {
    name: 'restore-host-specific-settings',
    label: 'Restore host setts.',
    type: 'button',
    buttonGroup: '11'
  },
  {
    name: 'restore-tumblr-blog-id',
    label: 'Restore tumblr blog id',
    type: 'button',
    buttonGroup: '20'
  },
  {
    name: 'close-settings',
    label: 'Close',
    type: 'button',
    buttonGroup: '20'
  }
];
