var parser = require('../parser/parser');
var settings = require('./settings');
var settingsInputs = require('./settings-inputs');
var settingsManager = require('./settings-manager');
var tabNavigator = require('../tab-navigator');
var utils = require('../utils');

var $settingsMenu = $('.settings-menu');

var controller = Object.create(null);

var throttleDelay = 1 * 1000;

var isOpen = false;

function initialize() {
  createSettingsInputs();
  setAllSettings();
  bindEvents();
}

function open() {
  $settingsMenu.removeClass('disabled');
  isOpen = true;
}

function close() {
  $settingsMenu.addClass('disabled');
  isOpen = false;
}

function createSettingsInputs() {
  _.forEach(settingsInputs, function (input) {
    fillInputProperties(input);
    switch (input.type) {
      case 'checkbox':
        createCheckbox(input);
        break;
      case 'button':
        createButton(input);
        break;
      case 'radio':
        createRadio(input);
        break;
      case 'text':
        createText(input);
        break;
      case 'confirmationInput':
        createConfirmationInput(input);
        break;
      case 'select':
        createSelect(input);
        break;
      default:
        utils.log('Unknown input type for ' + input.name + ': ' + input.type);
    }
  });
}

function createCheckbox(checkbox) {
  var $group = createGroup();
  var $label = createLabel();
  var $checkbox = $('<input type="checkbox" class="checkbox">');
  var $span = createSpan('text');

  $label.addClass(checkbox.dashedName + '-label');

  $checkbox.addClass(checkbox.dashedName + '-checkbox');
  $checkbox.attr('name', checkbox.dashedName);

  $span.addClass(checkbox.dashedName + '-text');
  $span.html(checkbox.label);

  $label.append($checkbox);
  $label.append($span);
  $group.append($label);

  appendItemToMenu($group);
  createInputSetFunction(checkbox, function (value) {
    value = !!value;
    $('[name="' + checkbox.dashedName + '"]').prop('checked', value);
  });
}

function createButton(button) {
  var $buttonGroup = null;
  if (button.buttonGroup) {
    $buttonGroup = createButtonGroup(button.buttonGroup);
  } else {
    $buttonGroup = createGroup();
  }

  var $button = $('<button type="button" class="button"></button>');

  $button.html(button.label);
  $button.addClass(button.dashedName);
  $button.attr('name', button.dashedName);

  $buttonGroup.append($button);
  appendItemToMenu($buttonGroup);
}

function createButtonGroup(id) {
  var $group = $('.button-group-' + id).eq(0);

  if ($group.length === 0) {
    $group = createGroup();
    $group.addClass('button-group');
    $group.addClass('button-group-' + id);
  }

  return $group;
}

function createRadio(radio) {
  var $label = createRadioLabel(radio);
  appendItemToMenu($label);
  _.forEach(radio.values, function (value) {
    fillInputProperties(value);
    var $value = createRadioValue(radio, value);
    appendItemToMenu($value);
  });

  createInputSetFunction(radio, function (value) {
    $('[name="' + radio.dashedName + '"]').filter('[value="' + value + '"]').prop('checked', true);
  });
}

function createRadioLabel(radio) {
  var $group = createGroup();
  var $label = createLabel();
  var $span = createSpan('text');

  $span.html(radio.label);

  $label.append($span);
  $group.append($label);

  return $group;
}

function createRadioValue(radio, radioValue) {
  var $group = createGroup();
  var $label = createLabel();
  var $radio = $("<input type='radio' class='radio'>");
  var $span = createSpan('text');

  $label.addClass(radio.dashedName + '-label');

  $radio.addClass(radio.dashedName + '-radio');
  $radio.addClass(radio.dashedName + '-' + radioValue.dashedName + '-radio');
  $radio.attr('name', radio.dashedName);
  $radio.attr('value', radioValue.value);

  $span.addClass(radio.dashedName + '-text');
  $span.html(radioValue.label);

  $label.append($radio);
  $label.append($span);
  $group.append($label);

  return $group;
}

function createText(text) {
  var $group = createGroup();
  var $label = createLabel();
  var $spanText = createSpan('text');
  var $text = $("<input type='text' class='input-text'>");
  var $spanValue = createSpan('value');

  $label.addClass(text.dashedName + '-label');

  $spanText.addClass(text.dashedName + '-text');
  $spanText.html(text.label);

  $text.addClass(text.dashedName + '-input-text');
  $text.attr('name', text.dashedName);

  $spanValue.addClass(text.dashedName + '-value');

  $label.append($spanText);
  $label.append($text);
  $label.append($spanValue);
  $group.append($label);

  appendItemToMenu($group);
  createInputSetFunction(text, function (value) {
    $spanValue.html(value);
  });
}

function createConfirmationInput(text) {
  var $group = createGroup();
  var $label = createLabel();
  var $spanText = createSpan('text');
  var $text = $("<input type='text' class='input-text'>");
  var $buttonAdd = $('<button type="button" class="confirmation-button">+</button>');
  var $buttonRemove = $('<button type="button" class="confirmation-button">-</button>');

  $label.addClass(text.dashedName + '-label');

  $spanText.addClass(text.dashedName + '-text');
  $spanText.html(text.label);

  $text.addClass(text.dashedName + '-input-text');
  $text.attr('name', text.dashedName + '-input');

  $buttonAdd.addClass(text.dashedName + '-confirmation-button');
  $buttonAdd.attr('name', text.dashedName + '-button-add');

  $buttonRemove.addClass(text.dashedName + '-confirmation-button');
  $buttonRemove.attr('name', text.dashedName + '-button-remove');

  $label.append($spanText);
  $label.append($text);
  $label.append($buttonAdd);
  $label.append($buttonRemove);
  $group.append($label);

  appendItemToMenu($group);
}

function createSelect(select) {
  var $group = createGroup();
  var $label = createLabel();
  var $spanText = createSpan('text');
  var $select = $("<select class='select'></select>");

  $label.addClass(select.dashedName + '-label');

  $spanText.addClass(select.dashedName + '-text');
  $spanText.html(select.label);

  $select.addClass(select.dashedName + '-select');
  $select.attr('name', select.dashedName);

  if (select.values) {
    _.forEach(select.values, function (v) {
      var $option = $("<option></option>");
      $option.attr('name', v.name);
      $option.attr('value', v.value);
      $option.html(v.label);
      $select.append($option);
    });
  }
  if (select.source) {
    _.forEach(settings.get(select.source), function (v) {
      var $option = $("<option></option>");
      $option.attr('name', v);
      $option.html(v);
      $select.append($option);
    });
  }

  $select.val(settings.get(select.name));

  $label.append($spanText);
  $label.append($select);
  $group.append($label);

  appendItemToMenu($group);
}

function fillInputProperties(input) {
  input.label = input.label || utils.camelCaseToNatural(input.name);
  input.dashedName = input.dashedName || utils.camelCaseToDash(input.name);
  input.capitalizedName = utils.capitalizeFirstLetter(input.name);
  input.naturalName = input.naturalName || utils.camelCaseToNatural(input.name);

  return input;
}

function createGroup() {
  return $('<div class="group"></div>');
}

function createLabel() {
  return $('<label class="label"></label>');
}

function createSpan(className) {
  return $('<span class="' + className + '"></span>');
}

function appendItemToMenu(input) {
  $settingsMenu.append(input);
}

function createInputSetFunction(input, setFunction) {
  controller['set' + input.capitalizedName] = setFunction;
}

function setAllSettings() {
  _.forEach(settingsInputs, function (input) {
    if (controller['set' + input.capitalizedName]) {
      controller['set' + input.capitalizedName](settings['get' + input.capitalizedName]());
    }
  });
}

function bindEvents() {
  $settingsMenu.find('.fixed-close').on('click', function () {
    close();
  });

  $('[name="import-visited-images-from-host-btn"]').on('click', function () {
    var settingsToImport = [
      'visitedImages'
    ];
    importSettings(settingsToImport, settings.get('import-settings-from-host'), settings.get('host'));
  });

  $('[name="import-common-keywords-from-host-btn"]').on('click', function () {
    var settingsToImport = [
      'commonKeywords'
    ];
    importSettings(settingsToImport, settings.get('import-settings-from-host'), settings.get('host'));
  });

  $('[name="import-tabs-from-host-btn"]').on('click', function () {
    var settingsToImport = [
      'tabs'
    ];
    importSettings(settingsToImport, settings.get('import-settings-from-host'), settings.get('host'));
  });

  $('[name="import-seen-images-from-host-btn"]').on('click', function () {
    var settingsToImport = [
      'seenImages'
    ];
    importSettings(settingsToImport, settings.get('import-settings-from-host'), settings.get('host'));
  });

  $('[name="import-all-settings-from-host-btn"]').on('click', function () {
    var settingsToImport = [
      'visitedImages',
      'commonKeywords',
      'tabs',
      'seenImages'
    ];
    importSettings(settingsToImport, settings.get('import-settings-from-host'), settings.get('host'));
  });

  $('[name="import-all-settings-from-host-btn"]').on('click', function () {
    var settingsToImport = [
      'visitedImages',
      'commonKeywords',
      'tabs',
      'seenImages'
    ];
    importSettings(settingsToImport, settings.get('import-settings-from-host'), settings.get('host'));
  });

  $('[name="go-to-tab-btn"]').on('click', function () {
    tabNavigator.goToTab(settings.get('go-to-tab') - 1);
  });

  $('[name="restore-tumblr-blog-id"]').on('click', function () {
    settings.set('tumblrBlogId', settings.default.tumblrBlogId);
  });

  $('[name="traverse-tabs"]').on('click', function () {
    tabNavigator.traverseTabs();
  });

  $('[name="stop-traversing-tabs"]').on('click', function () {
    tabNavigator.stopTraversingTabs();
  });

  $('[name="download-settings"]').on('click', function () {
    downloadSettings();
  });

  $('[name="upload-settings"]').on('click', function () {
    uploadSettings();
  });

  $('[name="download-general-settings"]').on('click', function () {
    downloadGeneralSettings();
  });

  $('[name="download-host-history"]').on('click', function () {
    downloadHostHistory();
  });

  $('[name="restore-cross-host-settings"]').on('click', function () {
    restoreCrossHostSettings();
  });

  $('[name="apply-settings-to-cross-host"]').on('click', function () {
    applySettingsToCrossHost();
  });

  $('[name="restore-general-settings"]').on('click', function () {
    restoreGeneralSettings();
  });

  $('[name="restore-host-specific-settings"]').on('click', function () {
    restoreHostSpecificSettings();
  });

  $('[name="clear-all-settings"]').on('click', function () {
    if (confirm('Do you really want to clear all settings?')) {
      if (confirm('Do you want to download current settings?')) {
        downloadSettings();
      }
      clearAll();
    }
  });

  $('[name="clear-host-settings"]').on('click', function () {
    if (confirm('Do you really want to clear host settings?')) {
      if (confirm('Do you want to download current settings?')) {
        downloadSettings();
      }
      clearHost();
    }
  });

  $('[name="clear-visited-images"]').on('click', function () {
    if (confirm('Do you really want to clear visited images?')) {
      if (confirm('Do you want to download current settings?')) {
        downloadSettings();
      }
      clearVisitedImages();
    }
  });

  $('[name="clear-common-keywords"]').on('click', function () {
    if (confirm('Do you really want to clear common keywords?')) {
      if (confirm('Do you want to download current settings?')) {
        downloadSettings();
      }
      clearCommonKeywords();
    }
  });

  $('[name="clear-tabs"]').on('click', function () {
    if (confirm('Do you really want to clear tabs?')) {
      if (confirm('Do you want to download current settings?')) {
        downloadSettings();
      }
      clearTabs();
    }
  });

  $('[name="clear-seen-images"]').on('click', function () {
    if (confirm('Do you really want to clear seen images?')) {
      if (confirm('Do you want to download current settings?')) {
        downloadSettings();
      }
      clearSeenImages();
    }
  });

  $('[name="compress-local-storage"]').on('click', function () {
    settings.compressLocalStorage();
  });

  $('[name="decompress-local-storage"]').on('click', function () {
    settings.decompressLocalStorage();
  });

  $('[name="close-settings"]').on('click', function () {
    close();
  });

  _.forEach(settingsInputs, function (input) {
    switch (input.type) {
      case 'checkbox':
        bindCheckboxEvent(input);
        break;
      case 'button':
        bindButtonEvent(input);
        break;
      case 'radio':
        bindRadioEvent(input);
        break;
      case 'text':
        bindTextEvent(input);
        break;
      case 'confirmationInput':
        bindConfirmationInputEvent(input);
        break;
      case 'select':
        bindSelectEvent(input);
        break;
    }

    bindCustomEvent(input);
  });
}

function bindCheckboxEvent(checkbox) {
  $checkbox = $('[name="' + checkbox.dashedName + '"]');
  $checkbox.on('change', function () {
    var newValue = !!$(this).prop('checked');
    // utils.log(checkbox.naturalName + ' has been changed to: ' + newValue);
    settings['set' + checkbox.capitalizedName](newValue);
    if (checkbox.immediateEffect) {
      settingsManager['apply' + checkbox.capitalizedName](newValue);
    }
  });
}

function bindButtonEvent(button) {

}

function bindRadioEvent(radio) {
  $radio = $('[name="' + radio.dashedName + '"]');
  $radio.on('change', function () {
    // utils.log(radio.naturalName + ' has been changed to: ' + this.value);
    settings['set' + radio.capitalizedName](this.value);
    if (radio.immediateEffect) {
      settingsManager['apply' + radio.capitalizedName](this.value);
    }
  });
}

function bindTextEvent(text) {
  var throttledInputFunc = utils.throttle(function (value) {
    // utils.log(text.naturalName + ' has been set to ' + value);
    settings['set' + text.capitalizedName](value);
    controller['set' + text.capitalizedName](value);
    if (text.immediateEffect) {
      settingsManager['apply' + text.capitalizedName](value);
    }
  }, throttleDelay);

  $text = $('[name="' + text.dashedName + '"]');
  $text.on('input', function () {
    throttledInputFunc(this.value);
  });
}

function bindConfirmationInputEvent(input) {
  var changeValue = function (mode) {
    var $input = $('[name="' + input.dashedName + '-input"]');
    var val = $input.val().trim();
    if (input.lowercase) {
      val = val.toLowerCase();
    }
    $input.val('');

    if (val.length > 0) {
      if (mode === 'add') {
        settings.add(input.bindedTo, val);
      } else {
        settings.remove(input.bindedTo, val);
      }
    }

    if (input.immediateEffect) {
      settingsManager['apply' + input.capitalizedName](val);
    }
  };

  $('[name="' + input.dashedName + '-button-add"]').on('click', function () {
    changeValue('add');
  });
  $('[name="' + input.dashedName + '-button-remove"]').on('click', function () {
    changeValue('remove');
  });
}

function bindSelectEvent(select) {
  $('[name="' + select.dashedName + '"]').on('change', function () {
    settings.set(select.name, this.value);
  });
}

function bindCustomEvent(input) {
  if (input.on && input.do) {
    $input = $('[name="' + input.dashedName + '"]');
    $input.on(input.on, function () {
      input.do();
    });
  }
}

function downloadSettings() {
  downloadData(utils.json(settings.getAll()), 'settings');

  utils.log('Successfully saved settings');
}

function downloadGeneralSettings() {
  downloadData(utils.json(settings.getGeneralSettings()), 'generalSettings');
  utils.log('Successfully saved general settings');
}

function downloadHostHistory() {
  downloadData(utils.json(settings.getHostSpecificSettings()), 'userHistory');
  utils.log('Successfully saved user history');
}

function downloadData(data, name) {
  var date = new Date().toLocaleString().replace(/\//g, ',');
  var textFile = utils.makeTextFile(data);
  var $link = $('<a download="' + name + ' - ' + date + '.txt"></a>');
  $link.attr('href', textFile);
  $link.hide();
  $settingsMenu.append($link);
  $link.append('<div>').find('div').trigger('click');
  $link.remove();
}

function uploadSettings() {
  var inputFile = $('<input>');
  inputFile.attr('type', 'file').hide().appendTo($settingsMenu).append('<div>').find('div').trigger('click');

  inputFile.on('change', function () {
    var fileUrl = window.URL.createObjectURL(inputFile[0].files[0]);
    $.get(fileUrl, function (data) {
      utils.log('Successfully loaded settings from file ' + inputFile[0].files[0].name);
      settings.loadAll(JSON.parse(data));
    });
    inputFile.remove();
  });
}

function applySettingsToCrossHost() {
  settings.applySettingsToCrossHost();
  updateMenu();
}

function restoreCrossHostSettings() {
  settings.restoreCrossHostSettings();
  updateMenu();
}

function restoreGeneralSettings() {
  settings.restoreGeneralSettings();
  updateMenu();
}

function restoreHostSpecificSettings() {
  settings.restoreHostSpecificSettings();
  updateMenu();
}

function clearAll() {
  settings.clearAll();
  updateMenu();
}

function clearHost() {
  settings.clearHost();
  updateMenu();
}

function clearVisitedImages() {
  settings.set('visitedImages', []);
  updateMenu();
}

function clearCommonKeywords() {
  settings.set('commonKeywords', []);
  updateMenu();
}

function clearTabs() {
  settings.set('tabs', []);
  updateMenu();
}

function clearSeenImages() {
  settings.set('seenImages', []);
  updateMenu();
}

function updateMenu() {
  setAllSettings();
  settingsManager.applyAll(settings.getAll());
}

function importSettings(settingsToImport, from, to) {
  var data = JSON.parse(settings.getItemWithDecompression(from));

  _.forEach(settingsToImport, function (s) {
    var merge;
    if (s === 'tabs') {
      var origTabs = tabNavigator.normalizeTabs(JSON.parse(settings.get(s)));
      var newTabs = tabNavigator.normalizeTabs(JSON.parse(data[s]));

      newTabs = _.map(newTabs, function (t) {
        var newT = _.assign({}, t);
        t.current = false;
        var hash = parser.parseHash(t.link);
        hash.host = to;
        hash.gps = hash.gps || from;
        t.link = parser.createHash(hash);
        return t;
      });

      merge = filterTabs(_.concat(origTabs, newTabs));
      merge = JSON.stringify(tabNavigator.minifyTabs(merge));
    } else {
      merge = _.concat(settings.get(s), data[s]);
    }
    settings.set(s, merge);
  });
}

function filterTabs(tabs) {
  var filteredTabs = tabs;

  filteredTabs = _.uniqBy(filteredTabs, function (ti) {
    if (ti.current) {
      return -1;
    } else {
      return ti.link;
    }
  });

  filteredTabs = _.uniqBy(filteredTabs, function (ti) {
    if (ti.current) {
      return -1;
    } else {
      var res = /(^[0-9?]+ images:)(.*)/.exec(ti.title);
      if (res) {
        return 'check for multiple tags opened: ' + res[2].trim();
      } else {
        return ti.title;
      }
    }
  });

  filteredTabs = filteredTabs.filter(function (ti) {
    return ti.current || !/[а-яА-ЯЁёіІїЇ]/.test(ti.title);
  });


  if (settings.get('closeTabsWithVisitedOrOpenedTags')) {
    var visitedImages = settings.get('visitedImages');
    var openedImages = tabNavigator.getOpenedTags(filteredTabs);

    filteredTabs = _.filter(filteredTabs, function (ti) {
      var tags = tabNavigator.getTagsForImagePage(ti);
      if (tags.length > 0) {
        return ti.current || _.difference(tags, visitedImages, openedImages).length > 0;
      } else {
        return true;
      }
    });
  }

  return filteredTabs;
}

module.exports = {
  initialize: initialize,
  open: open,
  close: close,
  isOpen: function () {
    return isOpen;
  }
};
