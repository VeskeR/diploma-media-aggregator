var bindEvents = require('../bind-events');
var header = require('../header');
var page = require('../page');
var settings = require('../settings/settings');
var settingsManager = require('../settings/settings-manager');
var settingsMenu = require('../settings/settings-menu');
var tabNavigator = require('../tab-navigator');

var base = {
  start: function () {
    settings.loadAll();
    tabNavigator.initialize();

    page.show();

    settingsMenu.initialize();
    header.initialize();

    settingsManager.applyAll(settings.getAll());

    bindEvents();
  }
};

module.exports = base;
