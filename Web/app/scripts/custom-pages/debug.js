var bindEvents = require('../bind-events');
var header = require('../header');
var settings = require('../settings/settings');
var settingsManager = require('../settings/settings-manager');
var settingsMenu = require('../settings/settings-menu');

var debug = {
  start: function () {
    settings.loadAll();
    settingsMenu.initialize();
    settingsManager.applyAll(settings.getAll());
    header.initialize();

    bindEvents();
  }
};

module.exports = debug;
