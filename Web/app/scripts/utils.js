var coreUtils = require('./core-utils');
var settings = require('./settings/settings');

var whateverorigin = 'http://whateverorigin.org/get?url=';

var localRedirectHome = 'http://' + settings.get('myRedirectHost') + '/';
var localRedirectGeneral = localRedirectHome + 'redirect?url=';
var localRedirectImage = localRedirectHome + 'redirectimage?url=';

var herokuGeneral = 'redirect?url=';
var herokuImage = 'redirectimage?url=';

var herokuHomeUS = 'https://stormy-journey-82852.herokuapp.com/';
var herokuGeneralUS = herokuGeneralUS + herokuGeneral;
var herokuImageUS = herokuGeneralUS + herokuImage;

var herokuHomeEU = 'https://damp-harbor-99871.herokuapp.com/';
var herokuGeneralEU = herokuHomeEU + herokuGeneral;
var herokuImageEU = herokuHomeEU + herokuImage;

// $.ajaxSetup({
//   scriptCharset: "utf-8", //or "ISO-8859-1"
//   contentType: "application/json; charset=utf-8"
// });

var utils = $.extend({}, coreUtils, {
  hdResolution: settings.get('hdResolution'),
  setHdResolution: function (resolution) {
    this.hdResolution = resolution;
  },
  getUrlsContent: function (urls) {
    var self = this;
    if (!_.isArray(urls)) {
      urls = [urls];
    }
    var promises = _.map(urls, function (url) {
      return self.getUrlContent(url);
    });
    return Promise.all(promises);
  },
  getUrlContent: function (url) {
    var self = this;

    return new Promise(function (resolve, reject) {
      var redirectRequest;
      // url = url.replace(/’/gi, "'");
      switch (settings.get('redirectService')) {
        case 'whateverorigin':
          redirectRequest = whateverorigin + encodeURIComponent(url) + '&callback=?';
          break;
        case 'local':
          redirectRequest = localRedirectGeneral + encodeURIComponent(url);
          break;
        case 'herokuUS':
          redirectRequest = herokuGeneralUS + encodeURIComponent(url);
          break;
        case 'herokuEU':
          redirectRequest = herokuGeneralEU + encodeURIComponent(url);
          break;
      }

      self.log('GET: ' + url);
      self.log('Sending redirect request to: ' + redirectRequest);

      $.getJSON(redirectRequest, resolve);
    });
  },
  getImage: function (url, callback) {
    var redirectRequest;
    switch (settings.get('redirectService')) {
      case 'local':
        redirectRequest = localRedirectImage + encodeURIComponent(url);
        break;
      case 'herokuUS':
        redirectRequest = herokuImageUS + encodeURIComponent(url);
        break;
      case 'herokuEU':
        redirectRequest = herokuImageEU + encodeURIComponent(url);
        break;
    }

    this.log('GET: ' + url);
    this.log('Sending redirect request to: ' + redirectRequest);

    $.getJSON(redirectRequest, callback);
  },
  getPlaceholditLink: function (width, height, src) {
    var link = 'http://placehold.it/' + width + 'x' + height + '/';
    if (settings.get('colorfulImages') && src) {
      var colorCode = this.getHashCode(src).toString(16).replace('-', '').substring(0, 3);
      link += colorCode + '/';
    }
    return link;
  },
  getImgSrc: function (smallSrc, largeSrc, hdSrc, width, height) {
    if (settings.get('debug')) {
      return this.getPlaceholditLink(width, height, smallSrc || largeSrc || hdSrc);
    }
    var src;

    if (hdSrc) {
      src = hdSrc;
    } else if (settings.get('largeThumbs')) {
      if (largeSrc) {
        src = largeSrc;
      } else {
        if (!/\.gif/.test(smallSrc)) {
          src = this.changePicSrc(smallSrc, 'large');
        } else {
          src = smallSrc;
        }
      }
    } else {
      src = smallSrc;
    }

    return src;
  },
  changePicSrc: function(src, type) {
    host = this.detectHostFromSrc(src);
    var changedSrc = src;

    switch (host) {
      case 'dribbble.com':
        changedSrc = src.replace(/(_1x|_teaser)/gi, '');;

        switch (type) {
          case 'small':
            return changedSrc.replace(/(\.(png|jpg))$/gi, '_teaser$1');
            break;
          case 'large':
            return changedSrc.replace(/(\.(png|jpg))$/gi, '_1x$1');
            break;
          case 'hd':
          default:
            return changedSrc;
            break;
        }
        break;
      default:
        return src;
    }

    return changedSrc;
  },
  detectHostFromSrc: function (src) {
    var hosts = [
      'dribbble.com'
    ];
    var host;
    hosts.forEach(function (h) {
      if (src.indexOf(h) !== -1) {
        host = h;
      }
    });
    return host;
  },
  getDownloadLink: function (link, callback) {
    if (!callback) {
      return link;
    } else {
      var redirectService = settings.get('redirectService');
      if ((redirectService === 'local' || redirectService === 'herokuUS' || redirectService === 'herokuEU') && settings.get('downloadEveryImage')) {
        this.getImage(link, function (data) {
          callback('data:image/jpeg;base64,' + data.contents);
        });
      } else {
        callback(link);
      }
    }
  },
  getDownloadFilename: function (link, host) {
    host = host || settings.get('host');
    var filename;

    switch (host) {
      case 'dribbble.com':
        filename = link;
        break;
      default:
        return link;
    }

    return filename;
  },
  getImageId: function (downloadLink, host) {
    host = this.detectHostFromSrc(downloadLink);
    var imageId;

    switch (host) {
      case 'dribbble.com':
        return downloadLink;
        break;
      default:
        return downloadLink;
    }

    return imageId;
  },
  setImgSizingBehaviour: function ($elem) {
    if (settings.get('keepImagesInBounds')) {
      $elem.removeClass('full-width');
      $elem.addClass('keep-in-bounds');
    } else {
      $elem.addClass('full-width');
      $elem.removeClass('keep-in-bounds');
    }
  },
  log: function () {
    if (settings.get('debug') || settings.get('forceLogging')) {
      console.log.apply(console, arguments);
    }
  },
  replaceSrcs: function (text) {
    var res = text.replace(/src/gi, 'data-replaced-src');
    return res;
  }
});

window.utils = utils;

module.exports = utils;
