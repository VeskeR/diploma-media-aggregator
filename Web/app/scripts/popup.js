var popup = {
  timeout: 0,
  baseTime: 1000,
  show: function (text, time) {
    clearTimeout(this.timeout);
    time = time || this.baseTime;
    if ($('#popup').length === 0) {
      $('body').append($('<div id="popup"></div>'));
    }
    var $popup = $('#popup');
    $popup.html(text);
    $popup.show();
    this.timeout = setTimeout(function () {
      $popup.hide();
    }, time);
  }
};

window.popup = popup;

module.exports = popup;
