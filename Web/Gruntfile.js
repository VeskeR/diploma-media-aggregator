'use strict';
module.exports = function (grunt) {
  require('load-grunt-tasks')(grunt);

  // grunt.loadTasks('tasks');

  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),
    browserify: {
      serve: {
        options: {
          browserifyOptions: {
            debug: true
          }
        },
        files: {
          '.tmp/scripts/main.js': ['app/scripts/main.js']
        }
      },
      dist: {
        files: {
          'dist/scripts/main.js': ['app/scripts/main.js']
        }
      }
    },
    clean: {
      serve: {
        src: ['.tmp/']
      },
      dist: {
        src: ['dist/']
      },
      gzip: {
        src: ['gzip/']
      }
    },
    compress: {
      dist: {
        options: {
          mode: 'gzip',
          pretty: true
        },
        expand: true,
        cwd: 'dist/',
        src: ['**/*.{js,css}'],
        dest: 'gzip/'
      }
    },
    concat: {
      options: {
        separator: ';'
      },
      dist: {
        files: {
          'dist/scripts/vendor/vendor.js': [
            'node_modules/zepto/dist/zepto.min.js',
            'node_modules/lz-string/libs/lz-string.min.js',
            'app/scripts/vendor/lodash/lodash.custom.min.js'
          ],
          'dist/styles/vendor/bower.css': []
        }
      }
    },
    connect: {
      options: {
        port: 9000,
        hostname: 'localhost',
        livereload: true
      },
      serve: {
        options: {
          open: 'http://localhost:9000/app/index.html',
          base: [
            '.',
            'app',
            '.tmp'
          ]
        }
      },
      serveShare: {
        options: {
          hostname: '0.0.0.0',
          open: 'http://localhost:9000/app/index.html',
          base: [
            '.',
            'app',
            '.tmp'
          ]
        }
      },
      dist: {
        options: {
          livereload: false,
          keepalive: true,
          open: 'http://localhost:9000/index.html',
          base: [
            'dist'
          ]
        }
      },
      distShare: {
        options: {
          hostname: '0.0.0.0',
          livereload: false,
          keepalive: true,
          open: 'http://localhost:9000/dist/index.html',
          base: [
            '.'
          ]
        }
      }
    },
    copy: {
      options: {
        encoding: 'utf-8'
      },
      serve: {
        files: [{
          expand: true,
          cwd: 'app',
          src: '**/*.{wav,ogg,mp3,json,png,xml,ico}',
          dest: '.tmp'
        }]
      },
      dist: {
        files: [{
          expand: true,
          cwd: 'app',
          src: '**/*.{wav,ogg,mp3,json}',
          dest: 'dist'
        }]
      },
      gzip: {
        files: [{
          expand: true,
          cwd: 'dist',
          src: '**/*.html',
          dest: 'gzip'
        }, {
          expand: true,
          cwd: 'gzip',
          src: ['**/*.js'],
          dest: 'gzip',
          ext: '.js.gz'
        }, {
          expand: true,
          cwd: 'gzip',
          src: ['**/*.css'],
          dest: 'gzip',
          ext: '.css.gz'
        }]
      }
    },
    'ftp-deploy': {
      'sex-gallery-dist': {
        auth: {
          host: 'files.000webhost.com',
          port: 21,
          authKey: 'sex-gallery'
        },
        src: 'dist',
        dest: '/public_html'
      },
      'ftop-dist': {
        auth: {
          host: 'ftop-wrapper.net23.net',
          port: 21,
          authKey: 'ftop-wrapper'
        },
        src: 'dist',
        dest: '/public_html'
      },
      'babeswp-dist': {
        auth: {
          host: 'files.000webhost.com',
          port: 21,
          authKey: 'babeswp-wrapper'
        },
        src: 'dist',
        dest: '/public_html'
      },
      'tumblr-dist': {
        auth: {
          host: 'files.000webhost.com',
          port: 21,
          authKey: 'tumblr-wrapper'
        },
        src: 'dist',
        dest: '/public_html'
      },
      gzip: {
        auth: {
          host: 'ftop-wrapper.net23.net',
          port: 21,
          authKey: 'ftop-wrapper'
        },
        src: 'gzip',
        dest: '/public_html'
      }
    },
    postcss: {
      options: {
        map: true,
        processors: [
          require('autoprefixer')({ browsers: ['last 2 versions', 'ie 8', 'ie 9'] })
        ]
      },
      serve: {
        files: [{
          expand: true,
          cwd: '.tmp',
          src: 'styles/**/*.css',
          dest: '.tmp'
        }]
      },
      dist: {
        options: {
          map: false,
          processors: [
            require('autoprefixer')({ browsers: ['last 2 versions', 'ie 8', 'ie 9'] }),
            require('cssnano')()
          ]
        },
        files: {
          'dist/styles/main.min.css': ['dist/styles/main.css']
        }
      }
    },
    processhtml: {
      dist: {
        files: [{
          expand: true,
          cwd: 'app',
          src: '**/*.html',
          dest: 'dist'
        }]
      }
    },
    sass: {
      serve: {
        options: {
          sourceMap: true
        },
        files: {
          '.tmp/styles/main.css': ['app/styles/main.scss']
        }
      },
      dist: {
        files: {
          'dist/styles/main.css': ['app/styles/main.scss']
        }
      }
    },
    uglify: {
      dist: {
        files: {
          'dist/scripts/main.min.js': ['dist/scripts/main.js'],
          'dist/scripts/vendor/vendor.min.js': ['dist/scripts/vendor/vendor.js']
        }
      }
    },
    watch: {
      options: {
        interrupt: true
      },
      bower: {
        files: ['bower.json'],
        tasks: [
          'wiredep'
        ]
      },
      js: {
        files: ['app/**/*.js'],
        tasks: [
          'browserify:serve'
        ]
      },
      sass: {
        files: ['app/**/*.{scss,sass}'],
        tasks: [
          'sass:serve',
          'postcss:serve'
        ]
      },
      serve: {
        options: {
          livereload: '<%= connect.options.livereload %>',
          interrupt: false
        },
        files: [
          '.tmp/**/*.{js,css,html,jpg,png,svg,gif,json}',
          'app/**/*.html'
        ]
      }
    },
    wiredep: {
      options: {
        ignorePath: /^(\.\.\/)*\.\./
      },
      task: {
        src: ['app/**/*.html']
      }
    },
  });

  grunt.registerTask('build:serve', [
    'clean',
    'browserify:serve',
    'sass:serve',
    'postcss:serve',
    'copy:serve'
  ]);

  grunt.registerTask('build:dist', [
    'clean',
    'browserify:dist',
    'sass:dist',
    'concat:dist',
    'copy:dist',
    'uglify:dist',
    'postcss:dist',
    'processhtml:dist',
    'compress:dist',
    'copy:gzip'
  ]);

  grunt.registerTask('serve', [
    'build:serve',
    'connect:serve',
    'watch'
  ]);

  grunt.registerTask('serve:share', [
    'build:serve',
    'connect:serveShare',
    'watch'
  ]);

  grunt.registerTask('serve:dist', [
    'build:dist',
    'connect:dist'
  ]);

  grunt.registerTask('serve:dist:share', [
    'build:dist',
    'connect:distShare'
  ]);

  grunt.registerTask('deploy:dist', [
    'build:dist',
    'ftp-deploy:sex-gallery-dist'
  ]);

  grunt.registerTask('deploy:gzip', [
    'build:dist',
    'ftp-deploy:gzip'
  ]);

  grunt.registerTask('default', [
    'build:dist'
  ]);
}
